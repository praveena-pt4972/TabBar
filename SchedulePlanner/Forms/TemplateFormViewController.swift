//
//  AddTemplateViewController.swift
//  TabBar
//
//  Created by praveena-pt4972 on 04/05/22.
//

import UIKit
import CoreData

class TemplateFormViewController: UIViewController,UITextFieldDelegate {
    
    var includeWeekend : Bool = true
    let templateNameTextField : UITextField = {
        let textField = UITextField()
        textField.translatesAutoresizingMaskIntoConstraints = false
        textField.placeholder = "Name your Template"
        textField.keyboardType = UIKeyboardType.default
        textField.returnKeyType = UIReturnKeyType.done
        textField.autocorrectionType = UITextAutocorrectionType.no
        textField.font = UIFont.systemFont(ofSize: 18)
        textField.borderStyle = UITextField.BorderStyle.roundedRect
        textField.layer.borderWidth = 1
        textField.layer.cornerRadius = 5
        textField.clearButtonMode = UITextField.ViewMode.whileEditing;
        textField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        return textField
    }()
    let dayNumberLabel : UILabel = {
        let textLabel = UILabel()
        textLabel.heightAnchor.constraint(equalToConstant: 20.0).isActive = true
        textLabel.text  = "1 Day"
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.textAlignment = .center
        return textLabel
    }()
        
    let image : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(systemName: "text.justify.left")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.tintColor = .systemGray
        return imageView
    }()
    let image2 : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(systemName: "calendar.badge.plus")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.tintColor = .systemGray
        return imageView
    }()
    let image3 : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(systemName: "calendar.badge.exclamationmark")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.tintColor = .systemGray
        return imageView
    }()
    let includeWeekendLabel : UILabel = {
        let textLabel = UILabel()
        textLabel.heightAnchor.constraint(equalToConstant: 20.0).isActive = true
        textLabel.text  = "Include Weekend"
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.textAlignment = .center
        return textLabel
    }()
    let toogleSwitch: UISwitch = {
        let toogleSwitch = UISwitch()
        toogleSwitch.onTintColor = themeColor
        toogleSwitch.translatesAutoresizingMaskIntoConstraints = false
        return toogleSwitch
    }()

    let daySlider = UISlider()
    var models = [Template]()

    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Add Routine"
        view.backgroundColor = .systemBackground
        view.addSubview(image)
        view.addSubview(templateNameTextField)
        templateNameTextField.delegate = self
        configureBarItems()
        view.addSubview(image2)
        view.addSubview(dayNumberLabel)
        daySlider.center = self.view.center
        daySlider.translatesAutoresizingMaskIntoConstraints = false
        daySlider.minimumValue = 1
        daySlider.maximumValue = 7
        daySlider.isContinuous = true
        daySlider.tintColor = UIColor(red: 0.92, green: 0.59, blue: 0.58, alpha: 1.00)
        daySlider.addTarget(self, action: #selector(self.sliderValueDidChange(_:)), for: .valueChanged)
    
        view.addSubview(daySlider)
        UIView.animate(withDuration: 0.8) { [self] in
            daySlider.setValue(1, animated: true)
            view.addSubview(image3)
            view.addSubview(includeWeekendLabel)
            view.addSubview(toogleSwitch)
            toogleSwitch.addTarget(self, action: #selector(toggleSwitch(sender:)), for: .valueChanged)
        }
        
        self.view = view
        addConstraints()
        self.navigationController?.presentationController?.delegate = self
       
        }
    
    @objc func toggleSwitch(sender: UISwitch) {
        if sender.isOn {
            includeWeekend = true
        }else{
            includeWeekend = false
        }
    }
    @objc func actionTextFieldIsEditingChanged(sender: UITextField) {
        if (sender.text == nil) {
            navigationItem.rightBarButtonItem?.isEnabled = false
            } else {
                navigationItem.rightBarButtonItem?.isEnabled = true
            }
     }
    
    func addConstraints(){
        image.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 40).isActive = true
        image.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 20).isActive = true
        
        templateNameTextField.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        templateNameTextField.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 60).isActive = true
        templateNameTextField.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor,constant: -100).isActive = true
        templateNameTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        image2.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 150).isActive = true
        image2.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 20).isActive = true
        
        dayNumberLabel.centerXAnchor.constraint(equalTo: view.centerXAnchor,constant: 0).isActive = true
        dayNumberLabel.topAnchor.constraint(equalTo: templateNameTextField.bottomAnchor,constant: 30).isActive = true
        
        daySlider.topAnchor.constraint(equalTo: dayNumberLabel.bottomAnchor,constant: 12).isActive = true
        daySlider.widthAnchor.constraint(equalTo: templateNameTextField.widthAnchor).isActive = true
        daySlider.centerXAnchor.constraint(equalTo: templateNameTextField.centerXAnchor).isActive = true
        
        image3.topAnchor.constraint(equalTo: image2.safeAreaLayoutGuide.topAnchor,constant: 80).isActive = true
        image3.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 20).isActive = true

        includeWeekendLabel.topAnchor.constraint(equalTo: daySlider.bottomAnchor,constant: 60).isActive = true
        includeWeekendLabel.leadingAnchor.constraint(equalTo: daySlider.leadingAnchor,constant: 5).isActive = true
        includeWeekendLabel.font = UIFont.preferredFont(forTextStyle: .headline, compatibleWith: .current)
        
        toogleSwitch.topAnchor.constraint(equalTo: daySlider.bottomAnchor,constant: 55).isActive = true
        toogleSwitch.trailingAnchor.constraint(equalTo: daySlider.trailingAnchor,constant: 0).isActive = true
        toogleSwitch.isOn = true
        
        
    }

        func configureBarItems(){
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelTemplate))
            navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(addTemplate))
            navigationItem.rightBarButtonItem?.tintColor = themeColor
            navigationItem.leftBarButtonItem?.tintColor = themeColor
    }

    let step:Float = 1
    
    var noOfDays : Int64 = 1
    
    @objc func sliderValueDidChange(_ sender:UISlider!)
    {
        var roundedStepValue =  sender.value
        roundedStepValue =  round(sender.value)
        if roundedStepValue == 1{
            dayNumberLabel.text = "\(String(Int(roundedStepValue))) Day "
        }else{
            dayNumberLabel.text = "\(String(Int(roundedStepValue))) Days "

        }
        noOfDays = Int64(roundedStepValue)
    }


    @objc func cancelTemplate(){
        if ((templateNameTextField.text!.trimmingCharacters(in: .whitespaces).count == 0)) {
            self.dismiss(animated: true,completion: nil)
        }else{
            let actionsheet = UIAlertController(
                    title: "Are you sure you want to discard this new event?",
                    message: "",
                    preferredStyle: .actionSheet)
                    actionsheet.addAction(UIAlertAction(title: "Discard Changes", style: .destructive, handler: { _ in
                        self.dismiss(animated: true,completion: nil)
            
                    }))
                    actionsheet.addAction(UIAlertAction(title: "Keep Editing", style: .cancel, handler: nil))
            
                    present(actionsheet,animated: true)
        }

    }
    
    @objc func addTemplate(){

        getExistingTemplate()
        if filterTemplate.count != 0{
            let actionsheet = UIAlertController(
                title: "Alert",
                message: "This Template Name already exists",
                preferredStyle: .alert)
            actionsheet.addAction(UIAlertAction(title: "ok", style: .cancel, handler: nil))
            self.present(actionsheet,animated: true)
        }
        else if (templateNameTextField.text?.trimmingCharacters(in: .whitespaces) == "") {

            let actionsheet = UIAlertController(
                title: "Alert",
                message: "Template name should not be empty",
                preferredStyle: .alert)
           
            actionsheet.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
            }))
            present(actionsheet,animated: true)

         }
        else{
            self.createItem(name1: templateNameTextField.text!,noOfDays1 : noOfDays,includeWeekend : includeWeekend)
            
        }
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           
           let maxLength : Int = 30
      
           let currentString: NSString = textField.text! as NSString
           
           let newString: NSString =  currentString.replacingCharacters(in: range, with: string) as NSString
           return newString.length <= maxLength
    }
 
    func createItem(name1 : String,noOfDays1 : Int64,includeWeekend : Bool){
        let newItem = Template(context: context)
        newItem.templateName = name1.trimmingCharacters(in: .whitespaces)
        newItem.noOfDays = noOfDays1
        newItem.includeWeekend = includeWeekend
        newItem.date = Date()

        do{
            try context.save()
            self.dismiss(animated: true,completion: nil)
            NotificationCenter.default.post(name: Notification.Name("routineSaved"), object: nil)
        }catch{
            print("Error")
        }
        
    }
    var filterTemplate = [Template]()
    
    func getExistingTemplate(){
       do{
           let requestEvents = Template.fetchRequest() as NSFetchRequest<Template>
           let predicate = NSPredicate(format: "templateName MATCHES[c] %@", templateNameTextField.text!.trimmingCharacters(in: .whitespaces))
           requestEvents.predicate = predicate
           filterTemplate = try context.fetch(requestEvents)

           
       }catch {
           print(error)
       }
    }

}

extension TemplateFormViewController : UIAdaptivePresentationControllerDelegate  {

    func presentationControllerShouldDismiss(_ presentationController: UIPresentationController) -> Bool {
        guard let textfield = templateNameTextField.text,  textfield.isEmpty else{
            return false
        }
        return true
    }

}

