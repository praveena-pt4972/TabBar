//
//  TemplateEventFormViewController.swift
//  SchedulePlannerAppPlanner
//
//  Created by praveena-pt4972 on 04/06/22.
//

import UIKit
import CoreData

class TemplateEventFormViewController: UIViewController , UITableViewDataSource,UITableViewDelegate, UITextFieldDelegate, UITextViewDelegate {

    var eventTitle : String = ""
    var notes : String = "Notes"
    var tagName : String = ""
    var startTime = Date().addingTimeInterval(TimeInterval(5.0 * 60.0)).nearestMinute()
    var endTime = Date().addingTimeInterval(TimeInterval(15.0 * 60.0)).nearestMinute()
    var remainder : Date?
    var noOfdays : Int64
    var includeWeekend : Bool = true
    var day : Int = 1
    var templateName : String

    init(noOfdays : Int64 ,day : Int,templateName : String){
        self.noOfdays = noOfdays
        self.day = day
        self.templateName = templateName
        super.init(nibName: nil, bundle: nil)

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let eventFormTableView : UITableView = {
        let eventFormTableView = UITableView(frame: CGRect.zero, style: .insetGrouped)
        eventFormTableView.autoresizingMask = .flexibleWidth
        eventFormTableView.translatesAutoresizingMaskIntoConstraints = false
        return eventFormTableView
    }()


    let options : [String] = ["5 mins Before","10 mins Before","At time of Event"]
    var isOpened : Bool = false
    var templateEvents = [TemplateEvents]()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Add Event"
        view.backgroundColor = .systemBackground
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(addEvent))
        navigationItem.rightBarButtonItem?.tintColor = themeColor

        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelEvent))
        navigationItem.leftBarButtonItem?.tintColor = themeColor
        eventFormTableView.register(EventTitleCustomCell.self, forCellReuseIdentifier: "textfield")
        eventFormTableView.register(EventNotesCustomCell.self, forCellReuseIdentifier: "textView")
        eventFormTableView.register(EventTimeCustomCell.self, forCellReuseIdentifier: "starttime")
        eventFormTableView.register(EventTimeCustomCell.self, forCellReuseIdentifier: "endtime")
        eventFormTableView.register(ExpandableRemainderTableViewCell.self, forCellReuseIdentifier: "subCells")
        eventFormTableView.register(EventTagCustomTableViewCell.self, forCellReuseIdentifier: "color")
        eventFormTableView.register(EventRemainderCustomCell.self, forCellReuseIdentifier: "remainder")
        view.addSubview(eventFormTableView)
        eventFormTableView.keyboardDismissMode = .onDrag

        setUpTableView()
        let tapGesture = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        eventFormTableView.addGestureRecognizer(tapGesture)
        tapGesture.cancelsTouchesInView = false
        self.navigationController?.presentationController?.delegate = self
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTable), name: Notification.Name("tagSaved"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTable), name: Notification.Name("routineSaved"), object: nil)

    }
    @objc func reloadTable(){
        getAllTags()
        self.eventFormTableView.reloadData()

    }
    
    func setUpTableView(){
        
//        getAllTags()
        eventFormTableView.reloadData()
        eventFormTableView.frame = view.bounds
        eventFormTableView.delegate = self
        eventFormTableView.dataSource = self
        eventFormTableView.backgroundColor = .secondarySystemBackground
        eventFormTableView.sectionFooterHeight = 30
        eventFormTableView.separatorColor = eventFormTableView.backgroundColor
        eventFormTableView.showsVerticalScrollIndicator = false
        eventFormTableView.estimatedRowHeight = 200
    }
  
     @objc func dismissKeyboard() {
        view.endEditing(true)
    }
  

    @objc func addEvent(){
 
       getAllTags()
        if (eventTitle.trimmingCharacters(in: .whitespaces)  == "") {

           let actionsheet = UIAlertController(
               title: "Alert",
               message: "Event title should not be empty",
               preferredStyle: .alert)
          
           actionsheet.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in

           }))
           present(actionsheet,animated: true)
        }else{
            if notes.trimmingCharacters(in: .whitespaces) == "Notes"{
                notes = ""
            }
            if endTime < startTime || startTime == endTime{
                let actionsheet = UIAlertController(
                        title: "Alert",
                        message: "Event end time should be after the start time",
                        preferredStyle: .alert)
                actionsheet.addAction(UIAlertAction(title: "ok", style: .default, handler: { _ in
                        }))
                    actionsheet.popoverPresentationController?.sourceView = self.view
                    present(actionsheet,animated: true)
            }else{
                createEvents(title : eventTitle,notes : notes.trimmingCharacters(in: .whitespaces),date : Date(), startTime : startTime,endTime : endTime,tag : String(tags[lastSelectedIndexPath!.last!].name!),day : Int64(day),includeWeekend : includeWeekend)
            }
            
            
            
        }
    }
    
    func createEvents(title : String,notes : String,date : Date , startTime : Date,endTime : Date,tag : String,day : Int64,includeWeekend : Bool){
        let newItem = TemplateEvents(context: context)
       
            newItem.title = title
            newItem.notes = notes
            newItem.startTime = startTime
            newItem.endTime = endTime
            newItem.tag = tag
            newItem.dayNumber = day
            newItem.templateName = templateName
        if remainder != nil {
            newItem.remainder = remainder

        }else{
            newItem.remainder = nil
        }

//        let templateNameFound = events.filter { $0.templateName!.contains(templateName) }
//
//        if templateNameFound.count != 0{
//            let eventItem = Events(context: context)
//            eventItem.templateEvents = newItem
//        }
        
        do{
            try context.save()
            NotificationCenter.default.post(name: Notification.Name("templateEventSaved"), object: nil)
            self.dismiss(animated: true,completion: nil)
        }catch{
            print("Error")
        }
        
    }
    @objc func cancelEvent(){
        if ((eventTitle.trimmingCharacters(in: .whitespaces) .count == 0) && notes.trimmingCharacters(in: .whitespaces)  == "Notes" || (notes.trimmingCharacters(in: .whitespaces).count == 0)  || notes == " " ) {

            self.dismiss(animated: true,completion: nil)


        }else{
                        let actionsheet = UIAlertController(
                            title: "Are you sure you want to discard this new event?",
                            message: "",
                            preferredStyle: .actionSheet)
                        actionsheet.addAction(UIAlertAction(title: "Discard Changes", style: .destructive, handler: { _ in
                            self.dismiss(animated: true,completion: nil)
            
                        }))
                        actionsheet.addAction(UIAlertAction(title: "Keep Editing", style: .cancel, handler: nil))
            actionsheet.popoverPresentationController?.sourceView = self.view

                        present(actionsheet,animated: true)
        }
        
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           eventTitle = textField.text!

           let maxLength : Int = 30
      
           
           let currentString: NSString = textField.text! as NSString
           
           let newString: NSString =  currentString.replacingCharacters(in: range, with: string) as NSString
           return newString.length <= maxLength
    }

    func textFieldDidChangeSelection(_ textField: UITextField) {
        eventTitle = textField.text!
//        if textField.text!.count < 0 || textField.text!.count > 30 {
//            let alert = UIAlertController(
//                title: "Alert",
//                message: "Event title should not be more than 30 letters",
//                preferredStyle: .alert)
//
//            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
//
//            }))
//            present(alert,animated: true)
//
//       }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        notes = textView.text!
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Notes"{
            textView.text = ""
            textView.textColor = .label
        }

    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
           // textView.resignFirstResponder()
            textView.text = textView.text
            return true

        }
         
        guard let rangeOfTextToReplace = Range(range, in: textView.text) else {
           return false
       }
       let substringToReplace = textView.text[rangeOfTextToReplace]
       let count = textView.text.count - substringToReplace.count + text.count
       return count <= 500


    }


    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Notes"
            textView.textColor = .placeholderText
        }
    }
    var selectedIndexPath: IndexPath? = [2,3]

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
        dismissKeyboard()
      
            if indexPath.section ==  2{
                if indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3{
                    let previous = selectedIndexPath
                        selectedIndexPath = indexPath
                        let allowDeselection = true // optional behavior
                        if allowDeselection && previous == selectedIndexPath {
                            selectedIndexPath = nil
                        }
                        tableView.reloadRows(at: [previous, selectedIndexPath].compactMap({ $0 }), with: .automatic)
                        tableView.deselectRow(at: indexPath, animated: true)
                }
   
            }
        if selectedIndexPath == [2,1]{
            let beforeFiveMin = Calendar.current.date(
              byAdding: .minute,
              value: -5,
              to: startTime)
            remainder = beforeFiveMin!
            
        }
        else if selectedIndexPath == [2,2]{
            let beforeTenMin = Calendar.current.date(
              byAdding: .minute,
              value: -10,
              to: startTime)
            remainder = beforeTenMin!

        }else{
            remainder = startTime
        }
        
        
    }

    @objc func toggled(sender: UISwitch) {
        if sender.isOn {
             isOpened = !isOpened
            eventFormTableView.reloadData()

        }else{
            isOpened = !isOpened
            eventFormTableView.reloadSections([2,3], with: .automatic)

        }
    }

    
    @objc func getEndTime(sender : UIDatePicker){
        endTime = sender.date
    }
    
    @objc func getStartTime(sender : UIDatePicker){
        startTime = sender.date
    }
 
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "textfield", for: indexPath) as! EventTitleCustomCell
                cell.selectionStyle = .none
                cell.backgroundColor = .tertiarySystemBackground
                cell.titletextField.tag = indexPath.row
                cell.titletextField.delegate = self
                eventTitle = cell.titletextField.text!
                         return cell

                }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "textView", for: indexPath) as! EventNotesCustomCell
                    cell.selectionStyle = .none
                cell.notesTextView.text = notes
                    
                cell.backgroundColor = .tertiarySystemBackground
                    if cell.notesTextView.text == "Notes"{
                        cell.notesTextView.textColor = .placeholderText

                    }
                cell.notesTextView.delegate = self
                  return cell
                }
            
        }else if indexPath.section == 1{
            
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "starttime", for: indexPath) as! EventTimeCustomCell
                cell.selectionStyle = .none
                cell.timeLabel.text = "Start time"
                cell.selectionStyle = .none
                cell.backgroundColor = .tertiarySystemBackground
                cell.datePicker.datePickerMode = .time
                cell.datePicker.minuteInterval = 5
                cell.datePicker.date = startTime
                cell.datePicker.addTarget(self, action: #selector(getStartTime(sender:)), for: .valueChanged)
                return cell

            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "endtime", for: indexPath) as! EventTimeCustomCell
                cell.selectionStyle = .none

                cell.datePicker.datePickerMode = .time
                cell.timeLabel.text = "End time"
                cell.selectionStyle = .none
                cell.datePicker.date  = endTime
                cell.datePicker.minuteInterval = 5

                cell.backgroundColor = .tertiarySystemBackground
                cell.datePicker.addTarget(self, action: #selector(getEndTime(sender:)), for: .valueChanged)
                    return cell
                
                }
            } else if indexPath.section == 2
            {
                if indexPath.row == 0{
                let cell = tableView.dequeueReusableCell(withIdentifier: "remainder", for: indexPath) as! EventRemainderCustomCell
                cell.selectionStyle = .none
                cell.remainderLabel.text = "Reminder"
                cell.backgroundColor = .tertiarySystemBackground
                cell.toogleSwitch.addTarget(self, action: #selector(toggled(sender:)), for: .valueChanged)
                    return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "subCells", for: indexPath) as! ExpandableRemainderTableViewCell
                cell.expandableCellsLabel.text = options[indexPath.row - 1]
                cell.selectionStyle = .none
                cell.backgroundColor = .tertiarySystemBackground
                cell.accessoryType = (indexPath == selectedIndexPath) ? .checkmark : .none
                cell.tintColor = themeColor
                return cell
                }
            }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "color", for: indexPath) as! EventTagCustomTableViewCell
            cell.selectionStyle = .none
            cell.collectionView.reloadData()
            cell.tagNameLabel .text = "Tag"
            cell.tagButton.addTarget(self, action: #selector(didTabAddTagButton(_:)), for: .touchUpInside)

            cell.backgroundColor = .tertiarySystemBackground
            return cell

        }

    }
    @objc func didTabAddTagButton(_ sender: Any){
        let rootVC = TagFormViewController()
        let navVC = UINavigationController(rootViewController: rootVC)
        navVC.modalPresentationStyle = .pageSheet
        present(navVC, animated: true)

    }

     func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
         if section == 0{
             return 30
         }else if section == 3{
             return 0
         }else {
             return 20

         }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 3{
            return 1
        }else if section == 2{
            if isOpened{
                return options.count + 1
            }else{
                return 1
            }
        }else{
            return 2
        }

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 3{
            return 250
        }else if indexPath.section == 0{
            if indexPath.row == 1
            {
                return 100
            }else{
                return 70
            }
        }else{
            return 70

        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    var tags = [Tags]()
    func getAllTags(){
    do{
        let request = Tags.fetchRequest() as NSFetchRequest<Tags>
        tags = try context.fetch(request)
     }catch {
         print(error)
        }
    }
}

extension TemplateEventFormViewController : UIAdaptivePresentationControllerDelegate  {

    func presentationControllerShouldDismiss(_ presentationController: UIPresentationController) -> Bool {
        if eventTitle.count != 0 || (notes.count != 0 && notes != "Notes"){
            return false
        }
        return true
    }
    

}



