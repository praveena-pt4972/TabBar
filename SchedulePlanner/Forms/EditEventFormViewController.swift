//
//  EditEventViewController.swift
//  SchedulePlannerAppPlanner
//
//  Created by praveena-pt4972 on 18/06/22.
//


import UIKit
import CoreData

class EditEventFormViewController: UIViewController , UITableViewDataSource,UITableViewDelegate, UITextFieldDelegate, UITextViewDelegate, UIAdaptivePresentationControllerDelegate {

  
    private var eventTitle : String?
    private var notes : String?
    private var tagName : String = ""
    private var date : Date?
    private var startTime : Date?
    private var endTime : Date?
    private var remainder : Date?

    private var event : Events
    init(event : Events){
        self.event = event
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let eventEditFormTableView : UITableView = {
        let eventEditFormTableView = UITableView(frame: CGRect.zero, style: .insetGrouped)
        eventEditFormTableView.autoresizingMask = .flexibleWidth
        eventEditFormTableView.translatesAutoresizingMaskIntoConstraints = false
        return eventEditFormTableView
    }()


    let options : [String] = ["5 mins Before","10 mins Before","At time of Event"]
    var isOpened : Bool = false
   var reloadDetailedEventTable: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Update Event"
        view.backgroundColor = .secondarySystemBackground
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .save, target: self, action: #selector(addEvent))
        navigationItem.rightBarButtonItem?.tintColor = themeColor
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelEvent))
        navigationItem.leftBarButtonItem?.tintColor = themeColor
        eventEditFormTableView.register(EventTitleCustomCell.self, forCellReuseIdentifier: "textfield")
        eventEditFormTableView.register(EventNotesCustomCell.self, forCellReuseIdentifier: "textView")
        eventEditFormTableView.register(EventTimeCustomCell.self, forCellReuseIdentifier: "starttime")
        eventEditFormTableView.register(EventTimeCustomCell.self, forCellReuseIdentifier: "endtime")
        eventEditFormTableView.register(ExpandableRemainderTableViewCell.self, forCellReuseIdentifier: "subCells")
        eventEditFormTableView.register(EventTagCustomTableViewCell.self, forCellReuseIdentifier: "color")
        eventEditFormTableView.register(EventRemainderCustomCell.self, forCellReuseIdentifier: "remainder")
        view.addSubview(eventEditFormTableView)
        eventEditFormTableView.keyboardDismissMode = .onDrag
        eventEditFormTableView.backgroundColor = .secondarySystemBackground
        setUpTableView()
        let tapGesture = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        eventEditFormTableView.addGestureRecognizer(tapGesture)
        tapGesture.cancelsTouchesInView = false
        self.navigationController?.presentationController?.delegate = self
        isOpened = (event.remainder != nil)
        
        if event.remainder != nil {
            let afterTenMin = Calendar.current.date(
              byAdding: .minute,
              value: 10,
              to: event.remainder!)
            
            
            let afterFiveMin = Calendar.current.date(
              byAdding: .minute,
              value: 5,
              to: event.remainder!)
            if afterFiveMin == event.startTime{
                selectedIndexPath = [2,1]
            }else if afterTenMin == event.startTime{
                selectedIndexPath = [2,2]
            }else if event.remainder == event.startTime{
                selectedIndexPath = [2,3]
            }else{
                selectedIndexPath = nil
            }
        }else{
            selectedIndexPath = [2,3]
        }
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTable), name: Notification.Name("tagSaved"), object: nil)
    }
    @objc func reloadTable(){
        getAllTags()
        self.eventEditFormTableView.reloadData()

    }
    override func viewDidAppear(_ animated: Bool) {
        getAllTags()
        eventEditFormTableView.reloadData()
    }
    
    func setUpTableView(){
        eventEditFormTableView.reloadData()
        eventEditFormTableView.frame = view.bounds
        eventEditFormTableView.delegate = self
        eventEditFormTableView.dataSource = self
        eventEditFormTableView.sectionFooterHeight = 30
        eventEditFormTableView.separatorColor = .secondarySystemFill
        eventEditFormTableView.showsVerticalScrollIndicator = false
        eventEditFormTableView.estimatedRowHeight = 200
        eventEditFormTableView.allowsMultipleSelection = false
        eventEditFormTableView.estimatedRowHeight = 200.0
        eventEditFormTableView.rowHeight = UITableView.automaticDimension
    }
  
     @objc func dismissKeyboard() {
        view.endEditing(true)
    }
  

    @objc func addEvent(){
        if eventTitle == nil{
            eventTitle = event.title!
        }
        if notes == nil{
            notes = event.notes!
        }
        if date == nil{
            date = event.date!
        }
        if startTime == nil{
            startTime = event.startTime!
            
        }
        if endTime == nil{
            endTime = event.endTime!

        }

        if (eventTitle?.count == 0) {

           let actionsheet = UIAlertController(
               title: "Alert",
               message: "Event title should not be empty",
               preferredStyle: .alert)
          
           actionsheet.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in

           }))
           present(actionsheet,animated: true)

        }else if endTime! < startTime! || startTime == endTime{
            let actionsheet = UIAlertController(
                    title: "Alert",
                    message: "Event end time should be after the start time",
                    preferredStyle: .alert)
            actionsheet.addAction(UIAlertAction(title: "ok", style: .default, handler: { _ in
                    }))
                actionsheet.popoverPresentationController?.sourceView = self.view
                present(actionsheet,animated: true)
        }

        else{
        updateItem(item: event,title : eventTitle!.trimmingCharacters(in: .whitespaces),notes : notes!.trimmingCharacters(in: .whitespaces),date : date!, startTime : startTime!,endTime : endTime!,tag : String(tags[lastSelectedIndexPath!.last!].name!))
        }
    }
    
    func updateItem(item: Events,title : String ,notes : String,date : Date , startTime : Date,endTime : Date,tag : String){
        item.title = title
        item.notes =  notes
        item.date =  date
        item.startTime =  startTime
        item.endTime =  endTime
        item.tag =  tag
        if remainder != nil{
            item.remainder =  remainder

        }else{
            item.remainder = nil
        }

        do{
            try context.save()
            NotificationCenter.default.post(name: Notification.Name("UpdateEventSaved"), object: nil)
            schedulingNotification()
            self.dismiss(animated: true,completion: nil)
        }catch{
            print("error")
        }
  
    }
    
    @objc func cancelEvent(){

        if ((eventTitle == nil) && (notes == nil) && (startTime == nil) && (endTime == nil)) || (remainder == event.remainder) {
            self.dismiss(animated: true,completion: nil)
        }else{
          
            let actionsheet = UIAlertController(
                    title: "Are you sure you want to discard this new event?",
                    message: "",
                    preferredStyle: .actionSheet)
            actionsheet.addAction(UIAlertAction(title: "Discard Changes", style: .destructive, handler: { _ in
                    self.dismiss(animated: true,completion: nil)
    
                }))
            actionsheet.addAction(UIAlertAction(title: "Keep Editing", style: .cancel, handler: nil))
            actionsheet.popoverPresentationController?.sourceView = self.view
            present(actionsheet,animated: true)
        }     }

    func textFieldDidChangeSelection(_ textField: UITextField) {
        eventTitle = textField.text!
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        eventTitle = textField.text!

           let maxLength : Int = 30
      
           
           let currentString: NSString = textField.text! as NSString
           
           let newString: NSString =  currentString.replacingCharacters(in: range, with: string) as NSString
           return newString.length <= maxLength
    }
    
    func textViewDidChange(_ textView: UITextView) {
        notes = textView.text!
    }

    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.text == "Notes"{
            textView.text = ""
            textView.textColor = .label
        }

    }

    var lastIndexPath : IndexPath? = []
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        notes = textView.text!
        if text == "\n" {
            textView.text = textView.text
            return true
        }
         
        guard let rangeOfTextToReplace = Range(range, in: textView.text) else {
           return false
       }
       let substringToReplace = textView.text[rangeOfTextToReplace]
       let count = textView.text.count - substringToReplace.count + text.count
       return count <= 500
    }

    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text == "" {
            textView.text = "Notes"
            textView.textColor = .placeholderText
        }
    }
    var selectedIndexPath: IndexPath?
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
      
            if indexPath.section ==  2{
               if indexPath.row == 1 || indexPath.row == 2 || indexPath.row == 3{

                   let previous = selectedIndexPath
                       selectedIndexPath = indexPath
                       
                       let allowDeselection = true
                       if allowDeselection && previous == selectedIndexPath {
                           selectedIndexPath = nil
                       }
                       
                       tableView.reloadRows(at: [previous, selectedIndexPath].compactMap({ $0 }), with: .automatic)
                    tableView.deselectRow(at: indexPath, animated: true)

                   if  startTime != nil {
                       if selectedIndexPath == [2,1]{
                           let beforeFiveMin = Calendar.current.date(
                             byAdding: .minute,
                             value: -5,
                             to: startTime!)
                           remainder = beforeFiveMin!
                           
                       }else if selectedIndexPath == [2,2]{
                           let beforeTenMin = Calendar.current.date(
                             byAdding: .minute,
                             value: -10,
                             to: startTime!)
                           remainder = beforeTenMin!

                       }else{
                           remainder = startTime!
                       }
                   }else{
                       if selectedIndexPath == [2,1]{
                           let beforeFiveMin = Calendar.current.date(
                             byAdding: .minute,
                             value: -5,
                             to: event.startTime!)
                           remainder = beforeFiveMin!
                           
                       }else if selectedIndexPath == [2,2]{
                           let beforeTenMin = Calendar.current.date(
                             byAdding: .minute,
                             value: -10,
                             to: event.startTime!)
                           remainder = beforeTenMin!

                       }else{
                           remainder = event.startTime!
                       }

                   }
               }
        }

    }
   
    @objc func toggled(sender: UISwitch) {
        if sender.isOn {
             isOpened = !isOpened
            eventEditFormTableView.reloadSections([2], with: .automatic)

            remainder = event.startTime!

        }else{
            isOpened = !isOpened
            eventEditFormTableView.reloadSections([2], with: .automatic)
            remainder = nil
        }
    }
    
    @objc func getEndTime(sender : UIDatePicker){
        endTime = sender.date
    }
    
    @objc func getStartTime(sender : UIDatePicker){
        date = sender.date
        startTime = sender.date
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "textfield", for: indexPath) as! EventTitleCustomCell
                cell.selectionStyle = .none
                cell.backgroundColor = .tertiarySystemBackground
                cell.titletextField.tag = indexPath.row
                if eventTitle == nil{
                    cell.titletextField.text = event.title!.trimmingCharacters(in: .whitespaces)
   
                   }else{
                       cell.titletextField.text = eventTitle!.trimmingCharacters(in: .whitespaces)
   
                   }
                cell.titletextField.delegate = self
                         return cell

                }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "textView", for: indexPath) as! EventNotesCustomCell
                    cell.selectionStyle = .none
                    if event.notes == ""{
                        cell.notesTextView.text = "Notes"
                        cell.notesTextView.textColor = .placeholderText
                    }else{
                        if notes == nil{
                            cell.notesTextView.text = event.notes?.trimmingCharacters(in: .whitespacesAndNewlines)
                            cell.notesTextView.textColor = .label

                        }else{
                            cell.notesTextView.text = notes?.trimmingCharacters(in: .whitespacesAndNewlines)
                            cell.notesTextView.textColor = .label

                        }
                    }
                    
                cell.backgroundColor = .tertiarySystemBackground
                cell.notesTextView.delegate = self
                
                  return cell
                }
            
        }else if indexPath.section == 1{
            
            if indexPath.row == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "starttime", for: indexPath) as! EventTimeCustomCell
                cell.timeLabel.text = "Start time"
                cell.selectionStyle = .none

                if date == nil{
                    cell.datePicker.date = event.startTime!

                }else{
                    cell.datePicker.date = startTime!

                }
                cell.datePicker.minuteInterval = 5
                cell.datePicker.setDate(event.startTime!, animated: true)

                cell.backgroundColor = .tertiarySystemBackground
                cell.datePicker.addTarget(self, action: #selector(getStartTime(sender:)), for: .valueChanged)
                return cell

            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "endtime", for: indexPath) as! EventTimeCustomCell
                cell.timeLabel.text = "End time"
                cell.selectionStyle = .none
                cell.datePicker.setDate(event.endTime!, animated: true)
                if endTime == nil{
                    cell.datePicker.date = event.endTime!

                }else{
                    cell.datePicker.date = endTime!

                }
                cell.datePicker.minuteInterval = 5
                cell.backgroundColor = .tertiarySystemBackground
                cell.datePicker.addTarget(self, action: #selector(getEndTime(sender:)), for: .valueChanged)
                    return cell
                
                }
            }else if indexPath.section == 2{
                if indexPath.row == 0{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "remainder", for: indexPath) as! EventRemainderCustomCell
                    cell.remainderLabel.text = "Reminder"
                    cell.selectionStyle = .none

                    cell.backgroundColor = .tertiarySystemBackground
                    cell.toogleSwitch.addTarget(self, action: #selector(toggled(sender:)), for: .valueChanged)
                    if isOpened == true{
                        cell.toogleSwitch.isOn = true
                       remainder = event.remainder

                    }else{
                        cell.toogleSwitch.isOn = false
                    }
                    
                    return cell
                }else{
                    let cell = tableView.dequeueReusableCell(withIdentifier: "subCells", for: indexPath) as! ExpandableRemainderTableViewCell
                    cell.selectionStyle = .none

                    cell.expandableCellsLabel.text = options[indexPath.row - 1]
                    cell.backgroundColor = .tertiarySystemBackground

                    cell.accessoryType = (indexPath == selectedIndexPath) ? .checkmark : .none
                    cell.tintColor = themeColor
                    return cell
                }
        }
            
    else
            {
                    let cell = tableView.dequeueReusableCell(withIdentifier: "color", for: indexPath) as! EventTagCustomTableViewCell
            cell.collectionView.reloadData()
        
                    cell.selectionStyle = .none
                    cell.tagNameLabel .text = "Tag"
        cell.tagButton.addTarget(self, action: #selector(didTabAddTagButton(_:)), for: .touchUpInside)

        cell.backgroundColor = .tertiarySystemBackground
                    return cell
                
            }

    }

    @objc func didTabAddTagButton(_ sender: Any){
        let rootVC = TagFormViewController()
        let navVC = UINavigationController(rootViewController: rootVC)
        navVC.modalPresentationStyle = .pageSheet
        present(navVC, animated: true)

    }
     func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
         if section == 0{
             return 30
         }else if section == 3{
             return 0
         }else {
             return 20

         }
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 3{
            return 1
        }else if section == 2{
            if isOpened{
                return options.count + 1
            }else{
                return 1
            }
        }else{
            return 2
        }

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 3{
            return 250
        }else if indexPath.section == 0{
            if indexPath.row == 1
            {
                return 100
            }else{
                return 70
            }
        }else{
            return 70

        }
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 4
    }
    
    func presentationControllerShouldDismiss(_ presentationController: UIPresentationController) -> Bool {
        if eventTitle != nil || notes != nil || date != nil || startTime != nil || endTime != nil || remainder != nil{
            return false
        }
        return true
    }
    var tags = [Tags]()
    func getAllTags(){
    do{
        let request = Tags.fetchRequest() as NSFetchRequest<Tags>
        tags = try context.fetch(request)
     }catch {
         print(error)
        }

    }
    
}





