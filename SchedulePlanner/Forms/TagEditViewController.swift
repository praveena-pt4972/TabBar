//
//  TagEditViewController.swift
//  SchedulePlannerAppPlanner
//
//  Created by praveena-pt4972 on 30/06/22.
//


import UIKit
import CoreData

class TagEditViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UITextFieldDelegate,UICollectionViewDelegateFlowLayout {
    
    private var name : String?
    private var color : UIColor?
    private var tag : Tags
    init(tag : Tags){
        self.tag = tag
        super.init(nibName: nil, bundle: nil)
    }
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let tagName: UILabel = {
        let tagName = UILabel(frame:CGRect.zero)
        tagName.text = "Tag Name"
        tagName.font = UIFont.preferredFont(forTextStyle: .headline)
        tagName.translatesAutoresizingMaskIntoConstraints = false
        return tagName
    }()
    let tagColor: UILabel = {
        let tagColor = UILabel(frame:CGRect.zero)
        tagColor.text = "Choose Tag Color"
        tagColor.font = UIFont.preferredFont(forTextStyle: .headline)
        tagColor.translatesAutoresizingMaskIntoConstraints = false
        return tagColor
    }()


    let tagNameTextField:UITextField = {
        let tagNameTextField = UITextField()
        tagNameTextField.translatesAutoresizingMaskIntoConstraints = false
        tagNameTextField.placeholder = "Enter tag name"
        tagNameTextField.keyboardType = UIKeyboardType.default
        tagNameTextField.returnKeyType = UIReturnKeyType.done
        tagNameTextField.autocorrectionType = UITextAutocorrectionType.no
        tagNameTextField.borderStyle = UITextField.BorderStyle.roundedRect
        tagNameTextField.layer.borderWidth = 0.1
        tagNameTextField.layer.cornerRadius = 5
        tagNameTextField.clearButtonMode = UITextField.ViewMode.whileEditing;
        tagNameTextField.contentVerticalAlignment = UIControl.ContentVerticalAlignment.center
        tagNameTextField.backgroundColor = .tertiarySystemBackground

        return tagNameTextField
    }()
   
    @objc func addTag(){
        getExistingTags()
        if color == nil {
            color = tag.color
        }
        
        if name == nil {
            name = tagNameTextField.text
        }
        
        if tagFilter.count != 0 && (name?.trimmingCharacters(in: .whitespaces) != tag.name?.trimmingCharacters(in: .whitespaces)){
            let actionsheet = UIAlertController(
                title: "Alert",
                message: "This Tag Name already exists",
                preferredStyle: .alert)
            actionsheet.addAction(UIAlertAction(title: "ok", style: .cancel, handler: nil))
            self.present(actionsheet,animated: true)
        }
        else if name?.trimmingCharacters(in: .whitespaces) == "" && color == nil{
            let actionsheet = UIAlertController(
                title: "Alert",
                message: "Please Enter Tag Name or Choose Tag Color",
                preferredStyle: .alert)
            actionsheet.addAction(UIAlertAction(title: "ok", style: .cancel, handler: nil))
            self.present(actionsheet,animated: true)

        }
        else if name?.trimmingCharacters(in: .whitespaces) == "" || name == nil{
            let actionsheet = UIAlertController(
                title: "Alert",
                message: "Please Enter Tag name",
                preferredStyle: .alert)
            actionsheet.addAction(UIAlertAction(title: "ok", style: .cancel, handler: nil))
            self.present(actionsheet,animated: true)

        }else if color == nil{
            let actionsheet = UIAlertController(
                title: "Alert",
                message: "Please Choose Tag Color",
                preferredStyle: .alert)
            actionsheet.addAction(UIAlertAction(title: "ok", style: .cancel, handler: nil))
            
            self.present(actionsheet,animated: true)

        }
        
        if name?.trimmingCharacters(in: .whitespaces) != "" && name?.trimmingCharacters(in: .whitespaces) != nil && color != nil  && (tagFilter.count == 0 || tag.name?.trimmingCharacters(in: .whitespaces) == name?.trimmingCharacters(in: .whitespaces)){
           
            updateItem(item: updatingTag[0], newName: name!,color : color!)
            dismiss(animated: true,completion: nil)

        }
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
           name = textField.text

           let maxLength : Int = 15
      
           
           let currentString: NSString = textField.text! as NSString
           
           let newString: NSString =  currentString.replacingCharacters(in: range, with: string) as NSString
           return newString.length <= maxLength
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 16
    }
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }



    let cellId = "CellId"

    let collectionView: UICollectionView = {

        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.autoresizingMask = .flexibleWidth
        cv.translatesAutoresizingMaskIntoConstraints = false
        return cv;
     
    }();

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Edit Tag"
        navigationController?.navigationBar.prefersLargeTitles = true
        view.backgroundColor = .secondarySystemBackground
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(addTag))
        navigationItem.rightBarButtonItem?.tintColor = themeColor
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelTagPage))
        navigationItem.leftBarButtonItem?.tintColor = themeColor
        tagNameTextField.delegate = self
        self.navigationController?.presentationController?.delegate = self
        setupViews()
        tagNameTextField.text = tag.name?.trimmingCharacters(in: .whitespaces)
        let tapGesture = UITapGestureRecognizer(target: self, action: "dismissKeyboard")
        view.addGestureRecognizer(tapGesture)
        tapGesture.cancelsTouchesInView = false
    }
    @objc func dismissKeyboard() {
       view.endEditing(true)
   }
    @objc func cancelTagPage(){
        if (name == tag.name && color == nil) {
            self.dismiss(animated: true,completion: nil)
        }else{
            let actionsheet = UIAlertController(
                title: "Are you sure you want to discard this new tag?",
                message: "",
                preferredStyle: .actionSheet)
            actionsheet.addAction(UIAlertAction(title: "Discard Changes", style: .destructive, handler: { _ in
                self.dismiss(animated: true,completion: nil)
            }))
            actionsheet.addAction(UIAlertAction(title: "Keep Editing", style: .cancel, handler: nil))
            present(actionsheet,animated: true)
        }

    }
   
    func setupViews() {
        view.addSubview(tagName)
        view.addSubview(tagNameTextField)
        view.addSubview(tagColor)
        tagName.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 10).isActive = true
        tagName.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 30).isActive = true
        tagNameTextField.topAnchor.constraint(equalTo: tagName.bottomAnchor, constant: 20).isActive = true
        tagNameTextField.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 30).isActive = true
        tagNameTextField.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,constant: -30).isActive = true
        tagNameTextField.widthAnchor.constraint(equalTo: view.safeAreaLayoutGuide.widthAnchor,constant: -80).isActive = true
        tagNameTextField.heightAnchor.constraint(equalToConstant: 50).isActive = true
        tagColor.topAnchor.constraint(equalTo: tagNameTextField.bottomAnchor, constant: 20).isActive = true
        tagColor.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 30).isActive = true
        view.addSubview(collectionView);
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.backgroundColor = .secondarySystemBackground
        collectionView.register(TagCollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.topAnchor.constraint(equalTo: tagColor.bottomAnchor,constant: 20).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: tagColor.leadingAnchor,constant: 10).isActive = true
        collectionView.trailingAnchor.constraint(equalTo:  tagNameTextField.trailingAnchor,constant: 0).isActive = true;
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.heightAnchor.constraint(equalTo: view.safeAreaLayoutGuide.heightAnchor).isActive = true;
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! TagCollectionViewCell
       cell.layer.cornerRadius = 10
           if indexPath.row == 0{
               cell.backgroundColor = UIColor.tagColor(.lavendar)
           }else if indexPath.row == 1{
               cell.backgroundColor = UIColor.tagColor(.lightGreen)
           }else if indexPath.row == 2{
               cell.backgroundColor = UIColor.tagColor(.lightPink)
           }else if indexPath.row == 3{
               cell.backgroundColor = UIColor.tagColor(.orange)
           }else if indexPath.row == 4{
               cell.backgroundColor = UIColor.tagColor(.peach)
           }else if indexPath.row == 5{
               cell.backgroundColor = UIColor.tagColor(.sandal)
           }else if indexPath.row == 6{
               cell.backgroundColor = UIColor.tagColor(.yellow)
           }else if indexPath.row == 7{
               cell.backgroundColor = UIColor.tagColor(.skyblue)
           }else if indexPath.row == 8{
               cell.backgroundColor = UIColor.tagColor(.blue)
           }else if indexPath.row == 9{
               cell.backgroundColor = UIColor.tagColor(.green)
           }else if indexPath.row == 10{
               cell.backgroundColor = UIColor.tagColor(.lightLavendar)
           }else if indexPath.row == 11{
               cell.backgroundColor = UIColor.tagColor(.lightOrange)
           }else if indexPath.row == 12{
               cell.backgroundColor = UIColor.tagColor(.oliveGreen)
           }else if indexPath.row == 13{
               cell.backgroundColor = UIColor.tagColor(.lightSkyBlue)
           }else if indexPath.row == 14{
               cell.backgroundColor = UIColor.tagColor(.themeColor)
           }else {
               cell.backgroundColor = UIColor.tagColor(.lightBlue)
           }
        getExistingTags()
                let filter = updatingTag.filter{ filtered in
            return filtered.color == tag.color
        }
        
        if filter[0].color == cell.backgroundColor{
            selectedIndex = indexPath
            cell.isSelected = (selectedIndex == indexPath)

        }
        return cell;

       
   }
    var selectedIndex : IndexPath?
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard selectedIndex != indexPath else { return }
       
         if let index = selectedIndex {
            let cell = collectionView.cellForItem(at: index) as! TagCollectionViewCell
            cell.isSelected = false
          }
            let cell = collectionView.cellForItem(at: indexPath) as! TagCollectionViewCell
            cell.isSelected = true
            selectedIndex = indexPath
        

        if indexPath.row == 0{
            color = UIColor.tagColor(.lavendar)
        }else if indexPath.row == 1{
            color = UIColor.tagColor(.lightGreen)
        }else if indexPath.row == 2{
            color = UIColor.tagColor(.lightPink)
        }else if indexPath.row == 3{
            color = UIColor.tagColor(.orange)
        }else if indexPath.row == 4{
            color = UIColor.tagColor(.peach)
        }else if indexPath.row == 5{
            color = UIColor.tagColor(.sandal)
        }else if indexPath.row == 6{
            color = UIColor.tagColor(.yellow)
        }else if indexPath.row == 7{
            color = UIColor.tagColor(.skyblue)
        }else if indexPath.row == 8{
            color = UIColor.tagColor(.blue)
        }else if indexPath.row == 9{
            color = UIColor.tagColor(.green)
        }else if indexPath.row == 10{
            color = UIColor.tagColor(.lightLavendar)
        }else if indexPath.row == 11{
            color = UIColor.tagColor(.lightOrange)
        }else if indexPath.row == 12{
            color = UIColor.tagColor(.oliveGreen)
        }else if indexPath.row == 13{
            color = UIColor.tagColor(.lightSkyBlue)
        }else if indexPath.row == 14{
            color = UIColor.tagColor(.themeColor)
        }else {
            color = UIColor.tagColor(.lightBlue)
        }

    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: collectionView.frame.width/5 , height: view.frame.height/14);
        }
    
    
    var tagFilter = [Tags]()
    var eventsFilter = [Events]()
    var templateEventsFilter = [TemplateEvents]()
    var updatingTag = [Tags]()

    
    func getExistingTags(){
    do{
        let request = Tags.fetchRequest() as NSFetchRequest<Tags>
        name = tagNameTextField.text?.trimmingCharacters(in: .whitespaces)
        if let name = name{
            let pred = NSPredicate(format: "name MATCHES[c] %@", name)
            request.predicate = pred
        }
        tagFilter = try context.fetch(request)
        
        let requestEvents = Events.fetchRequest() as NSFetchRequest<Events>
        let predicate = NSPredicate(format: "tag MATCHES[c] %@", tag.name!)
        requestEvents.predicate = predicate
        eventsFilter = try context.fetch(requestEvents)
        
        let requestTemplateEvents = TemplateEvents.fetchRequest() as NSFetchRequest<TemplateEvents>
        requestTemplateEvents.predicate = predicate
        templateEventsFilter = try context.fetch(requestTemplateEvents)
        
        let requestUpdatingTag = Tags.fetchRequest() as NSFetchRequest<Tags>
        let updateTagPredicate = NSPredicate(format: "name MATCHES[c] %@", tag.name!)
        requestUpdatingTag.predicate = updateTagPredicate
        updatingTag = try context.fetch(requestUpdatingTag)

     }catch {
         print(error)
        }
    }
  
    
    func updateItem(item: Tags, newName: String , color : UIColor){
        getExistingTags()
            item.name = newName
            item.color = color
            do{
                for i in 0..<eventsFilter.count{
                    eventsFilter[i].tag = item.name
                }
                for i in 0..<templateEventsFilter.count{
                    templateEventsFilter[i].tag = item.name
                }
                try context.save()
            NotificationCenter.default.post(name: Notification.Name("tagSaved"), object: nil)
            }catch{
                print(error)
            }
        }


    }
    

extension TagEditViewController : UIAdaptivePresentationControllerDelegate  {

    func presentationControllerShouldDismiss(_ presentationController: UIPresentationController) -> Bool {
        if name != nil || color != nil {
            return false
        }else{
            return true

        }
    }
    

}
