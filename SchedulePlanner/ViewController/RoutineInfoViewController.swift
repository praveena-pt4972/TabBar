//
//  RoutineInfoViewController.swift
//  SchedulePlannerAppPlanner
//
//  Created by praveena-pt4972 on 26/06/22.
//

import UIKit

class RoutineInfoViewController: UIViewController , UITableViewDelegate,UITableViewDataSource{
    
    private let templateName : String
    private let includeWeekend : Bool
    private let noOfDays : Int64
    
    init(templateName : String , noOfDays : Int64,includeWeekend : Bool){
            self.templateName = templateName
            self.noOfDays = noOfDays
            self.includeWeekend = includeWeekend
            super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let routineInfoTableView : UITableView = {
        let detailedEventFormTableView = UITableView(frame: CGRect.zero, style: .insetGrouped)
        detailedEventFormTableView.autoresizingMask = .flexibleWidth
        detailedEventFormTableView.translatesAutoresizingMaskIntoConstraints = false
        return detailedEventFormTableView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .secondarySystemBackground
        view.backgroundColor = .secondarySystemBackground
        routineInfoTableView.register(RoutinInfoTableViewCell.self, forCellReuseIdentifier: "cell")
        view.addSubview(routineInfoTableView)
        routineInfoTableView.delegate = self
        routineInfoTableView.dataSource = self
        routineInfoTableView.frame = view.bounds
        routineInfoTableView.rowHeight = UITableView.automaticDimension
        routineInfoTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RoutinInfoTableViewCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.label.text = templateName.trimmingCharacters(in: .whitespaces)
            cell.icon.image = UIImage(systemName: "text.justify.leading")
            return cell
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RoutinInfoTableViewCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            if noOfDays == 1{
                cell.label.text = String(noOfDays) + " "  + "Day"
            }else{
                cell.label.text = String(noOfDays) + " " + "Days"
            }
            cell.icon.image = UIImage(systemName: "calendar.badge.plus")
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RoutinInfoTableViewCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            if includeWeekend == true{
                cell.label.text = "Include Weekend"
            }else{
                cell.label.text = "Without Weekend"
            }
            cell.icon.image = UIImage(systemName: "calendar.badge.exclamationmark")
            return cell
        }
        
    }


}
