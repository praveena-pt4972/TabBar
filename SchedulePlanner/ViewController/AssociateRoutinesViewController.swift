import UIKit
import CoreData

class AssociateRoutinesViewController : UIViewController , UITableViewDelegate , UITableViewDataSource, UISearchResultsUpdating{
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredData(for: searchController.searchBar.text ?? "")
    }
    var templateName : String = ""
    var noOfDaysInTemplate : Int64?
    var includeWeekend : Bool = true
    var routineStartDate : Date = selectedDateInMonthView
    var filteredTemplates : [TemplateEvents] = []
    var filteredData = [Template]()
    var models = [Template]()
    
    let searchController = UISearchController(searchResultsController: nil)

     let routineEventListTable : UITableView = {
        let routineEventListTable = UITableView()
         routineEventListTable.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
         routineEventListTable.autoresizingMask = .flexibleWidth
         routineEventListTable.translatesAutoresizingMaskIntoConstraints = false
         routineEventListTable.sectionFooterHeight = 20
        return routineEventListTable
    }()
    
    
    var startDateLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.text = "Select start Date"
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.font = UIFont.systemFont(ofSize: 20)
        return label
    }()
    
    let datePicker : UIDatePicker =  {
        var datePicker = UIDatePicker()
        datePicker.datePickerMode = .date
        datePicker.tintColor = themeColor
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        return datePicker
    }()
    
    let noEventsMessage : UIImageView = {
        let noEventsMessage = UIImageView()
        let largeConfig = UIImage.SymbolConfiguration(pointSize: 50, weight: .light, scale: .large)
        noEventsMessage.image = UIImage(systemName: "calendar.badge.exclamationmark", withConfiguration: largeConfig)
        noEventsMessage.translatesAutoresizingMaskIntoConstraints = false
        noEventsMessage.tintColor = .placeholderText
        return noEventsMessage
    }()
    
    let noEventsLabel: UILabel = {
        let noEventsLabel = UILabel()
        noEventsLabel.text = "No Routines Found"
        noEventsLabel.textColor = .placeholderText
        noEventsLabel.font = UIFont.preferredFont(forTextStyle: .headline, compatibleWith: .none)
        noEventsLabel.translatesAutoresizingMaskIntoConstraints = false
        return noEventsLabel
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Associate Routines"
        view.backgroundColor = .systemBackground
        navigationController?.navigationBar.prefersLargeTitles = true
        view.addSubview(startDateLabel)
        view.addSubview(datePicker)
        datePicker.date = selectedDateInMonthView
        startDateLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 35).isActive = true
        startDateLabel.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20).isActive = true
        datePicker.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        datePicker.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -50).isActive = true

        view.addSubview(routineEventListTable)
        getAllTemplates()
        routineEventListTable.delegate = self
        routineEventListTable.dataSource = self
        routineEventListTable.showsVerticalScrollIndicator = false
        routineEventListTable.translatesAutoresizingMaskIntoConstraints = false
        routineEventListTable.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 100).isActive = true
        routineEventListTable.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20).isActive = true
        routineEventListTable.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true

        routineEventListTable.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        routineEventListTable.separatorStyle = UITableViewCell.SeparatorStyle.none
        searchController.searchResultsUpdater = self
        definesPresentationContext = true
        configureBarItems()
        datePicker.addTarget(self, action: #selector(getEndTime(sender:)), for: .valueChanged)
        view.addSubview(noEventsMessage)
        noEventsMessage.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 180).isActive = true
        noEventsMessage.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 130).isActive = true
        noEventsMessage.widthAnchor.constraint(equalTo: view.widthAnchor,multiplier: 2/5).isActive = true
        noEventsMessage.heightAnchor.constraint(equalTo: view.heightAnchor,multiplier: 1/5).isActive = true
        view.addSubview(noEventsLabel)
        noEventsLabel.topAnchor.constraint(equalTo: noEventsMessage.bottomAnchor,constant: 10).isActive = true
        noEventsLabel.leadingAnchor.constraint(equalTo: noEventsMessage.leadingAnchor,constant: 10).isActive = true
        self.navigationController?.presentationController?.delegate = self

        noEventsLabel.isHidden = true
        noEventsMessage.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(reloadRoutine), name: Notification.Name("routineSaved"), object: nil)
        
    }
    @objc func reloadRoutine(){
        getAllTemplates()
        routineEventListTable.reloadData()
    }
    
    @objc func getEndTime(sender : UIDatePicker){
        routineStartDate = sender.date

    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            if self.filteredData.count == 0{
                noEventsLabel.isHidden = false
                noEventsMessage.isHidden = false
            }else{
                noEventsLabel.isHidden = true
                noEventsMessage.isHidden = true
            }
            return self.filteredData.count
        } else if searchController.isActive && searchController.searchBar.text == ""{
            noEventsLabel.isHidden = false
            noEventsMessage.isHidden = false
            return 0
        }
        if models.count == 0{
            noEventsLabel.isHidden = false
            noEventsMessage.isHidden = false
        }else{
            noEventsLabel.isHidden = true
            noEventsMessage.isHidden = true
        }
        return models.count
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 15
    }
    
    var selectedIndexPath: IndexPath?
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        if searchController.isActive && searchController.searchBar.text != "" {
            templateName = filteredData[indexPath.section].templateName!
            noOfDaysInTemplate = filteredData[indexPath.section].noOfDays
            includeWeekend = filteredData[indexPath.section].includeWeekend
            let previous = selectedIndexPath
                selectedIndexPath = indexPath
                let allowDeselection = true
                if allowDeselection && previous == selectedIndexPath {
                    selectedIndexPath = nil
                }
                tableView.reloadRows(at: [previous, selectedIndexPath].compactMap({ $0 }), with: .automatic)
                tableView.deselectRow(at: indexPath, animated: true)

        }else{
            templateName = models[indexPath.section].templateName!
            noOfDaysInTemplate = models[indexPath.section].noOfDays
            includeWeekend = models[indexPath.section].includeWeekend
                let previous = selectedIndexPath
                selectedIndexPath = indexPath
                let allowDeselection = true
                if allowDeselection && previous == selectedIndexPath {
                    selectedIndexPath = nil
                }
                tableView.reloadRows(at: [previous, selectedIndexPath].compactMap({ $0 }), with: .automatic)
                tableView.deselectRow(at: indexPath, animated: true)
        }
        

    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
     
        cell.contentView.frame = cell.contentView.frame.inset(by: UIEdgeInsets(top: 5, left: 10, bottom: 20, right: 10))
        cell.layer.borderWidth = 2
        cell.layer.borderColor = themeColor.cgColor
        cell.layer.cornerRadius = 15
         var model = models[indexPath.section]
         var daysInString = String(model.noOfDays)
        if searchController.isActive && searchController.searchBar.text != "" {
            model = filteredData[indexPath.section]
            daysInString = String(filteredData[indexPath.section].noOfDays)
        }else{
            model = models[indexPath.section]
        }
        
        cell.textLabel?.text = "\(model.templateName!.trimmingCharacters(in: .whitespaces))"
        if daysInString == "1"{
            cell.detailTextLabel?.text = "\(daysInString) Day"
        }else{
            cell.detailTextLabel?.text = "\(daysInString) Days"
        }
        cell.accessoryType = (indexPath == selectedIndexPath) ? .checkmark : .none
        cell.tintColor = themeColor
        return cell
    }
    func createTemplateEvent(item: TemplateEvents, date : Date){
            let newItem = Events(context: context)
            newItem.title = item.title
            newItem.notes = item.notes
            newItem.date = date
            newItem.startTime = item.startTime
            newItem.endTime = item.endTime
            newItem.tag = item.tag
            newItem.remainder = item.remainder
            newItem.templateName = item.templateName
        do{
            try context.save()
            NotificationCenter.default.post(name: Notification.Name("routineSaved"), object: nil)
            schedulingNotification()
            dismiss(animated: true,completion: nil)
        }catch{
            print("Error")
        }
    }
    
 
    func deleteItem(item: Template){
        context.delete(item)
        do{
            try context.save()
        }catch{
            print(error)
        }
    }
    func configureBarItems(){
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(applyRoutine))
        navigationItem.rightBarButtonItem?.tintColor = themeColor
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelRoutine))
        navigationItem.leftBarButtonItem?.tintColor = themeColor
        navigationItem.searchController = searchController
        navigationController?.navigationBar.tintColor = themeColor

    }
    
    private func filteredData(for searchText: String) {
        filteredData = models.filter { filtered in
            return filtered.templateName!.trimmingCharacters(in: .whitespaces).lowercased().contains(searchText.lowercased().trimmingCharacters(in: .whitespaces))
      }
        routineEventListTable.reloadData()
    }
    
    @objc func cancelRoutine(){
        if selectedIndexPath == nil{
            dismiss(animated: true,completion: nil)
        }else{
            let actionsheet = UIAlertController(
                    title: "Are you sure you want to discard this associate routine?",
                    message: "",
                    preferredStyle: .actionSheet)
                    actionsheet.addAction(UIAlertAction(title: "Discard Changes", style: .destructive, handler: { _ in
                        self.dismiss(animated: true,completion: nil)
                    }))
                    actionsheet.addAction(UIAlertAction(title: "Keep Editing", style: .cancel, handler: nil))
                actionsheet.popoverPresentationController?.sourceView = self.view
                present(actionsheet,animated: true)
        }
    }

   @objc func applyRoutine(){
       getExistingTemplateEvents()
       var count = 1
       if let noOfDays = noOfDaysInTemplate{
           for i in 0..<noOfDays{
           let filteredDay = existingTemplateEvents.filter { filtered in
                return filtered.dayNumber == i+1
            }
          
            if existingTemplateEvents.count == 0 {
                let actionsheet = UIAlertController(
                       title: "Alert",
                       message: "There is no events in this template",
                       preferredStyle: .alert)

                actionsheet.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in

                }))
                present(actionsheet,animated: true)

            }else{
                   for j in 0..<filteredDay.count{
                       var date = CalendarFunctions().addDays(date: routineStartDate, days: count)
                        if includeWeekend == false{
                            if CalendarFunctions().weekDay(date: date) == 1{
                                date = CalendarFunctions().addDays(date: date, days: 1)
                            }else if CalendarFunctions().weekDay(date: date) == 0{
                                date = CalendarFunctions().addDays(date: date, days: 2)
                                count = count + 2
                            }
                        }
                        self.createTemplateEvent(item: filteredDay[j], date: CalendarFunctions().addDays(date: date, days: -1))
                   }
                count = count + 1

               }
            }
               
       }else{
           let actionsheet = UIAlertController(
               title: "Alert",
               message: "please choose any routines",
               preferredStyle: .alert)
           actionsheet.addAction(UIAlertAction(title: "OK", style: .default, handler: { _ in
           }))
           present(actionsheet,animated: true)
       }
     }
    func getAllTemplates(){
       do{
           let request = Template.fetchRequest() as NSFetchRequest<Template>
           let sort = NSSortDescriptor(key: "date", ascending: false)
           request.sortDescriptors = [sort]
           models = try context.fetch(request)
       }catch {
           print(error)
       }
    }

    var existingTemplateEvents = [TemplateEvents]()
    func getExistingTemplateEvents(){
        do{
            let request = TemplateEvents.fetchRequest() as NSFetchRequest<TemplateEvents>
            let pred = NSPredicate(format: "templateName MATCHES[c] %@", templateName)
            request.predicate = pred
            existingTemplateEvents = try context.fetch(request)
         }catch {
             print(error)
            }
        }
}

extension AssociateRoutinesViewController : UIAdaptivePresentationControllerDelegate  {

    func presentationControllerShouldDismiss(_ presentationController: UIPresentationController) -> Bool {
        if selectedIndexPath != nil{
            return false
        }
        return true
    }

}



