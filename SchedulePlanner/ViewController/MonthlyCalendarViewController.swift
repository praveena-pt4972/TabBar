//
//  ThirdViewController.swift
//  TabBar
//
//  Created by praveena-pt4972 on 02/05/22.
//

import UIKit
import CoreData

var selectedDateInMonthView = Date()

class MonthlyCalendarViewController : UIViewController, UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIGestureRecognizerDelegate{
 
    var currentDateLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.font = UIFont.systemFont(ofSize: 20)
        return label
        
    }()
    var weeklyCalendarMonthLabel : UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.textAlignment = .center
        label.font = UIFont.preferredFont(forTextStyle: .headline)
        label.font = UIFont.systemFont(ofSize: 20)
        return label
        
    }()

    let monthlyCalendarCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.isPagingEnabled = true
        return cv
    }()
    let weeklyCalendarCollectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        return cv
    }()
  
    let previousMonthButton : UIButton = {
        let previousMonthButton = UIButton(type: .custom)
        previousMonthButton.translatesAutoresizingMaskIntoConstraints = false
        let largeConfig = UIImage.SymbolConfiguration(pointSize: 30, weight: .light, scale: .small)
        let rightArrow = UIImage(systemName: "arrowtriangle.left.fill", withConfiguration: largeConfig)
        previousMonthButton.setImage(rightArrow, for: .normal)
        previousMonthButton.tintColor = themeColor
        previousMonthButton.addTarget(self, action: #selector(previousMonth(_:)), for: .touchUpInside)
        return previousMonthButton
    }()
    
    let nextMonthButton : UIButton = {
        let nextMonthButton = UIButton()
        nextMonthButton.translatesAutoresizingMaskIntoConstraints = false
        let largeConfig = UIImage.SymbolConfiguration(pointSize: 30, weight: .light , scale: .small)
        let leftArrow = UIImage(systemName: "arrowtriangle.right.fill", withConfiguration: largeConfig)
        nextMonthButton.setImage(leftArrow, for: .normal)
        nextMonthButton.tintColor = themeColor
        nextMonthButton.addTarget(self, action: #selector(nextMonth(_:)), for: .touchUpInside)
        return nextMonthButton
    }()
    let todayButton : UIButton = {
        let button = UIButton(type: .custom)
        button.translatesAutoresizingMaskIntoConstraints = false
        button.setTitle("Today", for: .normal)
        button.setTitleColor(themeColor , for: .normal)
        button.addTarget(self, action: #selector(didTabTodayButton(_:)), for: .touchUpInside)
        return button
    }()
  
    @objc func didTabTodayButton(_ sender: Any)
    {
        
        if !weeklyCalendarCollectionView.isHidden{
            selectedDateInMonthView = Date()
            setWeekView()
            self.weeklyCalendarCollectionView.scrollToItem(at: IndexPath(row: current, section: 0),
                                              at: .left,
                                           animated: true)
            selectedDateInMonthView = numberOfCellsInWeeklyCalendar[current + 1]
        }else{
            selectedDateInMonthView = Date()
            setMonthView()
            lastSelectedIndex = nil
            eventTableView.reloadData()
        }
        if currentDateLabel.text != CalendarFunctions().monthYearString(date: Date()){
            todayButton.isHidden = false
        }else{
            todayButton.isHidden = true
        }
        
    }
    
   
    let weekDayStack : UIStackView = {
        let weekDayStack = UIStackView()
        weekDayStack.axis = .horizontal
        weekDayStack.distribution = .equalSpacing
        weekDayStack.spacing = 2
        weekDayStack.translatesAutoresizingMaskIntoConstraints = false
        return weekDayStack
    }()
    
    let weekDays = ["Sun","Mon","Tue","Wed","Thur","Fri","Sat"]
    
    func stackCreation(){
        for i in 0..<7{
            let weekDayLabel : UILabel = {
                let weekDayLabel = UILabel()
                weekDayLabel.translatesAutoresizingMaskIntoConstraints = false
                weekDayLabel.textAlignment = .center

                weekDayLabel.text = weekDays[i]
                weekDayLabel.font = UIFont.preferredFont(forTextStyle: .body)
                weekDayLabel.adjustsFontForContentSizeCategory = false
                return weekDayLabel
                
            }()
            weekDayStack.addArrangedSubview(weekDayLabel)
            
        }
    }

    let eventTableView : UITableView = {
        let eventTableView = UITableView()
        eventTableView.register(CustomEventsTableViewCell.self, forCellReuseIdentifier: "cellID")
        return eventTableView
    }()
   
    let noEventsMessage : UIImageView = {
        let noEventsMessage = UIImageView()
        let largeConfig = UIImage.SymbolConfiguration(pointSize: 50, weight: .light, scale: .large)
        noEventsMessage.image = UIImage(systemName: "calendar.badge.exclamationmark", withConfiguration: largeConfig)
        noEventsMessage.translatesAutoresizingMaskIntoConstraints = false
        noEventsMessage.tintColor = .placeholderText
        return noEventsMessage
    }()
    let noEventsLabel: UILabel = {
        let noEventsLabel = UILabel()
        noEventsLabel.text = "No Events Found"
        noEventsLabel.textColor = .placeholderText
        noEventsLabel.font = UIFont.preferredFont(forTextStyle: .headline, compatibleWith: .none)
        noEventsLabel.translatesAutoresizingMaskIntoConstraints = false
        return noEventsLabel
    }()
    
    var tableViewTopAnchorActiveConstraint: NSLayoutConstraint?
    var NoEventMessageTopAnchorActiveConstraint: NSLayoutConstraint?
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        title = "Calendar"
        navigationController?.navigationBar.prefersLargeTitles = true
        view.addSubview(nextMonthButton)
        view.addSubview(currentDateLabel)
        view.addSubview(weeklyCalendarMonthLabel)
        view.addSubview(previousMonthButton)
        previousMonthButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 0).isActive = true
        previousMonthButton.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 80).isActive = true
        currentDateLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 0).isActive = true
        currentDateLabel.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor,constant: 0).isActive = true
        weeklyCalendarMonthLabel.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 0).isActive = true
        weeklyCalendarMonthLabel.centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor,constant: 0).isActive = true
        nextMonthButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 0).isActive = true
        nextMonthButton.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,constant: -80).isActive = true
        view.addSubview(todayButton)
        todayButton.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: -5).isActive = true
        todayButton.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: -10).isActive = true
        currentDateLabel.text = CalendarFunctions().monthString(date: selectedDateInMonthView)
            + " " + CalendarFunctions().yearString(date: selectedDateInMonthView)
        setMonthView()
        setWeekView()
        stackCreation()
        setUpWeekDayConstraints()
        setupCollectionConstraints()
        setupLongGestureRecognizerOnCollection()
        configureBars()
        getAllEvents()
        getAllTags()
        eventTableView.register(CustomEventsTableViewCell.self, forCellReuseIdentifier: "cellID")
        tableViewTopAnchorActiveConstraint = eventTableView.topAnchor.constraint(equalTo: monthlyCalendarCollectionView.bottomAnchor,constant: -80)
        tableViewTopAnchorActiveConstraint!.isActive = true
        setUpTableViewConstraints()
        weeklyCalendarMonthLabel.isHidden = true
        monthlyCalendarCollectionView.isHidden = false
        weeklyCalendarCollectionView.isHidden = true
        selectedDateInMonthView = Date().nearestMinute()
        view.addSubview(noEventsMessage)
        noEventsMessage.centerXAnchor.constraint(equalTo: view.centerXAnchor).isActive = true
        noEventsMessage.widthAnchor.constraint(equalTo: view.widthAnchor,multiplier: 2/5).isActive = true
        noEventsMessage.heightAnchor.constraint(equalTo: view.heightAnchor,multiplier: 1/5).isActive = true
        view.addSubview(noEventsLabel)
        noEventsLabel.topAnchor.constraint(equalTo: noEventsMessage.bottomAnchor,constant: 10).isActive = true
        noEventsLabel.leadingAnchor.constraint(equalTo: noEventsMessage.leadingAnchor,constant: 10).isActive = true
        NoEventMessageTopAnchorActiveConstraint = noEventsLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor,constant: -150)
        NoEventMessageTopAnchorActiveConstraint!.isActive = true
        noEventsLabel.isHidden = true
        noEventsMessage.isHidden = true
        monthlyCalendarCollectionView.reloadData()
        weeklyCalendarCollectionView.reloadData()
        eventTableView.reloadData()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTables), name: Notification.Name("EventSaved"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTables), name: Notification.Name("UpdateEventSaved"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTables), name: Notification.Name("routineSaved"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTables), name: Notification.Name("tagSaved"), object: nil)
       
        }
    @objc func reloadTables(){
        getAllTags()
        getAllEvents()
        self.setMonthView()
        self.monthlyCalendarCollectionView.reloadData()
        self.weeklyCalendarCollectionView.reloadData()
        self.eventTableView.reloadData()
        navigationController?.popViewController(animated: true)

    }
   
    var filterEvents : Int = 1
    var items = [Events]()
    var events = [Events]()
    var segmentedController: UISegmentedControl!
    var weeklyCalendarView : Bool = false
    
    var menuItems: [UIAction] {
        return [
            UIAction(title: "Add Event", image: UIImage(systemName: "clock"), handler: { (_) in
                let rootVC = EventFormViewController()
                let navVC = UINavigationController(rootViewController: rootVC)
                navVC.modalPresentationStyle = .pageSheet
                self.present(navVC, animated: true)

            }),
            UIAction(title: "Associate Routines",image: UIImage(systemName: "repeat.circle"), handler: { (_) in
                
                let rootVC = AssociateRoutinesViewController()
                let navVC = UINavigationController(rootViewController: rootVC)
                navVC.modalPresentationStyle = .pageSheet
                self.present(navVC, animated: true)

            }),
            
        ]
    }
   
    var demoMenu: UIMenu {
        return UIMenu(title: "", image: nil, identifier: nil, options: [], children: menuItems)
    }
    override func viewDidLayoutSubviews() {
        navigationController?.navigationBar.tintColor = themeColor
        let plusButton = UIBarButtonItem(systemItem: .add, primaryAction: nil, menu: demoMenu)
        navigationItem.rightBarButtonItem?.tintColor = themeColor

        if !weeklyCalendarView{
            let image = UIImage(systemName: "list.bullet.rectangle")
            let weeklyViewButton = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(viewWeeklyCalendar(_:)))
              self.navigationItem.rightBarButtonItems = [plusButton,weeklyViewButton]

        }else{
            let image = UIImage(systemName: "list.bullet.rectangle.fill")
            let weeklyViewButton = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(viewWeeklyCalendar(_:)))
              self.navigationItem.rightBarButtonItems = [plusButton,weeklyViewButton]
        }
    }
    
    func configureBars(){
        let image3 = UIImage(systemName: "clock")
        let image4 = UIImage(systemName: "repeat.circle")
        let image5 = UIImage(systemName: "clock.arrow.2.circlepath")

       let items  =  [image5,image3, image4]
        
        segmentedController = UISegmentedControl(items: items)
        navigationItem.titleView = segmentedController
        segmentedController.selectedSegmentTintColor = themeColor
        segmentedController.selectedSegmentIndex = 0
        segmentedController.addTarget(self, action: #selector(buttonDidChanged(_:)), for: .valueChanged)

    }
    
    var numberOfCellsInWeeklyCalendar = [Date]()
    var current = 1
    
       func setWeekView()
       {
      
           numberOfCellsInWeeklyCalendar.removeAll()
           let year = Calendar.current.component(.year, from: Date())
           var lastOfYear = Date()
           if let firstOfNextYear = Calendar.current.date(from: DateComponents(year: year + 2, month: 1, day: 1)){
               lastOfYear = Calendar.current.date(byAdding: .day, value: 0, to: firstOfNextYear)!
           }
           var previous = Date()
           if let previousYear = Calendar.current.date(from: DateComponents(year: year - 2 , month: 1, day: 1)) {
               previous = Calendar.current.date(byAdding: .day, value: 0, to: previousYear)!
           }
          while (previous < lastOfYear)
           {
              numberOfCellsInWeeklyCalendar.append(previous)
              previous = CalendarFunctions().addDays(date: previous, days: 1)
              let previousY = CalendarFunctions().currentDate(date: previous)
              let select = CalendarFunctions().addDays(date: selectedDateInMonthView, days: -1)
              let selected =  CalendarFunctions().currentDate(date: select)
              if previousY == selected {
                  current = numberOfCellsInWeeklyCalendar.count
              }
       }
        weeklyCalendarMonthLabel.text = CalendarFunctions().monthString(date: selectedDateInMonthView)
               + " " + CalendarFunctions().yearString(date: selectedDateInMonthView)
           weeklyCalendarCollectionView.reloadData()
           eventTableView.reloadData()
           
       }

    @objc func viewWeeklyCalendar(_ sender: UIBarButtonItem){
        if !weeklyCalendarView{
            let image = UIImage(systemName: "list.bullet.rectangle.fill")
            let image2 = UIImage(systemName: "plus")
            let weeklyViewButton = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(viewWeeklyCalendar(_:)))
             let plusButton = UIBarButtonItem(image: image2, style: .plain, target: self, action: #selector(addEvents))
             self.navigationItem.rightBarButtonItems = [plusButton,weeklyViewButton]
            self.navigationItem.rightBarButtonItem?.customView?.fadeTransition(0.10)
            monthlyCalendarCollectionView.isHidden = true
            weeklyCalendarMonthLabel.isHidden = false
            weeklyCalendarMonthLabel.fadeTransition(0.8)
            currentDateLabel.isHidden = true
            weeklyCalendarCollectionView.isHidden = false
            weeklyCalendarCollectionView.reloadData()
            eventTableView.reloadData()
            setWeekView()
            weeklyCalendarCollectionView.fadeTransition(0.8)
            previousMonthButton.isHidden = true
            nextMonthButton.isHidden = true
            weekDayStack.isHidden = true
            weekDayStack.fadeTransition(0.8)
            previousMonthButton.fadeTransition(0.8)
            nextMonthButton.fadeTransition(0.8)
            NoEventMessageTopAnchorActiveConstraint = noEventsLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor,constant: -300)
            NoEventMessageTopAnchorActiveConstraint!.isActive = true
            noEventsMessage.fadeTransition(0.4)
            tableViewTopAnchorActiveConstraint!.isActive = false
            tableViewTopAnchorActiveConstraint = eventTableView.topAnchor.constraint(equalTo: weeklyCalendarCollectionView.bottomAnchor,constant: 10)
            tableViewTopAnchorActiveConstraint!.isActive = true
            weeklyCalendarCollectionView.reloadData()
            let indexToScrollTo = IndexPath(item: current + 1, section: 0)
            weeklyCalendarCollectionView.scrollToItem(at: indexToScrollTo, at: .left, animated: false)
            selectedDateInMonthView = numberOfCellsInWeeklyCalendar[indexToScrollTo.row]
            weeklyCalendarView = true
                
        }else{
            let image = UIImage(systemName: "list.bullet")
            let image2 = UIImage(systemName: "plus")
            let weeklyViewButton = UIBarButtonItem(image: image, style: .plain, target: self, action: #selector(viewWeeklyCalendar(_:)))
             let plusButton = UIBarButtonItem(image: image2, style: .plain, target: self, action: #selector(addEvents))
             self.navigationItem.rightBarButtonItems = [plusButton,weeklyViewButton]
            self.navigationItem.rightBarButtonItem?.customView?.fadeTransition(0.10)
            monthlyCalendarCollectionView.isHidden = false
            monthlyCalendarCollectionView.fadeTransition(0.8)
            monthlyCalendarCollectionView.heightAnchor.constraint(equalToConstant: 600).isActive = true
            previousMonthButton.isHidden = false
            nextMonthButton.isHidden = false
            previousMonthButton.fadeTransition(0.8)
            nextMonthButton.fadeTransition(0.8)
            eventTableView.fadeTransition(0.8)
            currentDateLabel.isHidden = false
            currentDateLabel.fadeTransition(0.8)
            weeklyCalendarMonthLabel.isHidden = true
            NoEventMessageTopAnchorActiveConstraint!.isActive = false
            NoEventMessageTopAnchorActiveConstraint = noEventsLabel.bottomAnchor.constraint(equalTo: view.bottomAnchor,constant: -150)
            NoEventMessageTopAnchorActiveConstraint!.isActive = true
            noEventsMessage.fadeTransition(0.4)
            weekDayStack.isHidden = false
            weekDayStack.fadeTransition(0.8)
            setMonthView()
            weeklyCalendarCollectionView.isHidden = true
            tableViewTopAnchorActiveConstraint!.isActive = false
            tableViewTopAnchorActiveConstraint = eventTableView.topAnchor.constraint(equalTo: monthlyCalendarCollectionView.bottomAnchor,constant: -80)
            tableViewTopAnchorActiveConstraint!.isActive = true
            if currentDateLabel.text != CalendarFunctions().monthYearString(date: Date()){
                todayButton.isHidden = false
            }else{
                todayButton.isHidden = true
            }
            weeklyCalendarView = false
        }
    }
    
    @objc func addEvents(){
     navigationItem.rightBarButtonItem?.tintColor = themeColor
        let rootVC = EventFormViewController()
        let navVC = UINavigationController(rootViewController: rootVC)
        navVC.modalPresentationStyle = .pageSheet
        present(navVC, animated: true)
    }
    
    func setUpWeekDayConstraints(){
        view.addSubview(weekDayStack)
        weekDayStack.translatesAutoresizingMaskIntoConstraints = false
        weekDayStack.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 50).isActive = true
        weekDayStack.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 10).isActive = true
        weekDayStack.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,constant: -10).isActive = true
    }
    
    func setupLongGestureRecognizerOnCollection() {
        let longPressedGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gestureRecognizer:)))
        let longPressedGesture1 = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress(gestureRecognizer:)))
        longPressedGesture.minimumPressDuration = 0.5
        longPressedGesture.delegate = self
        longPressedGesture.delaysTouchesBegan = true
        monthlyCalendarCollectionView.addGestureRecognizer(longPressedGesture)
        weeklyCalendarCollectionView.addGestureRecognizer(longPressedGesture1)

    }
    
    @objc func handleLongPress(gestureRecognizer: UILongPressGestureRecognizer) {
        if (gestureRecognizer.state != .began) {
            return
        }

        let longPress = gestureRecognizer.location(in: monthlyCalendarCollectionView)
        let longPress1 = gestureRecognizer.location(in: weeklyCalendarCollectionView)

        if let indexPath = monthlyCalendarCollectionView.indexPathForItem(at: longPress) {
            if numberOfCellsInMonthlyCalendar[indexPath.item] != ""{
               let defaultSelectionDate = CalendarFunctions().stringToDate(date: (numberOfCellsInMonthlyCalendar[indexPath.item] + currentDateLabel.text!))
                let selectedDate = CalendarFunctions().currentDate(date: defaultSelectionDate)
                selectedrotineDate = CalendarFunctions().stringToDate(date: selectedDate)

                let rootVC = RoutinesListViewController()
                let navVC = UINavigationController(rootViewController: rootVC)
                navVC.modalPresentationStyle = .pageSheet
            if let sheet = navVC.sheetPresentationController {
                   sheet.detents = [.medium(), .large()]
               }
            present(navVC, animated: true)
        }
    }
        if weeklyCalendarView{
            if let indexPath = weeklyCalendarCollectionView.indexPathForItem(at: longPress1) {
                    let selectedDate = CalendarFunctions().currentDate(date: numberOfCellsInWeeklyCalendar[indexPath.item])
                    selectedrotineDate = CalendarFunctions().stringToDate(date: selectedDate)
                    let rootVC = RoutinesListViewController()
                let navVC = UINavigationController(rootViewController: rootVC)
                navVC.modalPresentationStyle = .pageSheet
                if let sheet = navVC.sheetPresentationController {
                       sheet.detents = [.medium(), .large()]
                   }
                present(navVC, animated: true)
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == weeklyCalendarCollectionView{
            return numberOfCellsInWeeklyCalendar.count
        }
        return numberOfCellsInMonthlyCalendar.count
    }

    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let model = events.filter{ filtered in
            return filtered.templateName == nil
        }
        let dates = model.map { $0.date }
        var filteredDates = Array(Set(dates))
        filteredDates.sort(){$0! < $1!}
        let templateEvent = events.filter{ filtered in
            return filtered.templateName != nil
        }
        var templateDate = templateEvent.map {$0.date}
        templateDate.sort(){$0! < $1!}
        
        if collectionView == weeklyCalendarCollectionView{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "calCell1", for: indexPath) as! CalendarCell
            
            let date = numberOfCellsInWeeklyCalendar[indexPath.item]
            cell.day.text = (CalendarFunctions().getTodayWeekDay(date: date))
            cell.dayOfMonth.text = String(CalendarFunctions().dayOfMonth(date: date))
            cell.layer.borderColor = themeColor.cgColor
            cell.layer.borderWidth = 2
            cell.layer.cornerRadius = 10
            if(date == selectedDateInMonthView)
            {
               
                cell.layer.borderColor = UIColor.label.cgColor
            }
            else
            {
                cell.layer.borderColor = themeColor.cgColor
            }
            
            let currentDate = CalendarFunctions().currentDate(date: Date())
            let cellDate = CalendarFunctions().currentDate(date: numberOfCellsInWeeklyCalendar[indexPath.item])
        
            if currentDate == cellDate{
                
                cell.day.textColor = themeColor
                cell.dayOfMonth.textColor = themeColor
            }else{
                cell.day.textColor = .label
                cell.dayOfMonth.textColor = .label
            }
            
            
            let dateFormat = filteredDates.map{ CalendarFunctions().currentDate(date: $0!)}
            let currentDate1 = CalendarFunctions().currentDate(date: date)
                if dateFormat.contains(currentDate1){
                    cell.eventDot.text = "."
                    cell.eventDot.font = UIFont.preferredFont(forTextStyle: .title1)
                    cell.eventDot.topAnchor.constraint(equalTo: cell.dayOfMonth.bottomAnchor,constant: -22).isActive = true

                }else{
                    cell.eventDot.text = ""
                }
           
            if templateDate.contains(numberOfCellsInWeeklyCalendar[indexPath.item]){
                
                        cell.backgroundColor = themeColor
                        cell.layer.cornerRadius = 10
                        cell.day.textColor = .label
                cell.dayOfMonth.textColor = .label
                }else{
                    cell.backgroundColor = .systemBackground
                }
            

            return cell
        }else{
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "calCell", for: indexPath) as! MonthlyCalendarCollectionViewCell
        
            cell.day.text = numberOfCellsInMonthlyCalendar[indexPath.item]
            var date = Date()
            if numberOfCellsInMonthlyCalendar[indexPath.item] != ""{
                 date = CalendarFunctions().stringToDate(date: (numberOfCellsInMonthlyCalendar[indexPath.item] + currentDateLabel.text!))
                
            }
            let today = CalendarFunctions().currentDate(date: date)

            if numberOfCellsInMonthlyCalendar[indexPath.item] != ""{

            if today == CalendarFunctions().currentDate(date: selectedDateInMonthView){
                lastSelectedIndex = indexPath
                cell.isSelected = (lastSelectedIndex == indexPath)

            }else{
                cell.isSelected = false
            }
            }else{
                cell.isSelected = false
            }
            let todayDate = CalendarFunctions().currentDate(date: Date())
           
            let dateFormatter3 = DateFormatter()
            dateFormatter3.dateFormat = "dd"
            let todayDayNumber = dateFormatter3.string(from: date)

              
            if numberOfCellsInMonthlyCalendar[indexPath.item] != ""{
                if today == todayDate{
                    if Int(todayDayNumber) == Int(numberOfCellsInMonthlyCalendar[indexPath.item]){
                        cell.day.textColor = themeColor

                    }
                }else{
                    cell.day.textColor = .label
  
                }
            }
            if numberOfCellsInMonthlyCalendar[indexPath.item] != ""{
                let dateFormat = filteredDates.map{ CalendarFunctions().currentDate(date: $0!)}
                let currentDate1 = CalendarFunctions().currentDate(date: date)
                    if dateFormat.contains(currentDate1){
                        cell.dayOfMonth.text = "."
                        cell.dayOfMonth.font = UIFont.preferredFont(forTextStyle: .title1)
                        cell.dayOfMonth.topAnchor.constraint(equalTo: cell.day.bottomAnchor,constant: -20).isActive = true
                    }else{
                        cell.dayOfMonth.text = ""
                    }
            }else{
                cell.dayOfMonth.text = ""

            }
                if templateDate.contains(date){
                        cell.backgroundColor = themeColor
                        cell.layer.cornerRadius = 10
                        cell.day.textColor = .label
                }else{
                    cell.backgroundColor = .systemBackground
                }
      
            return cell
        }
        
       
    }
    @objc func previousMonth(_ sender: Any)
    {
        let year = Calendar.current.component(.year, from: Date())
        let  firstOfNextYear = Calendar.current.date(from: DateComponents(year: year - 2, month: 1, day: 0))
        if firstOfNextYear! <= CalendarFunctions().minusMonth(date: selectedDateInMonthView){
        selectedDateInMonthView = CalendarFunctions().minusMonth(date: selectedDateInMonthView)
        setMonthView()
        eventTableView.reloadData()
        }
    }
    
    @objc func nextMonth(_ sender: Any)
    {
        let year = Calendar.current.component(.year, from: Date())
        let  firstOfNextYear = Calendar.current.date(from: DateComponents(year: year + 2, month: 1, day: 0))
        if firstOfNextYear! >= CalendarFunctions().addMonths(date: selectedDateInMonthView, days: 1){
            selectedDateInMonthView = CalendarFunctions().addMonths(date: selectedDateInMonthView, days: 1)
            setMonthView()
            eventTableView.reloadData()

        }
    }

    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {

        if !weeklyCalendarCollectionView.isHidden{
            let visibleCells = weeklyCalendarCollectionView.indexPathsForVisibleItems
                    visibleCells.forEach { indexPath in
                if let cell = weeklyCalendarCollectionView.cellForItem(at: indexPath), weeklyCalendarCollectionView.bounds.contains(cell.frame) {
                    let scrolledCell = numberOfCellsInWeeklyCalendar[indexPath.row]

                     weeklyCalendarMonthLabel.text = CalendarFunctions().monthString(date: scrolledCell)
                        + " " + CalendarFunctions().yearString(date: scrolledCell)
                    
                    
                    if CalendarFunctions().monthYearString(date: Date()) != weeklyCalendarMonthLabel.text{
                        todayButton.isHidden = false
                    }
                    else{
                        todayButton.isHidden = true
                    }

                }
            }
        }
    }
    
    
    var numberOfCellsInMonthlyCalendar = [String]()
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
   {
       if collectionView == weeklyCalendarCollectionView{
           let width = (collectionView.frame.size.width - 10) / 8
           let height = (collectionView.frame.size.height - 50)

            return CGSize(width: width, height: height)

       }else{
           let width = (collectionView.frame.size.width ) / 9
           let height = (collectionView.frame.size.height ) / 10
           return CGSize(width: width, height: height)

       }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 7
    }
    
    func setMonthView()
    {
        numberOfCellsInMonthlyCalendar.removeAll()
        let daysInMonth = CalendarFunctions().daysInMonth(date: selectedDateInMonthView)
        let firstDayOfMonth = CalendarFunctions().firstOfMonth(date: selectedDateInMonthView)
        let startingSpaces = CalendarFunctions().weekDay(date: firstDayOfMonth)
        
        var count: Int = 1
        
        while(count <= 42)
        {
            if(count <= startingSpaces || count - startingSpaces > daysInMonth)
            {
                numberOfCellsInMonthlyCalendar.append("")
            }
            else
            {
                numberOfCellsInMonthlyCalendar.append(String(count - startingSpaces))
            }
            count += 1
        }
        currentDateLabel.text = CalendarFunctions().monthString(date: selectedDateInMonthView)
            + " " + CalendarFunctions().yearString(date: selectedDateInMonthView)
        currentDateLabel.fadeTransition(0.5)
        if currentDateLabel.text != CalendarFunctions().monthYearString(date: Date()){
            todayButton.isHidden = false
        }else{
            todayButton.isHidden = true
        }
        monthlyCalendarCollectionView.reloadData()

    }

    func setupCollectionConstraints() {
        
        view.addSubview(monthlyCalendarCollectionView)
        view.addSubview(eventTableView)
        view.addSubview(weeklyCalendarCollectionView)

        monthlyCalendarCollectionView.delegate = self
        monthlyCalendarCollectionView.dataSource = self
        monthlyCalendarCollectionView.register(MonthlyCalendarCollectionViewCell.self,forCellWithReuseIdentifier: "calCell")
        monthlyCalendarCollectionView.isPrefetchingEnabled = true
        monthlyCalendarCollectionView.translatesAutoresizingMaskIntoConstraints = false
        monthlyCalendarCollectionView.topAnchor.constraint(equalTo: weekDayStack.bottomAnchor, constant: 10).isActive = true
        monthlyCalendarCollectionView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 5).isActive = true
        monthlyCalendarCollectionView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,constant: -5).isActive = true
        monthlyCalendarCollectionView.heightAnchor.constraint(equalTo: view.heightAnchor,multiplier : 2/5).isActive = true

        weeklyCalendarCollectionView.delegate = self
        weeklyCalendarCollectionView.dataSource = self
        weeklyCalendarCollectionView.register(CalendarCell.self,forCellWithReuseIdentifier: "calCell1")
        weeklyCalendarCollectionView.isPrefetchingEnabled = true
        weeklyCalendarCollectionView.translatesAutoresizingMaskIntoConstraints = false
        weeklyCalendarCollectionView.topAnchor.constraint(equalTo: currentDateLabel.bottomAnchor, constant: 10).isActive = true
        weeklyCalendarCollectionView.heightAnchor.constraint(equalToConstant: 100).isActive = true
        weeklyCalendarCollectionView.leadingAnchor.constraint(equalTo: view.leadingAnchor,constant: 10).isActive = true
        weeklyCalendarCollectionView.trailingAnchor.constraint(equalTo: view.trailingAnchor,constant: -10).isActive = true
        weeklyCalendarCollectionView.showsHorizontalScrollIndicator = false

    }

    var lastSelectedIndex:IndexPath?

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if !weeklyCalendarCollectionView.isHidden{

            selectedDateInMonthView = numberOfCellsInWeeklyCalendar[indexPath.item]
            if selectedDateInMonthView != Date(){
                todayButton.isHidden = false
            }else{
                todayButton.isHidden = true
            }
            weeklyCalendarCollectionView.reloadData()
            eventTableView.reloadData()
        }else{
            if numberOfCellsInMonthlyCalendar[indexPath.item] != ""{
              let cell = collectionView.cellForItem(at: indexPath) as! MonthlyCalendarCollectionViewCell
              cell.isSelected = true
              lastSelectedIndex = indexPath
            
            }
            
                collectionView.reloadData()
                if numberOfCellsInMonthlyCalendar[indexPath.item] != ""{
                let date = CalendarFunctions().getSelectedDate(date: (String(numberOfCellsInMonthlyCalendar[indexPath.item]) + currentDateLabel.text!))
                    if CalendarFunctions().currentDate(date: date) == CalendarFunctions().currentDate(date: Date()){
                        selectedDateInMonthView = Date().nearestMinute()
                    }else{
                        selectedDateInMonthView = date

                    }
                eventTableView.reloadData()
            }
        }
    }
    var tags = [Tags]()
    func getAllTags(){
    do{
        let request = Tags.fetchRequest() as NSFetchRequest<Tags>
        tags = try context.fetch(request)
     }catch {
         print(error)
        }

    }
}

extension MonthlyCalendarViewController : UITableViewDelegate, UITableViewDataSource {

    func setUpTableViewConstraints(){
       
        eventTableView.translatesAutoresizingMaskIntoConstraints = false
        eventTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 0).isActive = true
        eventTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,constant: 0).isActive = true
        eventTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,constant: -10).isActive = true
        eventTableView.autoresizingMask = .flexibleWidth
        eventTableView.delegate = self
        eventTableView.dataSource = self
        eventTableView.estimatedRowHeight = 200
        eventTableView.showsVerticalScrollIndicator = false
        eventTableView.rowHeight = UITableView.automaticDimension
        eventTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        eventTableView.reloadData()
    }
    @objc func buttonDidChanged(_ segmentedControl : UISegmentedControl){
        switch segmentedControl.selectedSegmentIndex{
        case 0:
            let selected = CalendarFunctions().currentDate(date: selectedDateInMonthView)

            filterEvents = 1
            items = events.filter { filtered in
               let date = CalendarFunctions().currentDate(date: filtered.date!)
               return date == selected
                 }
            eventTableView.reloadData()
            break
        case 1:
            let selected = CalendarFunctions().currentDate(date: selectedDateInMonthView)

            filterEvents = 2
            items = events.filter { filtered in
               let date = CalendarFunctions().currentDate(date: filtered.date!)
               return date == selected && filtered.templateName == nil
                 }
            eventTableView.reloadData()
            break
        case 2:
            let selected = CalendarFunctions().currentDate(date: selectedDateInMonthView)

            filterEvents = 3
            items = events.filter { filtered in
               let date = CalendarFunctions().currentDate(date: filtered.date!)
               return date == selected && filtered.templateName != nil
                 }
            eventTableView.reloadData()
            break
        default:
            print("")
        }
    }
    func eventsFilteration(){
        let selected = CalendarFunctions().currentDate(date: selectedDateInMonthView)
        if filterEvents == 1 {
             items = events.filter { filtered in
                let date = CalendarFunctions().currentDate(date: filtered.date!)
                return date == selected
                  }
        }else if filterEvents == 2
    {
             items = events.filter { filtered in
                let date = CalendarFunctions().currentDate(date: filtered.date!)
                return date == selected && filtered.templateName == nil
                  }

        }else{
             items = events.filter { filtered in
                let date = CalendarFunctions().currentDate(date: filtered.date!)
                return date == selected && filtered.templateName != nil
                  }
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        eventsFilteration()
        if items.count == 0{
            noEventsLabel.isHidden = false
            noEventsMessage.isHidden = false
        }else{
            noEventsLabel.isHidden = true
            noEventsMessage.isHidden = true
        }
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellID") as! CustomEventsTableViewCell
            cell.selectionStyle = .none
            eventsFilteration()
            let selectedDate = CalendarFunctions().getTimeString(date: items[indexPath.row].startTime!)
            let endTime = CalendarFunctions().getTimeString(date: items[indexPath.row].endTime!)
            cell.eventStartTime.text = selectedDate + " - "
            cell.eventEndTime.text = endTime
           
            cell.eventTitle.text = items[indexPath.row].title
            let tagList = tags.filter { filtered in
                   return items[indexPath.row].tag == filtered.name
            }
            cell.eventIcon.image = UIImage(systemName: "")

            cell.eventTag.text = items[indexPath.row].tag
            
            if  tagList.count != 0{
                cell.eventTag.backgroundColor = tagList[0].color

            }else{
                cell.eventTag.backgroundColor = themeColor
            }
            cell.eventTag.layer.cornerRadius = 25
            cell.eventTag.textColor = .black
            cell.eventTag.layer.masksToBounds = true
            cell.eventTag.layer.cornerRadius = 15
        
            if items[indexPath.row].templateName != nil{
                cell.eventIcon.image = UIImage(systemName: "repeat.circle")
                cell.eventIcon.tintColor = themeColor

            }else{
                cell.eventIcon.image = UIImage(systemName: "clock")
                cell.eventIcon.tintColor = themeColor

            }
           return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
 
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        eventsFilteration()
        navigationController?.pushViewController(DetailedEventViewController(event: items[indexPath.row]), animated: true)
   }
    
        func getAllEvents(){
           do{
               let request = Events.fetchRequest() as NSFetchRequest<Events>
               events = try context.fetch(request)
           }catch {
               print(error)
           }
        }
}
extension UIView {
    func fadeTransition(_ duration:CFTimeInterval) {
        let animation = CATransition()
        animation.timingFunction = CAMediaTimingFunction(name:
            CAMediaTimingFunctionName.easeInEaseOut)
        animation.type = CATransitionType.fade
        animation.duration = duration
        layer.add(animation, forKey: CATransitionType.fade.rawValue)
    }
}
