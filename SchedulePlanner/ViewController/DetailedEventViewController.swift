//
//  DetailedEventViewController.swift
//  SchedulePlannerAppPlanner
//
//  Created by praveena-pt4972 on 14/06/22.
//

import UIKit
import CoreData

class DetailedEventViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    public var event : Events
    init(event : Events){
        self.event = event
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    let detailedEventFormTableView : UITableView = {
        let detailedEventFormTableView = UITableView(frame: CGRect.zero, style: .insetGrouped)
        detailedEventFormTableView.autoresizingMask = .flexibleWidth
        detailedEventFormTableView.translatesAutoresizingMaskIntoConstraints = false
        return detailedEventFormTableView
    }()
    
    var events = [Events]()
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .secondarySystemBackground
        detailedEventFormTableView.register(CustomDetailedHeadingCellTableViewCell.self, forCellReuseIdentifier: "cell")
        detailedEventFormTableView.register(CustomTitleTableViewCell.self, forCellReuseIdentifier: "title")
        detailedEventFormTableView.register(TimeTableViewCell.self, forCellReuseIdentifier: "time")
        detailedEventFormTableView.register(ReminderTableViewCell.self, forCellReuseIdentifier: "bell")
        detailedEventFormTableView.register(NotesDetailTableViewCell.self, forCellReuseIdentifier: "notes")
        view.addSubview(detailedEventFormTableView)
        detailedEventFormTableView.delegate = self
        detailedEventFormTableView.dataSource = self
        detailedEventFormTableView.frame = view.bounds
        detailedEventFormTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        detailedEventFormTableView.rowHeight = UITableView.automaticDimension
        getAllEvents()
        configureBars()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadDetailedTable), name: Notification.Name("UpdateEventSaved"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadDetailedTable), name: Notification.Name("tagSaved"), object: nil)
        
    }
    @objc func reloadDetailedTable(){
        
        getAllEvents()
        detailedEventFormTableView.reloadData()
    }

    
    func configureBars(){
        let delete = UIImage(systemName: "trash")
        let edit = UIImage(systemName: "square.and.pencil")
         let deleteButton = UIBarButtonItem(image: delete, style: .plain, target: self, action: #selector(deleteEvent))
         let editButton = UIBarButtonItem(image: edit, style: .plain, target: self, action: #selector(editEvent))
         self.navigationItem.rightBarButtonItems = [deleteButton,editButton]
         navigationItem.rightBarButtonItem?.tintColor = themeColor
    }
    
    
    @objc func deleteEvent(item: Events){
        let actionsheet = UIAlertController(
            title: "",
            message: "Are you sure you want to delete this event?",
            preferredStyle: .actionSheet)
            actionsheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            actionsheet.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { _ in
                self.deleteItem(item: self.event)
            }))
            actionsheet.popoverPresentationController?.sourceView = self.view
            self.present(actionsheet,animated: true)
    }
    func deleteItem(item: Events){
        context.delete(item)
        do{
            try context.save()
            schedulingNotification()
            NotificationCenter.default.post(name: Notification.Name("EventSaved"), object: nil)
            self.navigationController?.popViewController(animated: true)
        }catch{
            print(error)
        }
    }
    @objc func editEvent(){
        let rootVC = EditEventFormViewController(event: event)
        let navVC = UINavigationController(rootViewController: rootVC)
        navVC.modalPresentationStyle = .pageSheet
        present(navVC, animated: true)
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        getExistingTags()
        
    
        if indexPath.row == 0{

            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! CustomDetailedHeadingCellTableViewCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.titleLabel.text = CalendarFunctions().getDateFormat(date: event.date!)
            cell.tagLabel.text = event.tag!.trimmingCharacters(in: .whitespaces)
           cell.tagLabel.layer.borderColor = UIColor.black.cgColor
           if  tagFilter.count != 0{
               cell.tagLabel.backgroundColor = tagFilter[0].color
   
           }else{
               cell.tagLabel.backgroundColor = themeColor
           }
           cell.tagLabel.layer.cornerRadius = 25
           cell.tagLabel.textColor = .black
           cell.tagLabel.layer.masksToBounds = true
           cell.tagLabel.layer.cornerRadius = 15
            return cell

            
    
        }else if indexPath.row == 1{
            let cell = tableView.dequeueReusableCell(withIdentifier: "time", for: indexPath) as! TimeTableViewCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            
            let startTime = CalendarFunctions().getTimeString(date: event.startTime!)
    
            let endTime = CalendarFunctions().getTimeString(date: event.endTime!)
            cell.timeLabel.text =  startTime
            cell.endtimeLabel.text = endTime
            if  tagFilter.count != 0{
                cell.timeLabel.backgroundColor = tagFilter[0].color
    
            }else{

                cell.timeLabel.backgroundColor = themeColor
            }
            cell.timeLabel.layer.cornerRadius = 25
            cell.timeLabel.textColor = .black
            cell.timeLabel.layer.masksToBounds = true
            cell.timeLabel.layer.cornerRadius = 15
            if  tagFilter.count != 0{
                cell.endtimeLabel.backgroundColor = tagFilter[0].color
    
            }else{

                cell.endtimeLabel.backgroundColor = themeColor
            }
            cell.endtimeLabel.layer.cornerRadius = 25
            cell.endtimeLabel.textColor = .black
            cell.endtimeLabel.layer.masksToBounds = true
            cell.endtimeLabel.layer.cornerRadius = 15
            if  tagFilter.count != 0{
                cell.endtimeLabel.backgroundColor = tagFilter[0].color
                cell.time.tintColor = tagFilter[0].color
                cell.arrow.tintColor = tagFilter[0].color

            }else{
                cell.time.tintColor = themeColor
                cell.arrow.tintColor = themeColor

                cell.endtimeLabel.backgroundColor = themeColor
            }
                        return cell


        }else if indexPath.row == 2{
            let cell = tableView.dequeueReusableCell(withIdentifier: "title", for: indexPath) as! CustomTitleTableViewCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.titleLabel.text = event.title!.trimmingCharacters(in: .whitespaces)
            if  tagFilter.count != 0{
                cell.titleIcon.tintColor = tagFilter[0].color

            }else{
                cell.titleIcon.tintColor = themeColor
            }
            return cell

            
            
        }else if indexPath.row == 3{
            let cell = tableView.dequeueReusableCell(withIdentifier: "bell", for: indexPath) as! ReminderTableViewCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            if  tagFilter.count != 0{
                cell.bell.tintColor = tagFilter[0].color

            }else{
                cell.bell.tintColor = themeColor
            }
            if event.remainder == nil {
               cell.reminderLabel.text = "No remainder"
//                cell.reminderLabel.isHidden = true
//                cell.bell.isHidden = true
           }else{
               let interval = event.remainder!.timeIntervalSince(event.startTime!)
               if interval/60 == -5.0 {
                   cell.reminderLabel.text = "5 minutes before"
   
               }else if interval/60 == -10.0{
                   cell.reminderLabel.text = "10 minutes before"
               }else{
                   cell.reminderLabel.text = "at the time of event"
               }
           }
                return cell

        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "notes", for: indexPath) as! NotesDetailTableViewCell
            cell.selectionStyle = UITableViewCell.SelectionStyle.none
            cell.notesLabel.text = event.notes!
          if cell.notesLabel.text == ""{
              cell.notesIcon.isHidden = true
          }else{
              cell.notesIcon.isHidden = false
              if tagFilter.count != 0{
                  cell.notesIcon.tintColor = tagFilter[0].color

              }else{
                  cell.notesIcon.tintColor = themeColor

              }
          }
            return cell

        }
        
    }
    
    var tagFilter = [Tags]()
    func getExistingTags(){
    do{
        let request = Tags.fetchRequest() as NSFetchRequest<Tags>
        if let tag = event.tag{
            let pred = NSPredicate(format: "name MATCHES[c] %@", tag)
            request.predicate = pred
        }
        tagFilter = try context.fetch(request)
     }catch {
         print(error)
        }
    }
    
    func getAllEvents(){
       do{
           let request = Events.fetchRequest() as NSFetchRequest<Events>
           events = try context.fetch(request)
       }catch {
             print(error)
       }

    }
}
