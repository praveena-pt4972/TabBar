//
//  SecondViewController.swift
//  TabBar
//
//  Created by praveena-pt4972 on 02/05/22.
//

import UIKit
import CoreData


class RoutinesViewController : UIViewController , UITableViewDelegate , UITableViewDataSource, UISearchResultsUpdating, UITextFieldDelegate{
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredData(for: searchController.searchBar.text ?? "")
    }
    
    let searchController = UISearchController(searchResultsController: nil)
    
     let templateTableView : UITableView = {
        let templateTableView = UITableView()
        templateTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        templateTableView.autoresizingMask = .flexibleWidth
        templateTableView.translatesAutoresizingMaskIntoConstraints = false
        templateTableView.sectionFooterHeight = 20
        return templateTableView
    }()
    let noRoutineMessageImage : UIImageView = {
        let noEventsMessage = UIImageView()
        let largeConfig = UIImage.SymbolConfiguration(pointSize: 50, weight: .light, scale: .large)
        noEventsMessage.image = UIImage(systemName: "calendar.badge.exclamationmark", withConfiguration: largeConfig)
        noEventsMessage.translatesAutoresizingMaskIntoConstraints = false
        noEventsMessage.tintColor = .placeholderText
        return noEventsMessage
    }()
    let noRoutineLabel: UILabel = {
        let noEventsLabel = UILabel()
        noEventsLabel.text = "No Routines Found"
        noEventsLabel.textColor = .placeholderText
        noEventsLabel.font = UIFont.preferredFont(forTextStyle: .headline, compatibleWith: .none)
        noEventsLabel.translatesAutoresizingMaskIntoConstraints = false
        return noEventsLabel
    }()
    var filteredData = [Template]()
    var models = [Template]()

    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Routines"
        view.backgroundColor = .systemBackground
        navigationController?.navigationBar.prefersLargeTitles = true
        view.addSubview(templateTableView)
        getAllTemplates()
        templateTableView.delegate = self
        templateTableView.dataSource = self
        templateTableView.showsVerticalScrollIndicator = false
        templateTableView.translatesAutoresizingMaskIntoConstraints = false
        templateTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        templateTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 20).isActive = true
        templateTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: -20).isActive = true
        templateTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        templateTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        searchController.searchResultsUpdater = self
        definesPresentationContext = true
        configureBarItems()
        view.addSubview(noRoutineMessageImage)
        noRoutineMessageImage.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 180).isActive = true
        noRoutineMessageImage.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 130).isActive = true
        noRoutineMessageImage.widthAnchor.constraint(equalTo: view.widthAnchor,multiplier: 2/5).isActive = true
        noRoutineMessageImage.heightAnchor.constraint(equalTo: view.heightAnchor,multiplier: 1/5).isActive = true
        view.addSubview(noRoutineLabel)
        noRoutineLabel.topAnchor.constraint(equalTo: noRoutineMessageImage.bottomAnchor,constant: 10).isActive = true
        noRoutineLabel.leadingAnchor.constraint(equalTo: noRoutineMessageImage.leadingAnchor,constant: 10).isActive = true
        noRoutineLabel.isHidden = true
        noRoutineLabel.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(reloadRoutine), name: Notification.Name("routineSaved"), object: nil)
        
    }
    @objc func reloadRoutine(){
        getAllTemplates()
        templateTableView.reloadData()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            if self.filteredData.count == 0{
                noRoutineLabel.isHidden = false
                noRoutineMessageImage.isHidden = false
            }else{
                noRoutineLabel.isHidden = true
                noRoutineMessageImage.isHidden = true
            }
            return self.filteredData.count
        } else if searchController.isActive && searchController.searchBar.text == ""{
            noRoutineLabel.isHidden = false
            noRoutineMessageImage.isHidden = false
            return 0
        }
        if models.count == 0{
            noRoutineLabel.isHidden = false
            noRoutineMessageImage.isHidden = false
        }else{
            noRoutineLabel.isHidden = true
            noRoutineMessageImage.isHidden = true
        }
        return models.count
    }
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == models.count - 1{
            return 0
        }else{
            return 15
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if searchController.isActive && searchController.searchBar.text != "" {
            let items = filteredData[indexPath.section]
            navigationController?.pushViewController(RoutineEventsViewController(name: items.templateName ?? "" ,noOfDays: items.noOfDays, includeWeekend: items.includeWeekend ), animated: true)

        }else{
            let items = models[indexPath.section]
            navigationController?.pushViewController(RoutineEventsViewController(name: items.templateName ?? "" ,noOfDays: items.noOfDays, includeWeekend: items.includeWeekend ), animated: true)
            navigationController?.navigationBar.tintColor = themeColor
        }

    }
    
    
    func tableView(_ tableView: UITableView, didHighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        cell?.contentView.layer.borderColor = UIColor.label.cgColor
        cell?.contentView.layer.borderWidth = 2
        tableView.deselectRow(at: indexPath, animated: true)
    }

    func tableView(_ tableView: UITableView, didUnhighlightRowAt indexPath: IndexPath) {
        let cell  = tableView.cellForRow(at: indexPath)
        cell?.contentView.layer.borderColor = themeColor.cgColor
        cell?.contentView.layer.borderWidth = 2
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 80
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        cell.contentView.frame = cell.contentView.frame.inset(by: UIEdgeInsets(top: 5, left: 10, bottom: 20, right: 10))
        cell.layer.cornerRadius = 15
        cell.contentView.layer.borderWidth = 2
        cell.contentView.layer.borderColor = themeColor.cgColor
        cell.contentView.layer.cornerRadius = 15
        var model = models[indexPath.section]
        var string = String(model.noOfDays)

        if searchController.isActive && searchController.searchBar.text != "" {
          model = filteredData[indexPath.section]
            string = String(filteredData[indexPath.section].noOfDays)
        }else{
            model = models[indexPath.section]

        }
        
        cell.textLabel?.text = "\(model.templateName!.trimmingCharacters(in: .whitespaces))"
        if string == "1"{
            cell.detailTextLabel?.text = "\(string) Day"

        }else{
            cell.detailTextLabel?.text = "\(string) Days"

        }
        return cell
    }
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            let allowedCharacters = CharacterSet(charactersIn:"+0123456789 ")//Here change this characters based on your requirement
            let characterSet = CharacterSet(charactersIn: string)
            return allowedCharacters.isSuperset(of: characterSet)
        
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
            -> UISwipeActionsConfiguration? {
            let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
                let item = self.models[indexPath.section]
                //let item2 = self.filteredData[indexPath.section]
                let actionsheet = UIAlertController(title: "",message: "Are you sure you want to delete this event?",preferredStyle: .actionSheet)
                if self.searchController.isActive && self.searchController.searchBar.text != "" {
                        actionsheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                        actionsheet.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { _ in
                                    let item2 = self.filteredData[indexPath.section]
                            self.filteredData.remove(at: indexPath.section)
                                    self.deleteItem(item: item2)
                    }))

                } else {
                    actionsheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
                    actionsheet.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { _ in
                        self.models.remove(at: indexPath.section)
                        self.deleteItem(item: item)
                                         }))
                                     }
                    actionsheet.popoverPresentationController?.sourceView = self.view
                    self.present(actionsheet,animated: true)
                    completionHandler(true)
                
            }
            let updateAction = UIContextualAction(style: .normal, title: nil){ (_, _, completionHandler) in
                let item = self.models[indexPath.section]
                    let alert = UIAlertController(title: "Edit Template", message: "Enter New Template Name", preferredStyle: .alert)
                    alert.addTextField{ fields in
                            fields.placeholder = "Enter your Template Name"
                            fields.returnKeyType = .next
                        
                    }
                    alert.addTextField{ field in
                            field.placeholder = "Number of Days"
                            field.returnKeyType = .continue
                            field.keyboardType = .numberPad
                            field.delegate = self
                        }
                
                    alert.addAction(UIAlertAction(title: "Save", style: .cancel, handler: { [weak self] _ in
                    
                        guard let fields = alert.textFields, fields.count == 2 ,  let newName = fields[0].text?.capitalized , !newName.trimmingCharacters(in: .whitespaces).isEmpty , let newNoOfDays = fields[1].text, !newNoOfDays.isEmpty , Int(newNoOfDays)! <= 7 else{
                                        return
                                }
                
                    if self!.searchController.isActive && self!.searchController.searchBar.text != "" {
                            let item2 = self!.filteredData[indexPath.section]
                            self!.filteredData[indexPath.section].templateName = newName
                        
                        
                        if newName.trimmingCharacters(in: .whitespaces) != "" && Int(newNoOfDays)! <= 7 {
                            self?.updateItem(item : item2,newName: newName, newNoOfDays : Int64(newNoOfDays)!)

                        }
                    } else {
                        if newName.trimmingCharacters(in: .whitespaces) != "" && Int(newNoOfDays)! <= 7 {
                            self?.updateItem(item : item,newName: newName, newNoOfDays: Int64(newNoOfDays)!)

                        }else{
                            
                        }
                    }
                
                }))
                    self.present(alert, animated: true)
                }
                deleteAction.image = UIImage(systemName: "trash")?.withTintColor(themeColor, renderingMode: .alwaysOriginal)
                deleteAction.backgroundColor = .systemBackground
                updateAction.image = UIImage(systemName: "pencil")?.withTintColor(themeColor, renderingMode: .alwaysOriginal)
                updateAction.backgroundColor = .systemBackground
            let configuration = UISwipeActionsConfiguration(actions: [ deleteAction,updateAction])
            return configuration
    }
    
    func updateItem(item: Template, newName: String,newNoOfDays : Int64){

        getExistingRoutine(template : item)
        item.templateName = newName
        item.noOfDays =  newNoOfDays
        do{
            for i in 0..<eventsFilter.count{
                eventsFilter[i].templateName = newName
            }
            for i in 0..<templateEventsFilter.count{
                templateEventsFilter[i].templateName = newName
            }
            try context.save()
            NotificationCenter.default.post(name: Notification.Name("routineSaved"), object: nil)
        }catch{
            print(error)
        }
    }
   
    func deleteItem(item: Template){
        getExistingRoutine(template : item)
        context.delete(item)
        for i in 0..<eventsFilter.count{
            context.delete(eventsFilter[i])
        }
        for i in 0..<templateEventsFilter.count{
            context.delete(templateEventsFilter[i])
        }
        do{
            try context.save()
            NotificationCenter.default.post(name: Notification.Name("routineSaved"), object: nil)
            
        }catch{
            print(error)
        }
    }
    var eventsFilter = [Events]()
    var templateEventsFilter = [TemplateEvents]()
    
    func getExistingRoutine(template : Template){
        do{
            let requestEvents = Events.fetchRequest() as NSFetchRequest<Events>
            let predicate = NSPredicate(format: "templateName MATCHES[c] %@", template.templateName!)
            requestEvents.predicate = predicate
            eventsFilter = try context.fetch(requestEvents)
            let requestTemplateEvents = TemplateEvents.fetchRequest() as NSFetchRequest<TemplateEvents>
            requestTemplateEvents.predicate = predicate
            templateEventsFilter = try context.fetch(requestTemplateEvents)
         }catch {
             print(error)
         }
    }

    func configureBarItems(){
        
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(goToTemplateForm))
        navigationItem.rightBarButtonItem?.tintColor = themeColor
        navigationController?.navigationBar.tintColor = themeColor
        navigationItem.searchController = searchController
    }
    
    private func filteredData(for searchText: String) {
        filteredData = models.filter { filtered in
        return
            filtered.templateName!.trimmingCharacters(in: .whitespaces).lowercased().contains(searchText.lowercased().trimmingCharacters(in: .whitespaces))
      }
        templateTableView.reloadData()
    }
    
   @objc func goToTemplateForm(){
       let rootVC = TemplateFormViewController()
       let navVC = UINavigationController(rootViewController: rootVC)
       navVC.modalPresentationStyle = .pageSheet
       present(navVC, animated: true)
    }
    
    func getAllTemplates(){
       do{
           let request = Template.fetchRequest() as NSFetchRequest<Template>
           let sort = NSSortDescriptor(key: "date", ascending: false)
           request.sortDescriptors = [sort]
           models = try context.fetch(request)
       }catch {
           print(error)
       }
    }
}


extension UITextField {

    open override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        return action == #selector(UIResponderStandardEditActions.cut) || action == #selector(UIResponderStandardEditActions.copy)
    }
}
