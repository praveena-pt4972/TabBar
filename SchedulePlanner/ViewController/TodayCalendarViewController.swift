import UIKit
import CoreData

class TodayCalendarViewController: UIViewController ,UIScrollViewDelegate{
    
    let currentDate = CalendarFunctions().currentDate(date: Date())
    let nextDay = CalendarFunctions().currentDate(date: CalendarFunctions().addDays(date: Date(), days: 1))
    let yesterday = CalendarFunctions().currentDate(date: CalendarFunctions().addDays(date: Date(), days: -1))

        let userDefaults = UserDefaults.standard
        let ON_OFF_KEY = "onOffKey"
        let window = UIApplication.shared.windows[0]

        static let switchOnOff = UISwitch()
        lazy var views : [UIView] = []
        func viewCreation(){
            for _ in 0..<3{
                lazy var vc : UIView = {
                    let vc = UIView()
                    return vc
                }()
                views.append(vc)
            }
        }
    
    var currentDateLabel : UILabel = {
        let currentDateLabel = UILabel()
        currentDateLabel.translatesAutoresizingMaskIntoConstraints = false
        currentDateLabel.textAlignment = .center
        currentDateLabel.font = UIFont.systemFont(ofSize: 25)
        return currentDateLabel
        
    }()
    
    
    let todayEventsTableView : UITableView = {
        let todayEventsTableView = UITableView()
        todayEventsTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        todayEventsTableView.autoresizingMask = .flexibleWidth
        todayEventsTableView.translatesAutoresizingMaskIntoConstraints = false
        return todayEventsTableView
   }()
   
    let yesterdayEventsTableView : UITableView = {
       let yesterdayEventsTableView = UITableView()
        yesterdayEventsTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        yesterdayEventsTableView.autoresizingMask = .flexibleWidth
        yesterdayEventsTableView.translatesAutoresizingMaskIntoConstraints = false
       return yesterdayEventsTableView
   }()
   
    let tomorrowEventsTableView : UITableView = {
       let tomorrowEventsTableView = UITableView()
        tomorrowEventsTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        tomorrowEventsTableView.autoresizingMask = .flexibleWidth
        tomorrowEventsTableView.translatesAutoresizingMaskIntoConstraints = false
       return tomorrowEventsTableView
   }()
    lazy var eventTableViews : [UITableView] = [yesterdayEventsTableView,todayEventsTableView,tomorrowEventsTableView]
    
    let noEventsMessage : UIImageView = {
        let noEventsMessage = UIImageView()
        let largeConfig = UIImage.SymbolConfiguration(pointSize: 50, weight: .light, scale: .large)
        noEventsMessage.image = UIImage(systemName: "calendar.badge.exclamationmark", withConfiguration: largeConfig)
        noEventsMessage.translatesAutoresizingMaskIntoConstraints = false
        noEventsMessage.tintColor = .placeholderText
        return noEventsMessage
    }()
    let noEventsLabel: UILabel = {
        let noEventsLabel = UILabel()
        noEventsLabel.text = "No Events Found"
        noEventsLabel.textColor = .placeholderText
        noEventsLabel.font = UIFont.preferredFont(forTextStyle: .headline, compatibleWith: .none)
        noEventsLabel.translatesAutoresizingMaskIntoConstraints = false
        return noEventsLabel
    }()
    let noEventsMessage1 : UIImageView = {
        let noEventsMessage = UIImageView()
        let largeConfig = UIImage.SymbolConfiguration(pointSize: 50, weight: .light, scale: .large)
        noEventsMessage.image = UIImage(systemName: "calendar.badge.exclamationmark", withConfiguration: largeConfig)
        noEventsMessage.translatesAutoresizingMaskIntoConstraints = false
        noEventsMessage.tintColor = .placeholderText
        return noEventsMessage
    }()
    let noEventsLabel1: UILabel = {
        let noEventsLabel = UILabel()
        noEventsLabel.text = "No Events Found"
        noEventsLabel.textColor = .placeholderText
        noEventsLabel.font = UIFont.preferredFont(forTextStyle: .headline, compatibleWith: .none)
        noEventsLabel.translatesAutoresizingMaskIntoConstraints = false
        return noEventsLabel
    }()

    let noEventsMessage2 : UIImageView = {
        let noEventsMessage = UIImageView()
        let largeConfig = UIImage.SymbolConfiguration(pointSize: 50, weight: .light, scale: .large)
        noEventsMessage.image = UIImage(systemName: "calendar.badge.exclamationmark", withConfiguration: largeConfig)
        noEventsMessage.translatesAutoresizingMaskIntoConstraints = false
        noEventsMessage.tintColor = .placeholderText
        return noEventsMessage
    }()
    let noEventsLabel2: UILabel = {
        let noEventsLabel = UILabel()
        noEventsLabel.text = "No Events Found"
        noEventsLabel.textColor = .placeholderText
        noEventsLabel.font = UIFont.preferredFont(forTextStyle: .headline, compatibleWith: .none)
        noEventsLabel.translatesAutoresizingMaskIntoConstraints = false
        return noEventsLabel
    }()
    var events = [Events]()
    
    lazy var scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.isPagingEnabled = true
        viewCreation()
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(views.count), height: view.frame.height - 300)
    
        for i in 0..<views.count{
            if i == 0{
                let yesterdayLabel = UILabel()
                yesterdayLabel.font = UIFont.preferredFont(forTextStyle: .title1)
                yesterdayLabel.text = "Yesterday"
                
                let YesterdayLabelStack : UIStackView = {
                    let YesterdayLabelStack = UIStackView()
                    YesterdayLabelStack.axis = .horizontal
                    YesterdayLabelStack.distribution = .equalSpacing
                    YesterdayLabelStack.spacing = 20
                    YesterdayLabelStack.translatesAutoresizingMaskIntoConstraints = false
                    return YesterdayLabelStack
                }()
                let rightArrow : UIImageView = {
                    let rightArrow = UIImageView()
                    rightArrow.image = UIImage(systemName: "arrowtriangle.right.fill")
                    rightArrow.isUserInteractionEnabled = true
                    rightArrow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(moveToTodayView(_:))))
                    rightArrow.tintColor = themeColor
                    rightArrow.translatesAutoresizingMaskIntoConstraints = false
                    return rightArrow
                }()


                YesterdayLabelStack.addArrangedSubview(yesterdayLabel)
                YesterdayLabelStack.addArrangedSubview(rightArrow)
                views[i].addSubview(YesterdayLabelStack)
                YesterdayLabelStack.centerXAnchor.constraint(equalTo: views[i].centerXAnchor).isActive = true
                views[i].addSubview(noEventsMessage)
                noEventsMessage.centerXAnchor.constraint(equalTo: views[i].centerXAnchor).isActive = true
                noEventsMessage.topAnchor.constraint(equalTo: views[i].topAnchor,constant: 250).isActive = true
                noEventsMessage.widthAnchor.constraint(equalTo: views[i].widthAnchor,multiplier: 2/5).isActive = true
                noEventsMessage.heightAnchor.constraint(equalTo: views[i].heightAnchor,multiplier: 1/5).isActive = true
                views[i].addSubview(noEventsLabel)
                noEventsLabel.topAnchor.constraint(equalTo: noEventsMessage.bottomAnchor,constant: 10).isActive = true
                noEventsLabel.centerXAnchor.constraint(equalTo: views[i].centerXAnchor).isActive = true
                noEventsLabel.isHidden = true
                noEventsMessage.isHidden = true

            }else if i == 1{
                let todayLabel = UILabel()
                todayLabel.font = UIFont.preferredFont(forTextStyle: .title1)
                todayLabel.text = "Today"

                let todayLabelStack : UIStackView = {
                    let todayLabelStack = UIStackView()
                    todayLabelStack.axis = .horizontal
                    todayLabelStack.distribution = .equalSpacing
                    todayLabelStack.spacing = 20
                    todayLabelStack.translatesAutoresizingMaskIntoConstraints = false
                    return todayLabelStack
                }()
                let leftArrow : UIImageView = {
                    let leftArrow = UIImageView()
                    leftArrow.image = UIImage(systemName: "arrowtriangle.left.fill")
                    leftArrow.translatesAutoresizingMaskIntoConstraints = false
                    leftArrow.isUserInteractionEnabled = true
                    leftArrow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(moveToYesterdayView(_:))))
                    leftArrow.tintColor = themeColor
                    return leftArrow
                }()
                let rightArrow : UIImageView = {
                    let rightArrow = UIImageView()
                    rightArrow.image = UIImage(systemName: "arrowtriangle.right.fill")
                    rightArrow.translatesAutoresizingMaskIntoConstraints = false
                    rightArrow.isUserInteractionEnabled = true
                    rightArrow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(moveToTomorrowView(_:))))
                    rightArrow.tintColor = themeColor
                    return rightArrow
                }()
                todayLabelStack.addArrangedSubview(leftArrow)
                todayLabelStack.addArrangedSubview(todayLabel)
                todayLabelStack.addArrangedSubview(rightArrow)
                views[i].addSubview(todayLabelStack)
                todayLabelStack.centerXAnchor.constraint(equalTo: views[i].centerXAnchor).isActive = true
                views[i].addSubview(noEventsMessage1)
                noEventsMessage1.centerXAnchor.constraint(equalTo: views[i].centerXAnchor).isActive = true
                noEventsMessage1.topAnchor.constraint(equalTo: views[i].topAnchor,constant: 250).isActive = true
                noEventsMessage1.widthAnchor.constraint(equalTo: views[i].widthAnchor,multiplier: 2/5).isActive = true
                noEventsMessage1.heightAnchor.constraint(equalTo: views[i].heightAnchor,multiplier: 1/5).isActive = true
                views[i].addSubview(noEventsLabel1)
                noEventsLabel1.centerXAnchor.constraint(equalTo: views[i].centerXAnchor).isActive = true
                noEventsLabel1.topAnchor.constraint(equalTo: noEventsMessage1.bottomAnchor,constant: 10).isActive = true
                noEventsLabel1.isHidden = true
                noEventsMessage1.isHidden = true

            
            }else{
                let tomorrowLabel = UILabel()
                tomorrowLabel.font = UIFont.preferredFont(forTextStyle: .title1)
                tomorrowLabel.text = "Tomorrow"
                
                let tomorrowLabelStack : UIStackView = {
                    let tomorrowLabelStack = UIStackView()
                    tomorrowLabelStack.axis = .horizontal
                    tomorrowLabelStack.distribution = .equalSpacing
                    tomorrowLabelStack.spacing = 20
                    tomorrowLabelStack.translatesAutoresizingMaskIntoConstraints = false
                    return tomorrowLabelStack
                }()
                let leftArrow : UIImageView = {
                    let leftArrow = UIImageView()
                    leftArrow.image = UIImage(systemName: "arrowtriangle.left.fill")
                    leftArrow.translatesAutoresizingMaskIntoConstraints = false
                    leftArrow.isUserInteractionEnabled = true
                    leftArrow.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(moveToTodayView(_:))))
                    leftArrow.tintColor = themeColor
                    return leftArrow
                }()
              
                tomorrowLabelStack.addArrangedSubview(leftArrow)
                tomorrowLabelStack.addArrangedSubview(tomorrowLabel)
                views[i].addSubview(tomorrowLabelStack)
                tomorrowLabelStack.centerXAnchor.constraint(equalTo: views[i].centerXAnchor).isActive = true
                views[i].addSubview(noEventsMessage2)
                noEventsMessage2.centerXAnchor.constraint(equalTo: views[i].centerXAnchor).isActive = true
                noEventsMessage2.topAnchor.constraint(equalTo: views[i].topAnchor,constant: 250).isActive = true
                noEventsMessage2.widthAnchor.constraint(equalTo: views[i].widthAnchor,multiplier: 2/5).isActive = true
                noEventsMessage2.heightAnchor.constraint(equalTo: views[i].heightAnchor,multiplier: 1/5).isActive = true
                views[i].addSubview(noEventsLabel2)
                noEventsLabel2.topAnchor.constraint(equalTo: noEventsMessage2.bottomAnchor,constant: 10).isActive = true
                noEventsLabel2.centerXAnchor.constraint(equalTo: views[i].centerXAnchor).isActive = true
                noEventsLabel2.isHidden = true
                noEventsMessage2.isHidden = true

                
            }

            views[i].addSubview(eventTableViews[i])
            eventTableViews[i].register(CustomEventsTableViewCell.self, forCellReuseIdentifier: "cellID")
            eventTableViews[i].backgroundColor = .systemBackground
            eventTableViews[i].dataSource = self
            eventTableViews[i].delegate = self
            eventTableViews[i].isHidden = true
            eventTableViews[i].translatesAutoresizingMaskIntoConstraints = false
            eventTableViews[i].topAnchor.constraint(equalTo: views[i].safeAreaLayoutGuide.topAnchor,constant: 80).isActive = true
            eventTableViews[i].leadingAnchor.constraint(equalTo: views[i].safeAreaLayoutGuide.leadingAnchor).isActive = true
            eventTableViews[i].widthAnchor.constraint(equalTo: views[i].widthAnchor).isActive = true
            eventTableViews[i].bottomAnchor.constraint(equalTo: views[i].bottomAnchor,constant: -150).isActive = true
            eventTableViews[i].autoresizingMask = .flexibleWidth
            eventTableViews[i].separatorStyle = UITableViewCell.SeparatorStyle.none
            scrollView.addSubview(views[i])
            views[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height)
            scrollView.delegate = self
            scrollView.contentOffset = CGPoint(x: view.frame.width, y: 0)
        }
        return scrollView

    }()
    

    override func viewDidAppear(_ animated: Bool) {
        selectedDateInMonthView = Date()
        for i in 0..<eventTableViews.count{
            eventTableViews[i].reloadData()
        }
        if scrollViewXValue != 0{
            selectedDateInMonthView = CalendarFunctions().addDays(date: Date(), days: scrollViewXValue - 1)
        }else{
            selectedDateInMonthView = Date().nearestMinute()
        }
    }
    public var scrollViewXValue : Int = 0
     
     func scrollViewDidScroll(_ scrollView: UIScrollView) {
         scrollViewXValue = Int(scrollView.contentOffset.x / view.frame.width)
         let date = CalendarFunctions().currentDate(date: Date())
         if scrollViewXValue != 0{
            selectedDateInMonthView = CalendarFunctions().addDays(date: CalendarFunctions().getSelectedDate(date: date), days: scrollViewXValue - 1)
         }else{
             selectedDateInMonthView = Date().nearestMinute()
         }
    }
    
    @objc func moveToTodayView(_ sender : UITapGestureRecognizer) {
        scrollView.setContentOffset(CGPoint(x: view.frame.width * 1, y: 0), animated: true)
    }
    @objc func moveToYesterdayView(_ sender : UITapGestureRecognizer) {
        scrollView.setContentOffset(CGPoint(x: view.frame.width * 0, y: 0), animated: true)
    }
    
    @objc func moveToTomorrowView(_ sender : UITapGestureRecognizer) {
        scrollView.setContentOffset(CGPoint(x: view.frame.width * 2, y: 0), animated: true)

    }
  
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    var tags = [Tags]()
    func getAllTags(){
    do{
        let request = Tags.fetchRequest() as NSFetchRequest<Tags>
        tags = try context.fetch(request)
     }catch {
         print(error)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addEvents))
        navigationController?.navigationBar.tintColor = themeColor
        navigationItem.rightBarButtonItem?.tintColor = themeColor
        view.addSubview(scrollView)
        scrollView.edgeTo(view: view)
        getAllEvents()
        getAllTags()
        checkSwitchState()
        TodayCalendarViewController.switchOnOff.addTarget(self, action: #selector(switchStateDidChange(_:)), for: .valueChanged)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTables), name: Notification.Name("EventSaved"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTables), name: Notification.Name("tagSaved"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTables), name: Notification.Name("UpdateEventSaved"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTables), name: Notification.Name("templateEventDeleteSaved"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTables), name: Notification.Name("templateEventEditSaved"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTables), name: Notification.Name("routineSaved"), object: nil)
        }
    @objc func reloadTables(){
        getAllEvents()
        getAllTags()
        self.todayEventsTableView.reloadData()
        self.tomorrowEventsTableView.reloadData()
        self.yesterdayEventsTableView.reloadData()
        navigationController?.popViewController(animated: true)
    }
    
    @objc func addEvents(){
            let rootVC = EventFormViewController()
            let navVC = UINavigationController(rootViewController: rootVC)
            navVC.modalPresentationStyle = .pageSheet
            present(navVC, animated: true)
    }
    
    @objc func switchStateDidChange(_ sender:UISwitch){

        if (sender.isOn == true){
            window.overrideUserInterfaceStyle = .dark
        }
        else{
           window.overrideUserInterfaceStyle = .light
        }
    }
    
    func checkSwitchState(){

        if userDefaults.bool(forKey: ON_OFF_KEY)
        {
            TodayCalendarViewController.switchOnOff.setOn(true, animated: false)
            window.overrideUserInterfaceStyle = .dark
        }else{
            TodayCalendarViewController.switchOnOff.setOn(false, animated: false)
            window.overrideUserInterfaceStyle = .light
        }
    }
        
}


extension TodayCalendarViewController : UITableViewDelegate,UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var events = events
        if tableView == todayEventsTableView{
            events = events.filter { filtered in
                    let date = CalendarFunctions().currentDate(date: filtered.date!)
                    return date == currentDate
                }
            if events.count == 0{
                noEventsLabel1.isHidden = false
                noEventsMessage1.isHidden = false
                eventTableViews[1].isHidden = true
            }else{
                noEventsLabel1.isHidden = true
                noEventsMessage1.isHidden = true
                eventTableViews[1].isHidden = false
            }
        }else if tableView == yesterdayEventsTableView{
            events = events.filter { filtered in
                if let filter = filtered.date{
                    let date = CalendarFunctions().currentDate(date: filter)
                    return date == yesterday
                }
                return true
                      
            }
            if events.count == 0{
                noEventsLabel.isHidden = false
                noEventsMessage.isHidden = false
                eventTableViews[0].isHidden = true
            }else{
                noEventsLabel.isHidden = true
                noEventsMessage.isHidden = true
                eventTableViews[0].isHidden = false
            }

        }else{
            events = events.filter { filtered in
                if let filter = filtered.date{
                    let date = CalendarFunctions().currentDate(date: filter)
                    return date == nextDay
                }
                return true
                
            }
            if events.count == 0{
                noEventsLabel2.isHidden = false
                noEventsMessage2.isHidden = false
                eventTableViews[2].isHidden = true
            }else{
                noEventsLabel2.isHidden = true
                noEventsMessage2.isHidden = true
                eventTableViews[2].isHidden = false
                }
            }
       
            return events.count
    }


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cellID") as! CustomEventsTableViewCell
            cell.selectionStyle = .none
        var items = events
        if tableView == todayEventsTableView{
                items = events.filter { filtered in
                    let date = CalendarFunctions().currentDate(date: filtered.date!)
                    return date == currentDate
                }
        }else if tableView == yesterdayEventsTableView{
                items = events.filter { filtered in
                    let date = CalendarFunctions().currentDate(date: filtered.date!)
                    return   date == yesterday
                }
        }else{
                items = events.filter { filtered in
                    let date = CalendarFunctions().currentDate(date: filtered.date!)
                    return date == nextDay
                }

        }
        
                items.sort{$0.startTime! < $1.startTime!}

                let startTime = CalendarFunctions().getTimeString(date: items[indexPath.row].startTime!)
                let endTime = CalendarFunctions().getTimeString(date: items[indexPath.row].endTime!)
                cell.eventStartTime.text = startTime + " - "
                cell.eventEndTime.text = endTime
                cell.eventTitle.text = items[indexPath.row].title!.trimmingCharacters(in: .whitespaces)
                let tagList = tags.filter { filtered in
                        return items[indexPath.row].tag == filtered.name
                }
            if items[indexPath.row].templateName != nil{
                cell.eventIcon.image = UIImage(systemName: "repeat.circle")
                cell.eventIcon.tintColor = themeColor

            }else{
                cell.eventIcon.image = UIImage(systemName: "clock")
                cell.eventIcon.tintColor = themeColor

            }
                cell.eventTag.text = items[indexPath.row].tag!.trimmingCharacters(in: .whitespaces)
        if  tagList.count != 0{
            cell.eventTag.backgroundColor = tagList[0].color

        }else{
            cell.eventTag.backgroundColor = themeColor
        }
                cell.eventTag.layer.cornerRadius = 25
                cell.eventTag.textColor = .black
                cell.eventTag.layer.masksToBounds = true
                cell.eventTag.layer.cornerRadius = 15
                return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        var events = events
        if tableView == todayEventsTableView{
            events = events.filter { filtered in
                        let date = CalendarFunctions().currentDate(date: filtered.date!)
                        return date == currentDate
                }

        }else if tableView == yesterdayEventsTableView{
            events = events.filter { filtered in
                    let date = CalendarFunctions().currentDate(date: filtered.date!)
                    return date == yesterday
            }
        }else{
             events = events.filter { filtered in
                    let date = CalendarFunctions().currentDate(date: filtered.date!)
                    return date == nextDay
            }
        }
        events.sort{$0.startTime! < $1.startTime!}

        navigationController?.pushViewController(DetailedEventViewController(event: events[indexPath.row]), animated: true)
    }
    
   
    func getAllEvents(){
       do{
           let request = Events.fetchRequest() as NSFetchRequest<Events>
           events = try context.fetch(request)
       }catch {
             print(error)
       }

    }
 


}
