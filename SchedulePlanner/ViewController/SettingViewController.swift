//
//  SettingViewController.swift
//
//  Created by praveena-pt4972 on 02/05/22.
//

    
import UIKit

class SettingViewController: UIViewController , UITableViewDataSource,UITableViewDelegate, UITextFieldDelegate, UITextViewDelegate {

        let userDefaults = UserDefaults.standard
        let ON_OFF_KEY = "onOffKey"
        let switchOnOff = UISwitch(frame:CGRect(x: 150, y: 150, width: 0, height: 0))

        let window = UIApplication.shared.windows[0]
        let settingTableview : UITableView = {
            let settingTableview = UITableView(frame: CGRect.zero, style: .insetGrouped)
            settingTableview.autoresizingMask = .flexibleWidth
            settingTableview.translatesAutoresizingMaskIntoConstraints = false
            return settingTableview
        }()


    @objc func switchStateDidChange(_ sender:UISwitch){

        if (sender.isOn == true){
            userDefaults.set(true, forKey: ON_OFF_KEY)
            window.overrideUserInterfaceStyle = .dark
        }
        else{
            userDefaults.set(false, forKey: ON_OFF_KEY)
        }
    }

    func checkSwitchState(){

        if userDefaults.bool(forKey: ON_OFF_KEY)
        {
            EventRemainderCustomCell().toogleSwitch.setOn(true, animated: false)

        }else{
            EventRemainderCustomCell().toogleSwitch.setOn(false, animated: false)
        }
    }
    override func viewDidLoad() {
        
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        title = "Settings"
        navigationController?.navigationBar.prefersLargeTitles = true
        settingTableview.register(CustomSettingTableViewCell.self, forCellReuseIdentifier: "darkTheme")
        settingTableview.register(UITableViewCell.self, forCellReuseIdentifier: "tagView")
        view.addSubview(settingTableview)
        TodayCalendarViewController.switchOnOff.onTintColor = themeColor
        TodayCalendarViewController.switchOnOff.addTarget(self, action: #selector(switchStateDidChange(_:)), for: .valueChanged)
        setUpTableView()
        checkSwitchState()
    }

    func setUpTableView(){

        settingTableview.reloadData()
        settingTableview.frame = view.bounds
        settingTableview.delegate = self
        settingTableview.dataSource = self
        settingTableview.showsVerticalScrollIndicator = false
    }



    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
                let cell = tableView.dequeueReusableCell(withIdentifier: "darkTheme", for: indexPath) as! CustomSettingTableViewCell
                cell.remainderLabel.text = "Dark Theme"
                cell.selectionStyle = .none
                return cell
            }else{
                let cell = tableView.dequeueReusableCell(withIdentifier: "tagView", for: indexPath)
                cell.selectionStyle = UITableViewCell.SelectionStyle.default
                cell.textLabel?.text = "Tags"
                return cell
            }

    }
    
    

     func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
             return 20
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
            return 1
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
      return 70
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        if indexPath.section == 1{
            let rootVC = TagViewController()
            let navVC = UINavigationController(rootViewController: rootVC)
            navVC.modalPresentationStyle = .pageSheet
            present(navVC, animated: true)
        }
    }
}


