//
//  TagTableViewController.swift
//  TabBar
//
//  Created by praveena-pt4972 on 19/05/22.
//

import UIKit
import CoreData

class TagViewController: UIViewController, UITableViewDataSource, UITableViewDelegate,UISearchResultsUpdating {
    
    
    func updateSearchResults(for searchController: UISearchController) {
        filteredData(for: searchController.searchBar.text ?? "")

    }
    
    private func filteredData(for searchText: String) {
        filteredData = tags.filter { filtered in
        return
            filtered.name!.trimmingCharacters(in: .whitespaces).lowercased().contains(searchText.lowercased().trimmingCharacters(in: .whitespaces))
      }
        tagTableView.reloadData()
    }
    
    let searchController = UISearchController(searchResultsController: nil)
    var filteredData = [Tags]()
    var tags = [Tags]()
    func getAllTags(){
    do{
        let request = Tags.fetchRequest() as NSFetchRequest<Tags>
        tags = try context.fetch(request)
     }catch {
         print(error)
        }
    }

     let tagTableView : UITableView = {
         let tagTableView = UITableView()
         tagTableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
         tagTableView.autoresizingMask = .flexibleWidth
         tagTableView.translatesAutoresizingMaskIntoConstraints = false
         tagTableView.sectionFooterHeight = 20
         return tagTableView
    }()
    let noEventsMessage : UIImageView = {
        let noEventsMessage = UIImageView()
        let largeConfig = UIImage.SymbolConfiguration(pointSize: 50, weight: .light, scale: .large)
        noEventsMessage.image = UIImage(systemName: "magnifyingglass", withConfiguration: largeConfig)
        noEventsMessage.translatesAutoresizingMaskIntoConstraints = false
        noEventsMessage.tintColor = .placeholderText
        return noEventsMessage
    }()
    let noEventsLabel: UILabel = {
        let noEventsLabel = UILabel()
        noEventsLabel.text = "No Tags Found"
        noEventsLabel.textColor = .placeholderText
        noEventsLabel.font = UIFont.preferredFont(forTextStyle: .headline, compatibleWith: .none)
        noEventsLabel.translatesAutoresizingMaskIntoConstraints = false
        return noEventsLabel
    }()
   
    override func viewDidLoad() {
        super.viewDidLoad()
        title = "Tags"
        navigationController?.navigationBar.prefersLargeTitles = true
        view.backgroundColor = .systemBackground
        navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addTag))
        navigationItem.rightBarButtonItem?.tintColor = themeColor
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .cancel, target: self, action: #selector(cancelTagPage))
        navigationItem.leftBarButtonItem?.tintColor = themeColor
        
        view.addSubview(tagTableView)
        getAllTags()
        setUpTableView()
        
        searchController.searchResultsUpdater = self
        navigationItem.searchController = searchController
        navigationController?.navigationBar.tintColor = themeColor

        view.addSubview(noEventsMessage)
        noEventsMessage.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 180).isActive = true
        noEventsMessage.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 130).isActive = true
        noEventsMessage.widthAnchor.constraint(equalTo: view.widthAnchor,multiplier: 2/5).isActive = true
        noEventsMessage.heightAnchor.constraint(equalTo: view.heightAnchor,multiplier: 1/5).isActive = true
        view.addSubview(noEventsLabel)
        noEventsLabel.topAnchor.constraint(equalTo: noEventsMessage.bottomAnchor,constant: 10).isActive = true
        noEventsLabel.leadingAnchor.constraint(equalTo: noEventsMessage.leadingAnchor,constant: 10).isActive = true
        
        noEventsLabel.isHidden = true
        noEventsMessage.isHidden = true
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTable), name: Notification.Name("tagSaved"), object: nil)
    }
    @objc func reloadTable(){
        getAllTags()
        self.tagTableView.reloadData()

    }
    @objc func cancelTagPage(){
        dismiss(animated: true,completion: nil)
    }
    
    func setUpTableView(){
        tagTableView.backgroundColor = .systemBackground
        tagTableView.showsVerticalScrollIndicator = false
        tagTableView.delegate = self
        tagTableView.dataSource = self
        tagTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        tagTableView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 0).isActive = true
        tagTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 30).isActive = true
        tagTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,constant: -30).isActive = true
        tagTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,constant: 0).isActive = true
    }
    
  
    @objc func addTag(){
        let rootVC = TagFormViewController()
        let navVC = UINavigationController(rootViewController: rootVC)
        navVC.modalPresentationStyle = .pageSheet
        present(navVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if section == tags.count - 1{
            return 0
        }
        return 20
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
 
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
      
        return 1
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        if searchController.isActive && searchController.searchBar.text != "" {
            
                if self.filteredData.count == 0{
                    noEventsLabel.isHidden = false
                    noEventsMessage.isHidden = false
                }else{
                    noEventsLabel.isHidden = true
                    noEventsMessage.isHidden = true

                }
            return self.filteredData.count
        } else if searchController.isActive && searchController.searchBar.text == ""{
            noEventsLabel.isHidden = false
            noEventsMessage.isHidden = false

            return 0
        }
        if tags.count == 0{
            noEventsLabel.isHidden = false
            noEventsMessage.isHidden = false

        }else{
            noEventsLabel.isHidden = true
            noEventsMessage.isHidden = true

        }
        return tags.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .subtitle, reuseIdentifier: "cell")
        
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        var model = tags[indexPath.section]
        
        if searchController.isActive && searchController.searchBar.text != "" {
          model = filteredData[indexPath.section]
        }else{
            model = tags[indexPath.section]

        }
        
        cell.textLabel?.text = model.name!.trimmingCharacters(in: .whitespaces)
        cell.textLabel?.textColor = .black
        cell.contentView.backgroundColor = model.color
        cell.contentView.layer.cornerRadius = 15
        return cell
    }
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath)
            -> UISwipeActionsConfiguration? {
            let deleteAction = UIContextualAction(style: .destructive, title: nil) { (_, _, completionHandler) in
                let item = self.tags[indexPath.section]
                let actionsheet = UIAlertController(
                    title: "Are You Sure you want to delete?",
                    message: "",
                    preferredStyle: .actionSheet)
                    actionsheet.addAction(UIAlertAction(title: "cancel", style: .cancel, handler: nil))
                    actionsheet.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { _ in
                       
                        self.deleteTag(item: item)

                    }))
                    self.present(actionsheet,animated: true)
                    actionsheet.popoverPresentationController?.sourceView = self.view
                    completionHandler(true)
                
            }

            deleteAction.image = UIImage(systemName: "trash")?.withTintColor(themeColor, renderingMode: .alwaysOriginal)
            deleteAction.backgroundColor = .systemBackground
        let configuration = UISwipeActionsConfiguration(actions: [ deleteAction])
        return configuration
    }

    func deleteTag(item: Tags){
        context.delete(item)
        do{
            try context.save()
            NotificationCenter.default.post(name: Notification.Name("tagSaved"), object: nil)

        }catch{
            print(error)
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var model = tags[indexPath.section]
        
        if searchController.isActive && searchController.searchBar.text != "" {
          model = filteredData[indexPath.section]
            let rootVC = TagEditViewController(tag: model)
            let navVC = UINavigationController(rootViewController: rootVC)
            navVC.modalPresentationStyle = .pageSheet
            present(navVC, animated: true)
        }else{
            model = tags[indexPath.section]
            let rootVC = TagEditViewController(tag: model)
            let navVC = UINavigationController(rootViewController: rootVC)
            navVC.modalPresentationStyle = .pageSheet
            present(navVC, animated: true)

        }
    }

}



