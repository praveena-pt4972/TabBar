//
//  FourthViewController.swift
//  TabBar
//
//  Created by praveena-pt4972 on 02/05/22.
//

import UIKit
import CoreData


class EventViewController : UIViewController,UITableViewDelegate , UITableViewDataSource,UISearchResultsUpdating{
    
    var filteredDates = [String]()
    func updateSearchResults(for searchController: UISearchController) {
        filteredEvents(for: searchController.searchBar.text ?? "")
    }
    let searchController = UISearchController(searchResultsController: nil)
    private func filteredEvents(for searchText: String) {
        filteredEvents = allEvents.filter { filtered in
        return
            filtered.tag!.lowercased().contains(searchText.lowercased())
            || filtered.title!.trimmingCharacters(in: .whitespaces).lowercased().contains(searchText.lowercased().trimmingCharacters(in: .whitespaces))
      }
        allEventTableView.reloadData()
    }
    
    var allEvents = [Events]()
    var filteredEvents = [Events]()
    var sortedEvents : Int = 2
    var hidePast : Bool = true
     let allEventTableView : UITableView = {
        let allEventTableView = UITableView()
         allEventTableView.autoresizingMask = .flexibleWidth
         allEventTableView.translatesAutoresizingMaskIntoConstraints = false
        return allEventTableView
    }()

    let noEventsMessage : UIImageView = {
        let noEventsMessage = UIImageView()
        let largeConfig = UIImage.SymbolConfiguration(pointSize: 50, weight: .light, scale: .large)
        noEventsMessage.image = UIImage(systemName: "calendar.badge.clock", withConfiguration: largeConfig)
        noEventsMessage.translatesAutoresizingMaskIntoConstraints = false
        noEventsMessage.tintColor = .placeholderText
        return noEventsMessage

    }()
    let noEventsLabel: UILabel = {
        let noEventsLabel = UILabel()
        noEventsLabel.text = "No Events Found"
        noEventsLabel.textColor = .placeholderText
        noEventsLabel.font = UIFont.preferredFont(forTextStyle: .headline, compatibleWith: .none)
        noEventsLabel.translatesAutoresizingMaskIntoConstraints = false
        return noEventsLabel
    }()
    
    
    func filterEvents() -> [Events]{
        var item = allEvents
        item = allEvents.filter { filter in
            return CalendarFunctions().stringToDate(date: CalendarFunctions().currentDate(date: filter.date!))  >= CalendarFunctions().stringToDate(date: CalendarFunctions().currentDate(date: Date()) )
        }
        if searchController.isActive && searchController.searchBar.text != "" {
                    item = filteredEvents
            }else if searchController.isActive && searchController.searchBar.text == ""{
                item = []
                        noEventsLabel.isHidden = false
                        noEventsMessage.isHidden = false
        }
        
        if sortedEvents == 1{
            item = allEvents
            if searchController.isActive && searchController.searchBar.text != "" {
                        item = filteredEvents
            }
            let eventDates = item.map { CalendarFunctions().currentDate(date: $0.date!)}
            filteredDates = Array(Set(eventDates))
            filteredDates.sort(){CalendarFunctions().stringToDate(date: $0) < CalendarFunctions().stringToDate(date: $1) }
          
        }else if sortedEvents == 2{
            let eventDates = item.map { CalendarFunctions().currentDate(date: $0.date!)}
            filteredDates = Array(Set(eventDates))
            filteredDates.sort(){CalendarFunctions().stringToDate(date: $0) < CalendarFunctions().stringToDate(date: $1) }

        }
        else if sortedEvents == 3{
            let eventDates = item.map { CalendarFunctions().currentDate(date: $0.date!)}
            filteredDates = Array(Set(eventDates))
            filteredDates.sort(){CalendarFunctions().stringToDate(date: $0) > CalendarFunctions().stringToDate(date: $1) }

        }
        return item

    }
    
    func numberOfSections(in tableView: UITableView) -> Int {

        let item = filterEvents()
        if item.count == 0{
            noEventsLabel.isHidden = false
            noEventsMessage.isHidden = false
        }else{
            noEventsLabel.isHidden = true
            noEventsMessage.isHidden = true
            return filteredDates.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let item = filterEvents()
        let filtered = item.filter{ filtered in
        return CalendarFunctions().currentDate(date: filtered.date!) == filteredDates[section]
    }
       return filtered.count
    }
    
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {

        let header = filteredDates[section]
        return header
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let items = filterEvents()

        let filtered = items.filter{ filtered in
            return CalendarFunctions().currentDate(date: filtered.date!) == filteredDates[indexPath.section]
    }
        
      let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! CustomEventsTableViewCell
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        let startTime = CalendarFunctions().getTimeString(date: filtered[indexPath.row].startTime!)
        let endTime = CalendarFunctions().getTimeString(date:  filtered[indexPath.row].endTime!)
        cell.eventStartTime.text = startTime + " - "
        cell.eventEndTime.text = endTime
        cell.eventTitle.text = filtered[indexPath.row].title!.trimmingCharacters(in: .whitespaces)
        cell.eventTag.text = filtered[indexPath.row].tag!.trimmingCharacters(in: .whitespaces)
        cell.eventTag.layer.cornerRadius = 25
        cell.selectionStyle = UITableViewCell.SelectionStyle.none
        let tagList = tags.filter { filteredTag in
                return
            filtered[indexPath.row].tag == filteredTag.name
        }
        if filtered[indexPath.row].templateName != nil{
            cell.eventIcon.image = UIImage(systemName: "repeat.circle")
            cell.eventIcon.tintColor = themeColor

        }else{
            cell.eventIcon.image = UIImage(systemName: "clock")
            cell.eventIcon.tintColor = themeColor
        }
        if  tagList.count != 0{
            cell.eventTag.backgroundColor = tagList[0].color
        }else{
            cell.eventTag.backgroundColor = themeColor
        }
        cell.eventTag.textColor = .black
        cell.eventTag.layer.masksToBounds = true
        cell.eventTag.layer.cornerRadius = 15
        return cell
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        title = "Events"
        navigationController?.navigationBar.prefersLargeTitles = true
        allEventTableView.register(CustomEventsTableViewCell.self, forCellReuseIdentifier: "cell")
        view.addSubview(allEventTableView)
        getAllEvent()
        getAllTags()
        allEventTableView.delegate = self
        allEventTableView.dataSource = self
        allEventTableView.translatesAutoresizingMaskIntoConstraints = false
        allEventTableView.topAnchor.constraint(equalTo: view.topAnchor,constant: 70).isActive = true
        allEventTableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 10).isActive = true
        allEventTableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor,constant: 0).isActive = true
        allEventTableView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,constant: 0).isActive = true
        allEventTableView.keyboardDismissMode = .onDrag
        allEventTableView.separatorStyle = UITableViewCell.SeparatorStyle.none
        configureBar()
        searchController.searchResultsUpdater = self
        navigationItem.searchController = searchController
        allEventTableView.showsVerticalScrollIndicator = false
        navigationController?.navigationBar.tintColor = themeColor
        view.addSubview(noEventsMessage)
        noEventsMessage.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 180).isActive = true
        noEventsMessage.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor,constant: 130).isActive = true
        noEventsMessage.widthAnchor.constraint(equalTo: view.widthAnchor,multiplier: 2/5).isActive = true
        noEventsMessage.heightAnchor.constraint(equalTo: view.heightAnchor,multiplier: 1/5).isActive = true
        view.addSubview(noEventsLabel)
        noEventsLabel.topAnchor.constraint(equalTo: noEventsMessage.bottomAnchor,constant: 10).isActive = true
        noEventsLabel.leadingAnchor.constraint(equalTo: noEventsMessage.leadingAnchor,constant: 10).isActive = true
        noEventsLabel.isHidden = true
        noEventsMessage.isHidden = true
        navigationController?.navigationBar.tintColor = themeColor
        navigationItem.rightBarButtonItem?.tintColor = themeColor
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTable), name: Notification.Name("EventSaved"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTable), name: Notification.Name("routineSaved"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTable), name: Notification.Name("UpdateEventSaved"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTable), name: Notification.Name("tagSaved"), object: nil)
    }
    @objc func reloadTable(){
        getAllTags()
        getAllEvent()
        allEventTableView.reloadData()
        navigationController?.popViewController(animated: true)

    }
  
    override func viewDidAppear(_ animated: Bool) {
        selectedDateInMonthView = Date()
    }
    
    var filterButton = UIBarButtonItem()
    
    
    private func setupViews(){
        filterButton = UIBarButtonItem(
            image: UIImage(systemName: "ellipsis.circle"),
            menu: createMenu()
        )
        let image2 = UIImage(systemName: "plus")
        let plus = UIBarButtonItem(image: image2, style: .plain, target: self, action: #selector(addEvent))

        navigationItem.rightBarButtonItems = [plus , filterButton]
    }

    private func createMenu(actionTitle: String? = nil) -> UIMenu {
   
        let image2 = UIImage(systemName: "eye.slash.fill")
        let image3 = UIImage(systemName: "arrow.down")
        let image4 = UIImage(systemName: "arrow.up")
       let menu = UIMenu(title: "Sort", children: [

            UIAction(title: "order by ASC" , image: image3) { [unowned self] action in
                self.filterButton.menu = createMenu(actionTitle: action.title)
                sortedEvents = 2
                allEventTableView.reloadData()
            },
            UIAction(title: "order by DESC", image: image4) { [unowned self] action in
                self.filterButton.menu = createMenu(actionTitle: action.title)
                sortedEvents = 3
                allEventTableView.reloadData()
            }
        ])
    
        let filter = UIAction(title: "Show Past Events" , image: image2
        ){ [unowned self] action in

        self.filterButton.menu = createMenu(actionTitle: action.title)
        if hidePast == false{
            sortedEvents = 2
            hidePast = true
        }else{
            sortedEvents = 1
            hidePast = false
        }

        allEventTableView.reloadData()
    }
               
        let menus = UIMenu(title: "", children: [filter,menu])
        if let actionTitle = actionTitle {
     
            menus.children.forEach { action in
                guard let action = action as? UIAction else {
                    return
                }
                if action.title == actionTitle {
                    action.state = .on
                }else{
                    action.state = .off
                }
            }
        } else {
            let action = menus.children.first as? UIAction
            action?.state = .off
        }
        return menus
    }
    
    func configureBar(){
        setupViews()
    }

    @objc func addEvent(){
        let rootVC = EventFormViewController()
        let navVC = UINavigationController(rootViewController: rootVC)
        navVC.modalPresentationStyle = .pageSheet
        present(navVC, animated: true)
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let items = filterEvents()
        let filtered = items.filter{ filtered in
            return CalendarFunctions().currentDate(date: filtered.date!) == filteredDates[indexPath.section]
        }
        navigationController?.pushViewController(DetailedEventViewController(event: filtered[indexPath.row]), animated: true)
        navigationController?.navigationBar.tintColor = themeColor
    }
    func getAllEvent(){
        
       do{
           let request = Events.fetchRequest() as NSFetchRequest<Events>
           allEvents = try context.fetch(request)
       }catch {
            print(error)
       }
    }
    var tags = [Tags]()
    func getAllTags(){
    do{
        let request = Tags.fetchRequest() as NSFetchRequest<Tags>
        tags = try context.fetch(request)
     }catch {
         print(error)
    }

    }
    
}
