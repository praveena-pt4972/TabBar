//
//  TemplateViewController.swift
//  TabBar
//
//  Created by praveena-pt4972 on 09/05/22.
//

import UIKit
import CoreData

class RoutineEventsViewController: UIViewController, UIScrollViewDelegate {

    
    public let name : String
    public let includeWeekend : Bool
    public let noOfDays : Int64
    var templateEvents = [TemplateEvents]()

    init(name : String , noOfDays : Int64,includeWeekend : Bool){
            self.name = name
            self.noOfDays = noOfDays
        self.includeWeekend = includeWeekend
            super.init(nibName: nil, bundle: nil)
        }

        required init?(coder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    
        lazy var views : [UIView] = []
        let label : UILabel = {
            let lbl = UILabel()
            lbl.text = "days"
            return lbl
        }()

        func viewCreation(){
            for _ in 0..<noOfDays{
                lazy var vc : UIView = {
                let vc = UIView()
                return vc
            }()
            views.append(vc)
        }
            
            
    }
    lazy var noMessageImage : [UIImageView] = []
    lazy var noMessageLabel : [UILabel] = []

    
    func noEventMessageCreation(){
        for _ in 0..<noOfDays{
            lazy var noEventsMessage : UIImageView = {
                let noEventsMessage = UIImageView()
                let largeConfig = UIImage.SymbolConfiguration(pointSize: 50, weight: .light, scale: .large)
                noEventsMessage.image = UIImage(systemName: "calendar.badge.exclamationmark", withConfiguration: largeConfig)
                noEventsMessage.translatesAutoresizingMaskIntoConstraints = false
                noEventsMessage.tintColor = .placeholderText
                return noEventsMessage

            }()
            lazy var noEventsLabel: UILabel = {
                let noEventsLabel = UILabel()
                noEventsLabel.text = "No Events Found"
                noEventsLabel.textColor = .placeholderText
                noEventsLabel.font = UIFont.preferredFont(forTextStyle: .headline, compatibleWith: .none)
                noEventsLabel.translatesAutoresizingMaskIntoConstraints = false
                return noEventsLabel
            }()
            noMessageImage.append(noEventsMessage)
            noMessageLabel.append(noEventsLabel)

        }
    }

     let tableview1 : UITableView = {
        let table = UITableView()
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.autoresizingMask = .flexibleWidth
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
     let tableview2 : UITableView = {
        let table = UITableView()
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.autoresizingMask = .flexibleWidth
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
     let tableview3 : UITableView = {
        let table = UITableView()
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.autoresizingMask = .flexibleWidth
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
     let tableview4 : UITableView = {
        let table = UITableView()
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.autoresizingMask = .flexibleWidth
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
     let tableview5 : UITableView = {
        let table = UITableView()
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.autoresizingMask = .flexibleWidth
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
     let tableview6 : UITableView = {
        let table = UITableView()
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.autoresizingMask = .flexibleWidth
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
     let tableview7 : UITableView = {
        let table = UITableView()
        table.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        table.autoresizingMask = .flexibleWidth
        table.translatesAutoresizingMaskIntoConstraints = false
        return table
    }()
    
    lazy var uiTableViews : [UITableView] = [tableview1,tableview2,tableview3,tableview4,tableview5,tableview6,tableview7]
  
    lazy var scrollView : UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.showsHorizontalScrollIndicator = false
        scrollView.isPagingEnabled = true
        viewCreation()
        noEventMessageCreation()
        scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(views.count), height: view.frame.height - 300)
        for i in 0..<views.count{
            let label = UILabel()
            label.text = "Day \(i+1)"
        
            views[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: view.frame.height )
            views[i].addSubview(label)
            label.font = UIFont.preferredFont(forTextStyle: .headline)
            label.edgeTo(view: views[i])
            views[i].addSubview(uiTableViews[i])
            uiTableViews[i].register(CustomEventsTableViewCell.self, forCellReuseIdentifier: CustomEventsTableViewCell.identifier)
            uiTableViews[i].backgroundColor = .systemBackground
            uiTableViews[i].reloadData()
            uiTableViews[i].dataSource = self
            uiTableViews[i].delegate = self
            uiTableViews[i].tableConstraints(view: views[i])
            uiTableViews[i].estimatedRowHeight = 200
            uiTableViews[i].rowHeight = UITableView.automaticDimension
            uiTableViews[i].separatorStyle = UITableViewCell.SeparatorStyle.none
            scrollView.addSubview(views[i])
            uiTableViews[i].autoresizingMask = .flexibleWidth
            uiTableViews[i].isHidden = true
            views[i].addSubview(noMessageImage[i])
            noMessageImage[i].centerXAnchor.constraint(equalTo: views[i].centerXAnchor).isActive = true
            noMessageImage[i].topAnchor.constraint(equalTo: views[i].topAnchor,constant: 250).isActive = true
            noMessageImage[i].widthAnchor.constraint(equalTo: views[i].widthAnchor,multiplier: 2/5).isActive = true
            noMessageImage[i].heightAnchor.constraint(equalTo: views[i].heightAnchor,multiplier: 1/5).isActive = true
            views[i].addSubview(noMessageLabel[i])
            noMessageLabel[i].topAnchor.constraint(equalTo: noMessageImage[i].bottomAnchor,constant: 10).isActive = true
            noMessageLabel[i].centerXAnchor.constraint(equalTo: views[i].centerXAnchor,constant: 0).isActive = true
            
            noMessageImage[i].isHidden = true
            noMessageLabel[i].isHidden = true
            configureBars()
            views[i].sizeToFit()
            scrollView.delegate = self
        }
        return scrollView

    }()
    var tags = [Tags]()
    func getAllTags(){
    do{
        let request = Tags.fetchRequest() as NSFetchRequest<Tags>
        tags = try context.fetch(request)
     }catch {
         print(error)
        }

    }

    override func viewDidLoad() {
        super.viewDidLoad()
        title =  name
        view.backgroundColor = .systemBackground
        view.addSubview(scrollView)
        scrollView.edgeTo(view: view)
        getAllTemplateEvents()
        getAllTags()
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTable), name: Notification.Name("templateEventSaved"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTable), name: Notification.Name("templateEventEditSaved"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTable), name: Notification.Name("templateEventDeleteSaved"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadTable), name: Notification.Name("tagSaved"), object: nil)
       
        }
    
    @objc func reloadTable(){
        getAllTemplateEvents()
        getAllTags()
        for i in 0..<uiTableViews.count{
            uiTableViews[i].reloadData()
        }
    }
            
  
    func configureBars(){
        let plus = UIImage(systemName: "plus")
        let info = UIImage(systemName: "info.circle")
         let plusButton = UIBarButtonItem(image: plus, style: .plain, target: self, action: #selector(goToEventForm))
         let infoButton = UIBarButtonItem(image: info, style: .plain, target: self, action: #selector(goToInfoRoutine))
         self.navigationItem.rightBarButtonItems = [plusButton,infoButton]
         navigationItem.rightBarButtonItem?.tintColor = themeColor

    }
    
   public var scrollViewXValue : Int = 0
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        scrollViewXValue = Int(scrollView.contentOffset.x / view.frame.width)
    }
    @objc func goToInfoRoutine(){
        navigationController?.pushViewController(RoutineInfoViewController(templateName: name,noOfDays : noOfDays,includeWeekend : includeWeekend), animated: true)
        navigationController?.navigationBar.tintColor = themeColor
//        let rootVC = RoutineInfoViewController(templateName: name,noOfDays : noOfDays,includeWeekend : includeWeekend)
//        let navVC = UINavigationController(rootViewController: rootVC)
//        navVC.modalPresentationStyle = .pageSheet
//        present(navVC, animated: true)
    }

    @objc func goToEventForm(){
        let rootVC = TemplateEventFormViewController(noOfdays: noOfDays,day : Int(scrollViewXValue)+1,templateName : name)
        let navVC = UINavigationController(rootViewController: rootVC)
        navVC.modalPresentationStyle = .pageSheet
        present(navVC, animated: true)
     }
 
}
extension UILabel{
    func edgeTo(view : UIView){
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 30).isActive = true
        centerXAnchor.constraint(equalTo: view.safeAreaLayoutGuide.centerXAnchor,constant: 0).isActive = true
    }
}

extension UIScrollView{
    func edgeTo(view : UIView){
        translatesAutoresizingMaskIntoConstraints = false
        topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
        bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor).isActive = true
        trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor).isActive = true
    }
}
extension UITableView{
     func tableConstraints(view : UIView){
         translatesAutoresizingMaskIntoConstraints = false
         topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor,constant: 80).isActive = true
         leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor).isActive = true
         bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor,constant: -200).isActive = true
         widthAnchor.constraint(equalTo: view.widthAnchor).isActive = true
    }
}

extension RoutineEventsViewController : UITableViewDataSource , UITableViewDelegate{
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var model = templateEvents
        if tableView == tableview1{
             model = templateEvents.filter{
                filtered in return filtered.dayNumber == 1 && filtered.templateName == name
            }
            if model.count == 0{
                noMessageImage[0].isHidden = false
                noMessageLabel[0].isHidden = false
                uiTableViews[0].isHidden = true
            }else{
                noMessageImage[0].isHidden = true
                noMessageLabel[0].isHidden = true
                uiTableViews[0].isHidden = false
            }
        }
        if tableView == tableview2{
            model = templateEvents.filter{
                filtered in
                return filtered.dayNumber == 2 && filtered.templateName == name
            }
            if model.count == 0{
                noMessageImage[1].isHidden = false
                noMessageLabel[1].isHidden = false
                uiTableViews[1].isHidden = true
            }else{
                noMessageImage[1].isHidden = true
                noMessageLabel[1].isHidden = true
                uiTableViews[1].isHidden = false
            }
        }
        if tableView == tableview3{
            model = templateEvents.filter{ filtered in
                return filtered.dayNumber == 3 && filtered.templateName == name
            }
            if model.count == 0{
                noMessageImage[2].isHidden = false
                noMessageLabel[2].isHidden = false
                uiTableViews[2].isHidden = true
            }else{
                noMessageImage[2].isHidden = true
                noMessageLabel[2].isHidden = true
                uiTableViews[2].isHidden = false
            }
        }
        if tableView == tableview4{
            model = templateEvents.filter{
                filtered in
                return filtered.dayNumber == 4 && filtered.templateName == name
            }
            if model.count == 0{
                noMessageImage[3].isHidden = false
                noMessageLabel[3].isHidden = false
                uiTableViews[3].isHidden = true
            }else{
                noMessageImage[3].isHidden = true
                noMessageLabel[3].isHidden = true
                uiTableViews[3].isHidden = false
                
            }
        }
        if tableView == tableview5{
            model = templateEvents.filter{
                filtered in
                return filtered.dayNumber == 5 && filtered.templateName == name
            }
            if model.count == 0{
                noMessageImage[4].isHidden = false
                noMessageLabel[4].isHidden = false
                uiTableViews[4].isHidden = true
            }else{
                noMessageImage[4].isHidden = true
                noMessageLabel[4].isHidden = true
                uiTableViews[4].isHidden = false
                
            }
        }
        if tableView == tableview6{
            model = templateEvents.filter{
                filtered in
                return filtered.dayNumber == 6 && filtered.templateName == name
            }
            if model.count == 0{
                noMessageImage[5].isHidden = false
                noMessageLabel[5].isHidden = false
                uiTableViews[5].isHidden = true
            }else{
                noMessageImage[5].isHidden = true
                noMessageLabel[5].isHidden = true
                uiTableViews[5].isHidden = false
            }
        }
        if tableView == tableview7{
            model = templateEvents.filter{
                filtered in
                return filtered.dayNumber == 7 && filtered.templateName == name
            }
            if model.count == 0{
                noMessageImage[6].isHidden = false
                noMessageLabel[6].isHidden = false
                uiTableViews[6].isHidden = true
            }else{
                noMessageImage[6].isHidden = true
                noMessageLabel[6].isHidden = true
                uiTableViews[6].isHidden = false
                
            }
        }

        return model.count

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CustomEventsTableViewCell.identifier, for: indexPath) as! CustomEventsTableViewCell
        cell.selectionStyle = .none
        var model =  templateEvents
        if tableView == tableview1{
            model = templateEvents.filter{
                filtered in
                return filtered.dayNumber == 1 && filtered.templateName == name
            }
        }
        else if tableView == tableview2{
            model =  templateEvents.filter{
                filtered in
                return filtered.dayNumber == 2 && filtered.templateName == name
            }
        }
       else if tableView == tableview3{
           model =  templateEvents.filter{ filtered in
                return filtered.dayNumber == 3 && filtered.templateName == name
            }
        }
       else if tableView == tableview4{
           model =  templateEvents.filter{
                filtered in
                return filtered.dayNumber == 4 && filtered.templateName == name
            }
        }
       else if tableView == tableview5{
           model =  templateEvents.filter{
                filtered in
                return filtered.dayNumber == 5 && filtered.templateName == name
           }
        }
       else if tableView == tableview6{
           model =  templateEvents.filter{
                filtered in
                return filtered.dayNumber == 6 && filtered.templateName == name
           }
        }
        else {
            model = templateEvents.filter{
                filtered in
                return filtered.dayNumber == 7 && filtered.templateName == name
            }
        }
        model.sort{$0.startTime! < $1.startTime!}
        let selectedDate = CalendarFunctions().getTimeString(date: model[indexPath.row].startTime!)
        let endTime =  CalendarFunctions().getTimeString(date: model[indexPath.row].endTime!)
        cell.eventStartTime.text = selectedDate + " - "
        cell.eventEndTime.text = endTime
        cell.eventTitle.text = model[indexPath.row].title!
        let tagList = tags.filter { filtered in
            return model[indexPath.row].tag == filtered.name!
        }
        cell.eventTag.text = model[indexPath.row].tag!.trimmingCharacters(in: .whitespaces)
        if  tagList.count != 0{
            cell.eventTag.backgroundColor = tagList[0].color

        }else{
            cell.eventTag.backgroundColor = themeColor
        }
        cell.eventTag.textColor = .black
        cell.eventTag.layer.masksToBounds = true
        cell.eventTag.layer.cornerRadius = 15
         
        return cell
 
    }
   
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       
        var model =   templateEvents
        if tableView == tableview1{
            model = templateEvents.filter{ filtered in
                return filtered.dayNumber == 1 && filtered.templateName == name
            }
        }
        if tableView == tableview2{
             model =  templateEvents.filter{ filtered in
                return filtered.dayNumber == 2 && filtered.templateName == name
             }
        }
        if tableView == tableview3{
             model =  templateEvents.filter{ filtered in
                return filtered.dayNumber == 3 && filtered.templateName == name
             }
        }
        if tableView == tableview4{
            model =  templateEvents.filter{ filtered in
                return filtered.dayNumber == 4 && filtered.templateName == name
            }
        }
        if tableView == tableview5{
            model =  templateEvents.filter{ filtered in
                return filtered.dayNumber == 5 && filtered.templateName == name
            }
        }
        if tableView == tableview6{
            model =  templateEvents.filter{ filtered in
                return filtered.dayNumber == 6 && filtered.templateName == name
            }
        }
        if tableView == tableview7{
            model =   templateEvents.filter{ filtered in
                return filtered.dayNumber == 7 && filtered.templateName == name
            }
        }
        model.sort{$0.startTime! < $1.startTime!}
        navigationController?.pushViewController(DetailedRoutineViewController(event: model[indexPath.row]), animated: true)
    }
    
    func getAllTemplateEvents(){
       do{
           let request = TemplateEvents.fetchRequest() as NSFetchRequest<TemplateEvents>
           templateEvents = try context.fetch(request)
       }catch {
            print(error)
       }

    }
    
}


