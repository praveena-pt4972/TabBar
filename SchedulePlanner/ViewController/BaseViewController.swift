//
//  ViewController.swift
//  TabBar
//
//  Created by praveena-pt4972 on 21/04/22.
//

import UIKit
import UserNotifications
import CoreData

let themeColor = UIColor(red: 0.92, green: 0.59, blue: 0.58, alpha: 1.00)
class BaseViewController: UIViewController {


    override func viewDidLoad() {
        super.viewDidLoad()
        getAllTags()
        addDefaultTags()
        schedulingNotification()
    }
    override func viewDidLayoutSubviews() {
      didTapButton()
    }
    var tags = [Tags]()

    @objc func didTapButton(){
        let tabBarVC = UITabBarController()
        tabBarVC.tabBar.tintColor = themeColor
        let vc1 = UINavigationController(rootViewController: TodayCalendarViewController())
        let vc3 = UINavigationController(rootViewController:  MonthlyCalendarViewController())
        let vc4 = UINavigationController(rootViewController: EventViewController())
        let vc2 = UINavigationController(rootViewController:RoutinesViewController())
        let vc5 = UINavigationController(rootViewController: SettingViewController())
        
        vc1.title = "Today"
        vc3.title = "Calendar"
        vc4.title = "Events"
        vc2.title = "Routines"
        vc5.title = "Settings"
        
        tabBarVC.setViewControllers([vc1,vc3,vc4,vc2,vc5], animated: false)
        
        guard let items = tabBarVC.tabBar.items else{
            return
        }
        let images = ["clock.fill","calendar.circle.fill","clock.fill","arrow.triangle.2.circlepath.circle.fill","gearshape.fill"]
        for x in 0..<items.count{
            items[x].image = UIImage(systemName: images[x])
        }
      tabBarVC.modalPresentationStyle = .fullScreen
        present(tabBarVC, animated: true)
    }
    func getAllTags(){
    do{
        let request = Tags.fetchRequest() as NSFetchRequest<Tags>
        tags = try context.fetch(request)
     }catch {

        }

    }
    func addDefaultTags(){
        if tags.count == 0 {
            for i in 0..<name.count{
                createTag(id: Int64(i), name: name[i], color: UIColor.tagColor(AssetsColor.init(rawValue: i+1)!)!)
            }
        }
    }
}

enum AssetsColor : Int{
   
    case blue = 1
    case green
    case lightLavendar
    case lightOrange
    case oliveGreen
    case lightSkyBlue
    case themeColor
    case lightBlue
    case lavendar
    case lightGreen
    case lightPink
    case orange
    case peach
    case sandal
    case yellow
    case skyblue
    
}

extension UIColor {
    static func tagColor(_ name: AssetsColor) -> UIColor? {
        switch name.rawValue {
        case 1:
            return UIColor(named: "blue")
        case 2:
            return UIColor(named: "green")
        case 3:
            return UIColor(named: "lightLavendar")
        case 4:
            return UIColor(named: "lightOrange")
        case 5:
            return UIColor(named: "oliveGreen")
        case 6:
            return UIColor(named: "lightSkyBlue")
        case 7:
            return UIColor(named: "themeColor")
        case 8:
            return UIColor(named: "lightBlue")
        case 9:
            return UIColor(named: "lavendar")
        case 10:
            return UIColor(named: "lightGreen")
        case 11:
            return UIColor(named: "lightPink")
        case 12:
            return UIColor(named: "orange")
        case 13:
            return UIColor(named: "peach")
        case 14:
            return UIColor(named: "sandal")
        case 15:
            return UIColor(named: "yellow")
        case 16:
            return UIColor(named: "skyblue")
        default:
            return UIColor(named: "themeColor")
        }
    }
}


