//
//  Tags+CoreDataProperties.swift
//  TabBar
//
//  Created by praveena-pt4972 on 19/05/22.
//
//

import Foundation
import CoreData
import UIKit

extension Tags {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Tags> {
        return NSFetchRequest<Tags>(entityName: "Tags")
    }

    @NSManaged public var id: Int64
    @NSManaged public var name: String?
    @NSManaged public var color: UIColor?

}

extension Tags : Identifiable {

}
