//
//  TemplateEvents+CoreDataProperties.swift
//  SchedulePlannerAppPlanner
//
//  Created by praveena-pt4972 on 26/06/22.
//
//

import Foundation
import CoreData


extension TemplateEvents {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<TemplateEvents> {
        return NSFetchRequest<TemplateEvents>(entityName: "TemplateEvents")
    }

    @NSManaged public var dayNumber: Int64
    @NSManaged public var endTime: Date?
    @NSManaged public var id: Int64
    @NSManaged public var notes: String?
    @NSManaged public var remainder: Date?
    @NSManaged public var startTime: Date?
    @NSManaged public var tag: String?
    @NSManaged public var templateName: String?
    @NSManaged public var title: String?
    @NSManaged public var template: Template?

}

extension TemplateEvents : Identifiable {

}
