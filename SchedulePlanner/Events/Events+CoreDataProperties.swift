//
//  Events+CoreDataProperties.swift
//  SchedulePlannerAppPlanner
//
//  Created by praveena-pt4972 on 03/07/22.
//
//

import Foundation
import CoreData


extension Events {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Events> {
        return NSFetchRequest<Events>(entityName: "Events")
    }

    @NSManaged public var date: Date?
    @NSManaged public var endTime: Date?
    @NSManaged public var id: Int64
    @NSManaged public var notes: String?
    @NSManaged public var remainder: Date?
    @NSManaged public var startTime: Date?
    @NSManaged public var tag: String?
    @NSManaged public var templateName: String?
    @NSManaged public var title: String?
    @NSManaged public var templateEvents: TemplateEvents?
    @NSManaged public var templates: NSSet?

}

// MARK: Generated accessors for templates
extension Events {

    @objc(addTemplatesObject:)
    @NSManaged public func addToTemplates(_ value: Template)

    @objc(removeTemplatesObject:)
    @NSManaged public func removeFromTemplates(_ value: Template)

    @objc(addTemplates:)
    @NSManaged public func addToTemplates(_ values: NSSet)

    @objc(removeTemplates:)
    @NSManaged public func removeFromTemplates(_ values: NSSet)

}

extension Events : Identifiable {

}
