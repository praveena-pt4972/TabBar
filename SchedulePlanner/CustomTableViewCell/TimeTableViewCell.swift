//
//  TimeTableViewCell.swift
//  SchedulePlannerAppPlanner
//
//  Created by praveena-pt4972 on 03/07/22.
//

import UIKit

class TimeTableViewCell: UITableViewCell {
   
    let timeLabel: UILabel = {
        let textLabel = PaddingLabel(withInsets: 10, 10, 10, 10)
        textLabel.font = UIFont.preferredFont(forTextStyle: .headline, compatibleWith: .none)
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        return textLabel
    }()
 
    let endtimeLabel: UILabel = {
        let textLabel = PaddingLabel(withInsets: 10, 10, 10, 10)
        textLabel.font = UIFont.preferredFont(forTextStyle: .headline, compatibleWith: .none)
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        return textLabel
    }()

    let time : UIImageView = {
        let time = UIImageView()
        let largeConfig = UIImage.SymbolConfiguration(pointSize: 20, weight: .bold , scale: .small)
        time.image = UIImage(systemName: "clock", withConfiguration: largeConfig)
        time.frame.size = CGSize(width: 20, height: 20)
        time.translatesAutoresizingMaskIntoConstraints = false
        time.tintColor = themeColor
        return time
    }()
    let arrow : UIImageView = {
        let arrow = UIImageView()
        let largeConfig = UIImage.SymbolConfiguration(pointSize: 20, weight: .bold , scale: .small)
        arrow.image = UIImage(systemName: "arrow.left.arrow.right", withConfiguration: largeConfig)
        arrow.translatesAutoresizingMaskIntoConstraints = false
        arrow.tintColor = themeColor
        return arrow
    }()
   

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
            contentView.addSubview(timeLabel)
            contentView.addSubview(endtimeLabel)
            contentView.addSubview(time)
            contentView.addSubview(arrow)
            time.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 30).isActive = true
            time.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 20).isActive = true
            timeLabel.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 20).isActive = true
            timeLabel.leadingAnchor.constraint(equalTo: time.trailingAnchor,constant: 20).isActive = true
            arrow.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 30).isActive = true
            arrow.leadingAnchor.constraint(equalTo: timeLabel.trailingAnchor,constant: 20).isActive = true
            endtimeLabel.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 20).isActive = true
            endtimeLabel.leadingAnchor.constraint(equalTo: arrow.trailingAnchor,constant: 20).isActive = true
            contentView.bottomAnchor.constraint(equalTo: timeLabel.bottomAnchor,constant: 30).isActive = translatesAutoresizingMaskIntoConstraints
        
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
