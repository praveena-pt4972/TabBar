//
//  CustomDateTableViewCell.swift
//  SchedulePlannerAppPlanner
//
//  Created by praveena-pt4972 on 03/07/22.
//


import UIKit

class CustomTitleTableViewCell: UITableViewCell {

    let titleLabel: UILabel = {
        let textLabel = UILabel()
        textLabel.font = UIFont.preferredFont(forTextStyle: .headline, compatibleWith: .none)
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        return textLabel
    }()
 

    let titleIcon : UIImageView = {
        let titleIcon = UIImageView()
        let largeConfig = UIImage.SymbolConfiguration(pointSize: 20, weight: .bold , scale: .small)
        titleIcon.image = UIImage(systemName: "text.justify.leading", withConfiguration: largeConfig)
        titleIcon.frame.size = CGSize(width: 20, height: 20)
        titleIcon.translatesAutoresizingMaskIntoConstraints = false
        titleIcon.tintColor = themeColor
        return titleIcon
    }()

   

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(titleLabel)
        contentView.addSubview(titleIcon)
        titleIcon.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 0).isActive = true
        titleIcon.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 20).isActive = true
        titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 0).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: titleIcon.trailingAnchor,constant: 20).isActive = true
        contentView.bottomAnchor.constraint(equalTo: titleLabel.bottomAnchor,constant: 30).isActive = translatesAutoresizingMaskIntoConstraints
        
    }
    
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
