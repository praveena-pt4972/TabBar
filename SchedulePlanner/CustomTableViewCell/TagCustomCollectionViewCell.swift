//
//  CustomCollectionViewCell.swift
//  TabBar
//
//  Created by praveena-pt4972 on 19/05/22.
//

import UIKit

class TagCustomCollectionViewCell: UICollectionViewCell {
    
    let tagNameLabel: UILabel = {
            let tagNameLabel = PaddingLabel(withInsets: 5, 5, 10, 10)
            tagNameLabel.font = UIFont.preferredFont(forTextStyle: .headline)
            tagNameLabel.translatesAutoresizingMaskIntoConstraints = false //enable autolayout
            return tagNameLabel
        }()
    
    override var isSelected: Bool{
            didSet(newValue){
                if TodayCalendarViewController.switchOnOff.isOn == true{
                    contentView.layer.borderWidth = newValue ? 3 : 0
                    contentView.layer.cornerRadius = 15
                    contentView.layer.borderColor = UIColor.brown.cgColor
                }else{
                    contentView.layer.borderWidth = newValue ? 2 : 0
                    contentView.layer.cornerRadius = 15
                    contentView.layer.borderColor = UIColor.black.cgColor
                }
            }
    }
 
    override init(frame: CGRect) {
        super.init(frame: frame);
        
        contentView.addSubview(tagNameLabel)
       
        tagNameLabel.translatesAutoresizingMaskIntoConstraints = false
        tagNameLabel.leadingAnchor.constraint(equalTo:contentView.safeAreaLayoutGuide.leadingAnchor).isActive = true
        tagNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor).isActive = true
        tagNameLabel.trailingAnchor.constraint(equalTo:  contentView.trailingAnchor).isActive = true
        tagNameLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor).isActive = true
        tagNameLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor).isActive = true
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }


}

class PaddingLabel: UILabel {
    
    var topInset: CGFloat
    var bottomInset: CGFloat
    var leftInset: CGFloat
    var rightInset: CGFloat
    
    required init(withInsets top: CGFloat, _ bottom: CGFloat, _ left: CGFloat, _ right: CGFloat) {
        self.topInset = top
        self.bottomInset = bottom
        self.leftInset = left
        self.rightInset = right
        super.init(frame: CGRect.zero)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func drawText(in rect: CGRect) {
        let insets = UIEdgeInsets(top: topInset, left: leftInset, bottom: bottomInset, right: rightInset)
        super.drawText(in: rect.inset(by: insets))
    }
    
    override var intrinsicContentSize: CGSize {
        get {
            var contentSize = super.intrinsicContentSize
            contentSize.height += topInset + bottomInset
            contentSize.width += leftInset + rightInset
            return contentSize
        }
    }
    
}
