//
//  TextViewTableViewCell.swift
//  TabBar
//
//  Created by praveena-pt4972 on 16/05/22.
//

import UIKit

class EventNotesCustomCell: UITableViewCell {

    var notesTextView: UITextView = {
        let notesTextView  = UITextView()
        notesTextView.translatesAutoresizingMaskIntoConstraints = false
        notesTextView.font = UIFont.systemFont(ofSize: 16.0)
        notesTextView.backgroundColor = .tertiarySystemBackground
        notesTextView.isEditable = true
        return notesTextView
        }()


    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(notesTextView)
        contentView.backgroundColor  = .tertiarySystemBackground
        notesTextView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 5).isActive = true
        notesTextView.widthAnchor.constraint(equalTo: contentView.widthAnchor,constant: -0).isActive = true
        notesTextView.heightAnchor.constraint(equalTo: contentView.heightAnchor,constant: -0).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    

}
