//
//  TimeTableViewCell.swift
//  TabBar
//
//  Created by praveena-pt4972 on 16/05/22.
//

import UIKit

class EventTimeCustomCell: UITableViewCell {

    let timeLabel : UILabel = {
        let timeLabel = UILabel(frame:CGRect.zero)
        timeLabel.translatesAutoresizingMaskIntoConstraints = false //enable autolayout
        return timeLabel
        }()

    let datePicker : UIDatePicker =  {
        var datePicker = UIDatePicker()
        datePicker.datePickerMode = .dateAndTime
        datePicker.tintColor = themeColor
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        return datePicker
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)

        contentView.addSubview(timeLabel)
        timeLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 10).isActive = true
        timeLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor,constant: -30).isActive = true
        timeLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor,constant: 0).isActive = true
        contentView.addSubview(datePicker)
        datePicker.translatesAutoresizingMaskIntoConstraints = false
        datePicker.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,constant: -10).isActive = true
        datePicker.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 20).isActive = true

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
