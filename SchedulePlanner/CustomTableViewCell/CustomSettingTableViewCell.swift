//
//  CustomSettingTableViewCell.swift
//  SchedulePlannerAppPlanner
//
//  Created by praveena-pt4972 on 07/06/22.
//

import UIKit


class CustomSettingTableViewCell: UITableViewCell {

    let remainderLabel: UILabel = {
        let remainderLabel = UILabel(frame:CGRect.zero)
        remainderLabel.translatesAutoresizingMaskIntoConstraints = false
        remainderLabel.adjustsFontForContentSizeCategory = false
        return remainderLabel
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
       
        contentView.addSubview(remainderLabel)
        remainderLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 10).isActive = true
        remainderLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor,constant: -30).isActive = true
        remainderLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor,constant: -5).isActive = true
        contentView.addSubview(TodayCalendarViewController.switchOnOff)
        TodayCalendarViewController.switchOnOff.translatesAutoresizingMaskIntoConstraints = false
        TodayCalendarViewController.switchOnOff.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,constant: -30).isActive = true
        TodayCalendarViewController.switchOnOff.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 20).isActive = true

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
