//
//  TagTableViewCell.swift
//  TabBar
//
//  Created by praveena-pt4972 on 16/05/22.
//

import UIKit

class ExpandableRemainderTableViewCell: UITableViewCell {

    let expandableCellsLabel: UILabel = {
        let l = UILabel(frame:CGRect.zero)
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }()

override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    let margins = UIEdgeInsets(top: 0, left: 50, bottom: 10, right: 10)
    contentView.frame = contentView.frame.inset(by: margins)
    contentView.addSubview(expandableCellsLabel)
    expandableCellsLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 10).isActive = true
    expandableCellsLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor,constant: -30).isActive = true
    expandableCellsLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor,constant: -5).isActive = true

}
required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
}

}
