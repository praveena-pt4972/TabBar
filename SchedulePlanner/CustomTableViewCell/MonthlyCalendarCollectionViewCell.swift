//
//  MonthlyCalendarCollectionViewCell.swift
//  SchedulePlannerAppPlanner
//
//  Created by praveena-pt4972 on 13/06/22.
//

import UIKit

class MonthlyCalendarCollectionViewCell: UICollectionViewCell {
        var dayOfMonth:  UILabel = UILabel()
        var day :  UILabel = UILabel()
        let stack : UIStackView = {
            let stack = UIStackView()
            stack.axis = .vertical
            stack.distribution = .equalSpacing
            stack.spacing = 1
            stack.translatesAutoresizingMaskIntoConstraints = false
            return stack
        }()
        
        override var isSelected: Bool{
            didSet(newValue){
                if TodayCalendarViewController.switchOnOff.isOn == true{
                    contentView.layer.borderWidth = newValue ? 2 : 0
                    contentView.layer.cornerRadius = 10
                    contentView.layer.borderColor = UIColor.white.cgColor
                }
                else{
                    contentView.layer.borderWidth = newValue ? 2 : 0
                    contentView.layer.cornerRadius = 10
                    contentView.layer.borderColor = UIColor.black.cgColor
                }
        }
    }
        
        override init(frame : CGRect) {
            super.init(frame : frame)
            dayOfMonth.textAlignment =  .center
            contentView.layer.cornerRadius = 15
            day.textAlignment =  .center
            day.font = UIFont.preferredFont(forTextStyle: .subheadline)
            dayOfMonth.font = UIFont.preferredFont(forTextStyle: .footnote)
            stack.addArrangedSubview(day)
            stack.addArrangedSubview(dayOfMonth)
            contentView.addSubview(stack)
            stack.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 3).isActive = true
            stack.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
          }

        required init(coder aDecoder: NSCoder) {
            fatalError("init(coder:) has not been implemented")
        }
    }

