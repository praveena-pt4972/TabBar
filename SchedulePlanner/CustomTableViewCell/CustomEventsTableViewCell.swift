//
//  CustomTableViewCell.swift
//  TabBar
//
//  Created by praveena-pt4972 on 19/05/22.
//

import UIKit
import CoreData



class CustomEventsTableViewCell: UITableViewCell {
    
    static let identifier = "CustomTableViewCell"
    
    let eventIcon : UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(systemName: "")
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.tintColor = .systemGray
        return imageView
    }()
    
    
    let eventStartTime : UILabel = {
        let eventStartTime  = UILabel()
        eventStartTime.font = UIFont.preferredFont(forTextStyle: .headline)
        eventStartTime.translatesAutoresizingMaskIntoConstraints = false
        eventStartTime.adjustsFontForContentSizeCategory = false
        return eventStartTime
    }()
    
    let eventEndTime : UILabel = {
        let eventEndTime  = UILabel()
        eventEndTime.font = UIFont.preferredFont(forTextStyle: .headline)
        eventEndTime.translatesAutoresizingMaskIntoConstraints = false
        eventEndTime.adjustsFontForContentSizeCategory = false
        return eventEndTime
    }()
    
    let eventTitle : UILabel = {
        let eventTitle  = UILabel()
        eventTitle.font = UIFont.preferredFont(forTextStyle: .headline)
        eventTitle.numberOfLines = 0
        eventTitle.preferredMaxLayoutWidth = 150
        eventTitle.adjustsFontForContentSizeCategory = false
        eventTitle.lineBreakMode = NSLineBreakMode.byWordWrapping
        eventTitle.sizeToFit()
        eventTitle.translatesAutoresizingMaskIntoConstraints = false
        return eventTitle
    }()
  
    let eventTag : UILabel = {
        let eventTag  = PaddingLabel(withInsets: 5, 5,10, 10)
        eventTag.font = UIFont.boldSystemFont(ofSize: 15.0)
        eventTag.translatesAutoresizingMaskIntoConstraints = false
        return eventTag
    }()
    
    let titleLabelStack : UIStackView = {
        let titleLabelStack = UIStackView()
        titleLabelStack.axis = .vertical
        titleLabelStack.distribution = .fillProportionally
        titleLabelStack.translatesAutoresizingMaskIntoConstraints = false
        return titleLabelStack
    }()
    
    let timeLabelStack : UIStackView = {
        let timeLabelStack = UIStackView()
        timeLabelStack.axis = .vertical
        timeLabelStack.distribution = .equalSpacing
        timeLabelStack.spacing = 3
        timeLabelStack.alignment = .fill
        timeLabelStack.translatesAutoresizingMaskIntoConstraints = false
        return timeLabelStack
    }()
    
    override var isSelected: Bool{
            didSet(newValue){
                if TodayCalendarViewController.switchOnOff.isOn == true{
                    contentView.layer.borderWidth = newValue ? 3 : 0
                    contentView.layer.cornerRadius = 15
                    contentView.layer.borderColor = UIColor.brown.cgColor
                }else{
                    contentView.layer.borderWidth = newValue ? 2 : 0
                    contentView.layer.cornerRadius = 15
                    contentView.layer.borderColor = UIColor.black.cgColor
                }
            }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        timeLabelStack.addArrangedSubview(eventStartTime)
        timeLabelStack.addArrangedSubview(eventEndTime)
        contentView.layer.cornerRadius = 15
        contentView.addSubview(timeLabelStack)
        contentView.addSubview(eventTitle)
        contentView.addSubview(eventIcon)
        contentView.addSubview(eventTag)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        timeLabelStack.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 10).isActive = true
        eventTitle.leadingAnchor.constraint(equalTo: timeLabelStack.trailingAnchor,constant: 20).isActive = true
        timeLabelStack.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 20).isActive = true
        eventTitle.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 20).isActive = true
        eventTag.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,constant: -10).isActive = true
        eventIcon.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 10).isActive = true
        eventIcon.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,constant: -20).isActive = true
        eventTag.bottomAnchor.constraint(equalTo: contentView.bottomAnchor,constant: -10).isActive = true
        contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 5, left: 10, bottom: 10, right: 10))
        contentView.layer.borderWidth = 2
        contentView.layer.borderColor = themeColor.cgColor
    }
}
