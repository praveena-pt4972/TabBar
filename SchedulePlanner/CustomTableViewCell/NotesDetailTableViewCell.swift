//
//  NotesDetailTableViewCell.swift
//  SchedulePlannerAppPlanner
//
//  Created by praveena-pt4972 on 04/07/22.
//


import UIKit

class NotesDetailTableViewCell: UITableViewCell {
   
   

   
    let notesLabel: UILabel = {
        let textLabel = UILabel()
        textLabel.font = UIFont.preferredFont(forTextStyle: .headline, compatibleWith: .none)
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.numberOfLines = 0
        textLabel.preferredMaxLayoutWidth = 300
        textLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        return textLabel    }()
 
    let notesIcon : UIImageView = {
        let notesIcon = UIImageView()
        let largeConfig = UIImage.SymbolConfiguration(pointSize: 20, weight: .bold , scale: .small)
        notesIcon.image = UIImage(systemName: "doc.plaintext", withConfiguration: largeConfig)
        notesIcon.frame.size = CGSize(width: 20, height: 20)
        notesIcon.translatesAutoresizingMaskIntoConstraints = false
        notesIcon.tintColor = themeColor
        return notesIcon
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(notesLabel)
        contentView.addSubview(notesIcon)
        notesIcon.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 0).isActive = true
        notesIcon.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 20).isActive = true
        notesLabel.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 0).isActive = true
        notesLabel.leadingAnchor.constraint(equalTo: notesIcon.trailingAnchor,constant: 20).isActive = true
        contentView.bottomAnchor.constraint(equalTo: notesLabel.bottomAnchor,constant: 30).isActive = translatesAutoresizingMaskIntoConstraints
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
