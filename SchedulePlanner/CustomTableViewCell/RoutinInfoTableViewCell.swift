//
//  routinInfoTableViewCell.swift
//  SchedulePlannerAppPlanner
//
//  Created by praveena-pt4972 on 26/06/22.
//

import UIKit

class RoutinInfoTableViewCell: UITableViewCell {

    
    let label: UILabel = {
        let textLabel = UILabel()
        textLabel.font = UIFont.preferredFont(forTextStyle: .headline, compatibleWith: .none)
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.numberOfLines = 0
        textLabel.preferredMaxLayoutWidth = 300
        textLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        return textLabel
    }()
   
    let icon : UIImageView = {
        let icon = UIImageView()
        icon.image = UIImage(systemName: "text.justify.leading")
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.tintColor = themeColor
        return icon
    }()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(label)
        contentView.addSubview(icon)
        icon.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 20).isActive = true
        icon.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 30).isActive = true
        label.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 20).isActive = true
        label.leadingAnchor.constraint(equalTo: icon.trailingAnchor,constant: 20).isActive = true
        contentView.bottomAnchor.constraint(equalTo: label.bottomAnchor,constant: 20).isActive = translatesAutoresizingMaskIntoConstraints
        
    }
    
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
