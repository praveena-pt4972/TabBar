//
//  TagCollectionViewCell.swift
//  SchedulePlannerAppPlanner
//
//  Created by praveena-pt4972 on 14/06/22.
//

import UIKit

class TagCollectionViewCell: UICollectionViewCell {
    override var isSelected: Bool{
            didSet(newValue){
                if TodayCalendarViewController.switchOnOff.isOn == true{
                    contentView.layer.borderWidth = newValue ? 3 : 0
                    contentView.layer.cornerRadius = 10
                    contentView.layer.borderColor = UIColor.white.cgColor
                }else{
                    contentView.layer.borderWidth = newValue ? 2 : 0
                    contentView.layer.cornerRadius = 10
                    contentView.layer.borderColor = UIColor.black.cgColor
                }
            }
    }
}
