//
//  ColorTableViewCell.swift
//  TabBar
//
//  Created by praveena-pt4972 on 16/05/22.
//

import UIKit
import CoreData

var lastSelectedIndexPath:IndexPath?

class EventTagCustomTableViewCell: UITableViewCell {

    let tagNameLabel: UILabel = {
        let tagNameLabel = UILabel(frame:CGRect.zero)
        tagNameLabel.translatesAutoresizingMaskIntoConstraints = false
        return tagNameLabel
    }()
    
    let tagButton : UIButton = {
        let nextMonthButton = UIButton()
        nextMonthButton.translatesAutoresizingMaskIntoConstraints = false
        let largeConfig = UIImage.SymbolConfiguration(pointSize: 25, weight: .bold , scale: .small)
        let leftArrow = UIImage(systemName: "plus", withConfiguration: largeConfig)
        nextMonthButton.setImage(leftArrow, for: .normal)
        nextMonthButton.tintColor = themeColor
        return nextMonthButton
    }()
    
    let cellId = "CellId"
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.estimatedItemSize = CGSize(width: 20, height: 50)
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.translatesAutoresizingMaskIntoConstraints = false;
        cv.showsVerticalScrollIndicator = false
        return cv;
    }()
    
  
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        collectionView.allowsMultipleSelection = false
        getAllTags()
        contentView.addSubview(tagNameLabel)
        tagNameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 10).isActive = true
        tagNameLabel.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 20).isActive = true
        contentView.addSubview(tagButton)
        tagButton.translatesAutoresizingMaskIntoConstraints = false
        tagButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,constant: -40).isActive = true
        tagButton.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 15).isActive = true
        setupViews()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var tags = [Tags]()
    func getAllTags(){
    do{
        let request = Tags.fetchRequest() as NSFetchRequest<Tags>
        tags = try context.fetch(request)
     }catch {
         print(error)
        }

    }

}

extension EventTagCustomTableViewCell : UICollectionViewDelegate, UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        getAllTags()
        return tags.count
    }

    func setupViews() {
        contentView.addSubview(collectionView)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(TagCustomCollectionViewCell.self, forCellWithReuseIdentifier: cellId)
        collectionView.backgroundColor  = .tertiarySystemBackground
        collectionView.allowsMultipleSelection = false
        collectionView.topAnchor.constraint(equalTo: tagNameLabel.bottomAnchor,constant: 20).isActive = true
        collectionView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 10).isActive = true
        collectionView.widthAnchor.constraint(equalTo: contentView.widthAnchor,constant: -20).isActive = true
        collectionView.bottomAnchor.constraint(equalTo: contentView.bottomAnchor).isActive = true
        collectionView.reloadData()

    }
   
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        getAllTags()

        let item = tags[indexPath.row]
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! TagCustomCollectionViewCell
        cell.tagNameLabel .text = item.name!.trimmingCharacters(in: .whitespaces)
        cell.backgroundColor = item.color
        cell.tagNameLabel.textColor = .black
        cell.layer.cornerRadius = 15
        if indexPath.row == 0 {
            lastSelectedIndexPath = indexPath
            cell.isSelected = true
        }
        cell.isSelected = (lastSelectedIndexPath == indexPath)
        return cell
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
 
        guard lastSelectedIndexPath != indexPath else { return }

         if let index = lastSelectedIndexPath {
            let cell = collectionView.cellForItem(at: index) as! TagCustomCollectionViewCell
            cell.isSelected = false
          }
          let cell = collectionView.cellForItem(at: indexPath) as! TagCustomCollectionViewCell
          cell.isSelected = true
          lastSelectedIndexPath = indexPath
    }
}

