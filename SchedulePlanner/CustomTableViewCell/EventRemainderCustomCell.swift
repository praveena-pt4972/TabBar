//
//  RemainderTableViewCell.swift
//  TabBar
//
//  Created by praveena-pt4972 on 16/05/22.
//

import UIKit

class EventRemainderCustomCell: UITableViewCell {

    let toogleSwitch: UISwitch = {
        let toogleSwitch = UISwitch()
        toogleSwitch.onTintColor = themeColor
        toogleSwitch.translatesAutoresizingMaskIntoConstraints = false
        return toogleSwitch
    }()
    let remainderLabel: UILabel = {
        let remainderLabel = UILabel(frame:CGRect.zero)
        remainderLabel.translatesAutoresizingMaskIntoConstraints = false
        remainderLabel.adjustsFontForContentSizeCategory = false
        return remainderLabel
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
       
        contentView.addSubview(remainderLabel)
        remainderLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 10).isActive = true
        remainderLabel.widthAnchor.constraint(equalTo: contentView.widthAnchor,constant: -30).isActive = true
        remainderLabel.heightAnchor.constraint(equalTo: contentView.heightAnchor,constant: -5).isActive = true

        contentView.addSubview(toogleSwitch)
        toogleSwitch.translatesAutoresizingMaskIntoConstraints = false
        toogleSwitch.trailingAnchor.constraint(equalTo: contentView.trailingAnchor,constant: -10).isActive = true
        toogleSwitch.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 20).isActive = true

    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
