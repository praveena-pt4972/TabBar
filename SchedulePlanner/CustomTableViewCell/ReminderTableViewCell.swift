//
//  ReminderTableViewCell.swift
//  SchedulePlannerAppPlanner
//
//  Created by praveena-pt4972 on 03/07/22.
//

import UIKit

class ReminderTableViewCell: UITableViewCell {
   
    let reminderLabel: UILabel = {
        let textLabel = UILabel()
        textLabel.font = UIFont.preferredFont(forTextStyle: .headline, compatibleWith: .none)
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        return textLabel
    }()
 

    let bell : UIImageView = {
        let bell = UIImageView()
        let largeConfig = UIImage.SymbolConfiguration(pointSize: 20, weight: .bold , scale: .small)
        bell.image = UIImage(systemName: "bell", withConfiguration: largeConfig)
        bell.frame.size = CGSize(width: 20, height: 20)
        bell.translatesAutoresizingMaskIntoConstraints = false
        bell.tintColor = themeColor
        return bell
    }()
   
   

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        contentView.addSubview(reminderLabel)
        contentView.addSubview(bell)
        bell.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 0).isActive = true
        bell.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 20).isActive = true
        reminderLabel.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 0).isActive = true
        reminderLabel.leadingAnchor.constraint(equalTo: bell.trailingAnchor,constant: 20).isActive = true
        contentView.bottomAnchor.constraint(equalTo: reminderLabel.bottomAnchor,constant: 30).isActive = translatesAutoresizingMaskIntoConstraints
        
    }
 
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
