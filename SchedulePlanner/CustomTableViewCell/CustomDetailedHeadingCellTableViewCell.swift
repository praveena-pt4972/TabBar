//
//  CustomDetailedCellTableViewCell.swift
//  SchedulePlannerAppPlanner
//
//  Created by praveena-pt4972 on 24/06/22.
//

import UIKit

class CustomDetailedHeadingCellTableViewCell: UITableViewCell {
   
   
    
    let titleLabel: UILabel = {
        let textLabel = UILabel()
        textLabel.font = UIFont.preferredFont(forTextStyle: .largeTitle, compatibleWith: .none)
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.numberOfLines = 0
        textLabel.preferredMaxLayoutWidth = 250
        textLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        return textLabel
    }()
   
    let tagLabel: UILabel = {
        let textLabel = PaddingLabel(withInsets: 5, 5, 10, 10)
        textLabel.font = UIFont.preferredFont(forTextStyle: .headline, compatibleWith: .none)
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        textLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
        return textLabel
    }()
 
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
   
        contentView.addSubview(titleLabel)
        contentView.addSubview(tagLabel)
        titleLabel.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 30).isActive = true
        titleLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 20).isActive = true
        tagLabel.centerYAnchor.constraint(equalTo: contentView.centerYAnchor,constant: 0).isActive = true
        tagLabel.trailingAnchor.constraint(equalTo: contentView.safeAreaLayoutGuide.trailingAnchor,constant: -20).isActive = true
        contentView.bottomAnchor.constraint(equalTo: titleLabel.bottomAnchor,constant: 10).isActive = translatesAutoresizingMaskIntoConstraints
        
    }
    
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

}
