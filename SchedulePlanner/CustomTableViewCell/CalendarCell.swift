//
//  CalendarCell.swift
//  TabBar
//
//  Created by praveena-pt4972 on 23/05/22.
//


import UIKit

class CalendarCell: UICollectionViewCell {
    var dayOfMonth:  UILabel = UILabel()
    var day :  UILabel = UILabel()
    var eventDot :  UILabel = UILabel()

    let stack : UIStackView = {
        let stack = UIStackView()
        stack.axis = .vertical
        stack.distribution = .equalSpacing
        stack.spacing = 0.5
        stack.translatesAutoresizingMaskIntoConstraints = false
        return stack
    }()
    
    override var isSelected: Bool{
            didSet(newValue){
            contentView.layer.borderWidth = newValue ? 2 : 0
            contentView.layer.cornerRadius = 10
            contentView.layer.borderColor = themeColor.cgColor
            }
    }
    
    override init(frame : CGRect) {
        super.init(frame : frame)
        dayOfMonth.textAlignment =  .center
        contentView.layer.cornerRadius = 15
        day.textAlignment =  .center
        dayOfMonth.font = UIFont.preferredFont(forTextStyle: .caption1)
        eventDot.textAlignment =  .center
        day.font = UIFont.preferredFont(forTextStyle: .body)
        stack.addArrangedSubview(day)
        stack.addArrangedSubview(dayOfMonth)
        stack.addArrangedSubview(eventDot)
        contentView.addSubview(stack)
        stack.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 5).isActive = true
        stack.centerXAnchor.constraint(equalTo: contentView.centerXAnchor).isActive = true
      }

    required init(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
