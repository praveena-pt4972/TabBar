////
////  CustomDetailEventCell.swift
////  SchedulePlannerAppPlanner
////
////  Created by praveena-pt4972 on 14/06/22.
////
//
//import UIKit
//
//class CustomDetailEventCell: UITableViewCell {
//   
//   
//    let labelName : UILabel = {
//        let labelName = UILabel()
//        labelName.font = UIFont.preferredFont(forTextStyle: .headline, compatibleWith: .none)
//        labelName.translatesAutoresizingMaskIntoConstraints = false
//        return labelName
//    }()
//    let textLabel1: UILabel = {
//        let textLabel = PaddingLabel(withInsets: 5, 5, 10, 10)
//        textLabel.font = UIFont.preferredFont(forTextStyle: .body, compatibleWith: .none)
//        textLabel.translatesAutoresizingMaskIntoConstraints = false
//        textLabel.numberOfLines = 200
//        textLabel.preferredMaxLayoutWidth = 200
//        textLabel.lineBreakMode = NSLineBreakMode.byWordWrapping
//        textLabel.sizeToFit()
//        return textLabel
//    }()
//   
//    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
//        super.init(style: style, reuseIdentifier: reuseIdentifier)
//   
//        contentView.addSubview(labelName)
//        labelName.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 30).isActive = true
//        labelName.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 20).isActive = true
//        contentView.addSubview(textLabel1)
//        textLabel1.translatesAutoresizingMaskIntoConstraints = false
//        textLabel1.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 140).isActive = true
//        textLabel1.topAnchor.constraint(equalTo: contentView.topAnchor,constant: 15).isActive = true
//        contentView.bottomAnchor.constraint(equalTo: textLabel1.bottomAnchor,constant: 30).isActive = true
//        
//    }
//    
//    required init?(coder: NSCoder) {
//        fatalError("init(coder:) has not been implemented")
//    }
//
//}
