//
//  TextFieldTableViewCell.swift
//  TabBar
//
//  Created by praveena-pt4972 on 16/05/22.
//

import UIKit

class EventTitleCustomCell: UITableViewCell{

    let titletextField: UITextField = {
        let titletextField = UITextField()
        titletextField.translatesAutoresizingMaskIntoConstraints = false
        titletextField.textAlignment = .left
        titletextField.backgroundColor = .tertiarySystemBackground
        titletextField.placeholder = "Enter the title *"
        return titletextField
    }()

override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    let margins = UIEdgeInsets(top: 0, left: 50, bottom: 10, right: 10)
    contentView.frame = contentView.frame.inset(by: margins)
    contentView.addSubview(titletextField)
    titletextField.leadingAnchor.constraint(equalTo: contentView.leadingAnchor,constant: 10).isActive = true
    titletextField.widthAnchor.constraint(equalTo: contentView.widthAnchor,constant: 0).isActive = true
    titletextField.heightAnchor.constraint(equalTo: contentView.heightAnchor,constant: -5).isActive = true

}

required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
}


}
