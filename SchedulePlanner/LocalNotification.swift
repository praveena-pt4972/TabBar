//
//  LocalNotification.swift
//  SchedulePlannerAppPlanner
//
//  Created by praveena-pt4972 on 15/06/22.
//

import Foundation
import UserNotifications

class LocalNotification{
    var title : String
    var time : Date
    var identifier : Int

    init(title : String,time : Date,identifier : Int){
        self.title = title
        self.time = time
        self.identifier = identifier
    }
    
    func scheduleNotification(){
       
        let center = UNUserNotificationCenter.current()
        let content = UNMutableNotificationContent()
        content.title = title
        content.body = "you have a new reminder"
        content.sound = .default
        var currDateComp = Calendar.current.dateComponents([.day, .month, .year], from: Date())
        currDateComp.hour = Calendar.current.component(.hour, from: time)
        currDateComp.minute = Calendar.current.component(.minute, from: time)


        let trigger = UNCalendarNotificationTrigger(dateMatching: currDateComp, repeats: false)
        let request = UNNotificationRequest(identifier: "reminder\(String(identifier))", content: content, trigger: trigger)
        center.add(request) { (error) in
            if error != nil {
                print("Error = \(error?.localizedDescription ?? "error local notification")")
            }
        }
        
    }
    
    
}
