//
//  Template+CoreDataProperties.swift
//  SchedulePlannerAppPlanner
//
//  Created by praveena-pt4972 on 03/07/22.
//
//

import Foundation
import CoreData


extension Template {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Template> {
        return NSFetchRequest<Template>(entityName: "Template")
    }

    @NSManaged public var date: Date?
    @NSManaged public var includeWeekend: Bool
    @NSManaged public var noOfDays: Int64
    @NSManaged public var templateName: String?
    @NSManaged public var templateEvents: NSSet?
    @NSManaged public var events: Events?

}

// MARK: Generated accessors for templateEvents
extension Template {

    @objc(addTemplateEventsObject:)
    @NSManaged public func addToTemplateEvents(_ value: TemplateEvents)

    @objc(removeTemplateEventsObject:)
    @NSManaged public func removeFromTemplateEvents(_ value: TemplateEvents)

    @objc(addTemplateEvents:)
    @NSManaged public func addToTemplateEvents(_ values: NSSet)

    @objc(removeTemplateEvents:)
    @NSManaged public func removeFromTemplateEvents(_ values: NSSet)

}

extension Template : Identifiable {

}
