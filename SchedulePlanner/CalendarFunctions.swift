//
//  CalendarFunctions.swift
//  TabBar
//
//  Created by praveena-pt4972 on 23/05/22.
//
//    Wednesday, Sep 12, 2018           --> EEEE, MMM d, yyyy
//    09/12/2018                        --> MM/dd/yyyy
//    09-12-2018 14:11                  --> MM-dd-yyyy HH:mm
//    Sep 12, 2:11 PM                   --> MMM d, h:mm a
//    September 2018                    --> MMMM yyyy
//    Sep 12, 2018                      --> MMM d, yyyy
//    Wed, 12 Sep 2018 14:11:54 +0000   --> E, d MMM yyyy HH:mm:ss Z
//    2018-09-12T14:11:54+0000          --> yyyy-MM-dd'T'HH:mm:ssZ
//    12.09.18                          --> dd.MM.yy
//    10:41:02.112                      --> HH:mm:ss.SSS


import Foundation

import UIKit
import CoreData

let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext


class CalendarFunctions
{

    let calendar = Calendar.current
    let dateFormatter = DateFormatter()
    
    func currentDate(date : Date) -> String{
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        let currentDate = dateFormatter.string(from: date)
        return currentDate
    }
    func getDateFormat(date : Date) -> String{
        dateFormatter.dateFormat = "MMMM d, yyyy EEEE "
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        let getDateFormat = dateFormatter.string(from: date)
        return getDateFormat
    }
    
    func stringToDate(date : String) -> Date{
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        if let current = dateFormatter.date(from: date) {
            return current
        }
         return Date()
    }
    
    func monthString(date: Date) -> String
    {
        dateFormatter.dateFormat = "LLLL"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.string(from: date)
    }
    func monthYearString(date: Date) -> String
    {
        dateFormatter.dateFormat = "MMMM yyyy"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        return dateFormatter.string(from: date)
    }
    
    func yearString(date: Date) -> String
    {
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = "yyyy"
        return dateFormatter.string(from: date)
    }
    
    func dayOfMonth(date: Date) -> Int
    {
        let components = calendar.dateComponents([.day], from: date)
        return components.day!
    }
    
    func addDays(date: Date, days: Int) -> Date
    
    {
        return calendar.date(byAdding: .day, value: days, to: date)!
    }
    func addMonths(date: Date, days: Int) -> Date
    
    {
        return calendar.date(byAdding: .month, value: days, to: date)!
    }
    func minusMonth(date: Date) -> Date
    {
        return calendar.date(byAdding: .month, value: -1, to: date)!
    }
    
    
    func getTodayWeekDay(date: Date)-> String{
        
           let day = addDays(date: date, days: 0)
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        dateFormatter.dateFormat = "EEE"
           let weekDay = dateFormatter.string(from: day)
           return weekDay
     }
    func daysInMonth(date: Date) -> Int
    {
        let range = calendar.range(of: .day, in: .month, for: date)!
        return range.count
    }
  
    func firstOfMonth(date: Date) -> Date
    {
        let components = calendar.dateComponents([.year, .month], from: date)
        return calendar.date(from: components)!
    }
    
    func weekDay(date: Date) -> Int
    {
        let components = calendar.dateComponents([.weekday], from: date)
        return components.weekday! - 1
    }
    
    func getSelectedDate(date: String) -> Date
    
    {
        dateFormatter.dateFormat = "dd-MM-yyyy"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        let selectedDate = dateFormatter.date(from: date)

        if let selectedDate = selectedDate {
            return selectedDate
        }
        return Date()
    }
    func getTimeString(date: Date) -> String
    {
        dateFormatter.dateFormat = "HH:mm a"
        dateFormatter.timeZone = TimeZone.current
        dateFormatter.locale = Locale.current
        let stringtime = dateFormatter.string(from: date)
        return stringtime
    }

}

func schedulingNotification(){
    var events = [Events]()
        do{
            let request = Events.fetchRequest() as NSFetchRequest<Events>
             let sort = NSSortDescriptor(key: "remainder", ascending: true)
             request.sortDescriptors = [sort]
            events = try context.fetch(request)
        }catch {
            print(error)
        }
    

    let eventTimes = events.filter{ filtered in
        let date = CalendarFunctions().currentDate(date: filtered.date!)
        let currentDate = CalendarFunctions().currentDate(date: Date())

        let currentTimeString = CalendarFunctions().getTimeString(date: Date())
        var remainderTimeString : String = ""
        if filtered.remainder != nil{
            remainderTimeString = CalendarFunctions().getTimeString(date: filtered.remainder!)

        }
            return  date == currentDate && currentTimeString <= remainderTimeString
    }
        
    for i in 0..<eventTimes.count{
        let notify = LocalNotification(title: eventTimes[i].title!, time: eventTimes[i].remainder!, identifier : i)
            notify.scheduleNotification()
    }

}

